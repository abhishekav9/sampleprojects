/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business;

import com.itam.business.enterprise.Enterprise;
import com.itam.business.network.Network;
import com.itam.business.organization.Organization;
import com.itam.business.role.Role;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class GlobalSystem extends Organization {

    private static GlobalSystem system;
    private Network rootNetwork;
    Random random = new Random();

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public static GlobalSystem getInstance() {
        if (system == null) {
            system = new GlobalSystem();
        }
        return system;
    }

    public boolean checkIfUsernameUnique(String userName) {
        if (!this.getUserAccountDirectory().checkUserNameUnique(userName)) {
            return false;
        }
        for (Enterprise enterprise : rootNetwork.getEnterpriseDirectory().getEnterpriseList()) {
            if (!enterprise.getUserAccountDirectory().checkUserNameUnique(userName)) {
                return false;
            }
            for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                if (!organization.getUserAccountDirectory().checkUserNameUnique(userName)) {
                    return false;
                }
            }
        }
        for (Network network : rootNetwork.getNetworkDirectory().getNetworkList()) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (!enterprise.getUserAccountDirectory().checkUserNameUnique(userName)) {
                    return false;
                }

                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    if (!organization.getUserAccountDirectory().checkUserNameUnique(userName)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private GlobalSystem() {
        super(null);
        rootNetwork = new Network("GlobalSystem", Network.NetworkType.Global.getValue());

    }

    public List<Network> getAllNetworks() {
        List<Network> allNetworks = new ArrayList<>();
        List<Network> firstLevelNetwork = rootNetwork.getNetworkDirectory().getNetworkList();
        allNetworks.add(rootNetwork);
        if (firstLevelNetwork.size() > 0) {
            for (Network region : firstLevelNetwork) {
                allNetworks.add(region);
                if (region.getNetworkDirectory().getNetworkList().size() > 0) {
                    for (Network country : region.getNetworkDirectory().getNetworkList()) {
                        allNetworks.add(country);
                        if (country.getNetworkDirectory().getNetworkList().size() > 0) {
                            for (Network state : country.getNetworkDirectory().getNetworkList()) {
                                allNetworks.add(state);
                                if (state.getNetworkDirectory().getNetworkList().size() > 0) {
                                    for (Network city : state.getNetworkDirectory().getNetworkList()) {
                                        allNetworks.add(city);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return allNetworks;
    }

    public Network getRootNetwork() {
        return rootNetwork;
    }

    public void setRootNetwork(Network rootNetwork) {
        this.rootNetwork = rootNetwork;
    }

    @Override
    public ArrayList<Role> getSupportedRole(Enterprise enterprise) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
