/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business;

import com.itam.business.employee.Employee;
import com.itam.business.organization.AdminOrganization;
import com.itam.business.role.SysAdminRole;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class InitializeSystem {
    public static GlobalSystem configure()
    {
        GlobalSystem system = GlobalSystem.getInstance();
        Employee employee = system.getPersonDirectory().createEmployee("Abhishek","AV","abhishekav9@gmail.com");
        system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", new SysAdminRole(), employee);
        return system;
    }
}