/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business;

import com.itam.business.employee.Employee;
import com.itam.business.organization.Organization;
import com.itam.business.organization.SupplierOrganization;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class PersonDirectory {

    private List<Person> personList;

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

    public PersonDirectory() {
        personList = new ArrayList();
    }

    public Person addPerson(Organization organization) {
        Person p = null;
        //if (!(organization instanceof SupplierOrganization)) {
            p = new Employee();
            personList.add(p);
        //}
        return p;
    }

    public Employee createEmployee(String firstName, String lastName, String emailId) {
        Employee employee = new Employee();
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmailId(emailId);
        personList.add(employee);
        return employee;
    }

    public void removePerson(Person person) {
        personList.remove(person);
    }
}
