/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.asset;

import com.itam.business.enterprise.Enterprise;
import com.itam.business.organization.Organization;
import com.itam.business.product.Product;
import com.itam.business.useraccounts.UserAccount;
import com.itam.business.work.PurchaseOrder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public abstract class Asset {

    private String assetId;
    private boolean isDeallocationRequested = false;
    private Product product;
    private PurchaseOrder purchaseOrder;
    private Enterprise enterprise;
    private AssetStatus status;
    private UserAccount currentUserAccount;
    private List<UserAccount> userAccountHistory;
    private List<Date> assignedDateHistory;
    private List<Date> releasedDateHistory;
    private Date releasedDate;
    private Date assignedDate;
    private Organization currentOrganization;
    private List<Organization> organizationHistory;
    private List<Date> maintenanceDateList;
    private AssetDirectory associatedAssetDirectory;

    public Asset() {
        status = AssetStatus.Spare;
        associatedAssetDirectory = new AssetDirectory();
        userAccountHistory = new ArrayList<>();
        assignedDateHistory = new ArrayList<>();
        releasedDateHistory = new ArrayList<>();
        organizationHistory = new ArrayList<>();

        int i = new Random().nextInt();
        i = (i < 0) ? (i * (-1)) : i;
        assetId = String.valueOf(i);
    }

    public enum AssetStatus {

        InMaintenance("In Maintenance"),
        SpareMaintenance("Spare - In Maintenance"),
        Spare("Spare"),
        InUse("In Use"),
        Disposed("Disposed");

        private String value;

        private AssetStatus(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }

        public String getValue() {
            return value;
        }
    }

    public void setIsDeallocationRequested(boolean isDeallocationRequested) {
        this.isDeallocationRequested = isDeallocationRequested;
    }

    public boolean isIsDeallocationRequested() {
        return isDeallocationRequested;
    }

    public Date getReleasedDate() {
        return releasedDate;
    }

    public void setReleasedDate(Date releasedDate) {
        this.releasedDate = releasedDate;
    }

    public AssetDirectory getAssociatedAssetDirectory() {
        return associatedAssetDirectory;
    }

    public void setAssociatedAssetDirectory(AssetDirectory associatedAssetDirectory) {
        this.associatedAssetDirectory = associatedAssetDirectory;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public List<Date> getMaintenanceDateList() {
        return maintenanceDateList;
    }

    public void setMaintenanceDateList(List<Date> maintenanceDateList) {
        this.maintenanceDateList = maintenanceDateList;
    }

    public List<Date> getAssignedDateHistory() {
        return assignedDateHistory;
    }

    public List<Date> getReleasedDateHistory() {
        return releasedDateHistory;
    }

    public Date getAssignedDate() {
        return assignedDate;
    }

    public void setAssignedDate(Date assignedDate) {
        this.assignedDate = assignedDate;
    }

    public Organization getCurrentOrganization() {
        return currentOrganization;
    }

    public void setCurrentOrganization(Organization currentOrganization) {
        this.currentOrganization = currentOrganization;
    }

    public List<Organization> getOrganizationHistory() {
        return organizationHistory;
    }

    public void setOrganizationHistory(List<Organization> organizationHistory) {
        this.organizationHistory = organizationHistory;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public AssetStatus getStatus() {
        return status;
    }

    public void setStatus(AssetStatus status) {
        this.status = status;
    }

    public UserAccount getCurrentUserAccount() {
        return currentUserAccount;
    }

    public void setCurrentUserAccount(UserAccount currentUserAccount) {
        this.currentUserAccount = currentUserAccount;
    }

    public List<UserAccount> getUserAccountHistory() {
        return userAccountHistory;
    }

    public void setUserAccountHistory(List<UserAccount> userAccountHistory) {
        this.userAccountHistory = userAccountHistory;
    }

    //Assign Assets
    public void assignAsset(UserAccount userAccount, Organization organization) {
        Date d = new Date();
        this.status = AssetStatus.InUse;
        this.assignedDate = d;
        this.assignedDateHistory.add(d);
        this.currentUserAccount = userAccount;
        this.userAccountHistory.add(userAccount);
        this.organizationHistory.add(organization);
        this.currentOrganization = organization;

    }

    public void associateAssets(UserAccount userAccount, List<Asset> assignChosenAsset, Organization organization) {
        Date d = new Date();
        this.associatedAssetDirectory.getAssetList().addAll(assignChosenAsset);
        if (this instanceof HardwareAsset) {
            userAccount.getHardwareAssetDirectory().getAssetList().add(this);
            if (assignChosenAsset.size() > 0) {
                for (Asset softwareAsset : assignChosenAsset) {
                    softwareAsset.setStatus(Asset.AssetStatus.InUse);
                    softwareAsset.setCurrentUserAccount(userAccount);
                    softwareAsset.getUserAccountHistory().add(userAccount);
                    softwareAsset.getAssociatedAssetDirectory().getAssetList().add(this);
                    userAccount.getSoftwareAssetDirectory().getAssetList().add(softwareAsset);
                    softwareAsset.getOrganizationHistory().add(organization);
                    softwareAsset.setAssignedDate(d);
                    softwareAsset.getAssignedDateHistory().add(d);
                    softwareAsset.setCurrentOrganization(organization);
                }
            }
        } else {
            for (Asset hardwareAsset : assignChosenAsset) {
                hardwareAsset.associatedAssetDirectory.getAssetList().add(this);
            }
            userAccount.getSoftwareAssetDirectory().getAssetList().add(this);
        }
    }

    public void releaseAssociatedAsset() {
        this.status = AssetStatus.Spare;
        this.isDeallocationRequested=false;
        Date d = new Date();
        this.getReleasedDateHistory().add(d);
        this.setReleasedDate(d);
        this.currentUserAccount = null;
        this.currentOrganization = null;
        Asset asset = null;
        ListIterator<Asset> li = this.associatedAssetDirectory.getAssetList().listIterator();
        while (li.hasNext()) {
            asset = li.next();
            if (asset != null) {
                if (this instanceof SoftwareAsset) {
                    asset.associatedAssetDirectory.getAssetList().remove(this);
                    li.remove();
                    break;

                } else if (this instanceof HardwareAsset) {
                    li.remove();
                    asset.setStatus(AssetStatus.Spare);
                    asset.setReleasedDate(d);
                    asset.getReleasedDateHistory().add(d);
                    asset.associatedAssetDirectory.getAssetList().remove(this);
                    asset.currentUserAccount = null;
                    asset.currentOrganization = null;

                }
            }
        }
    }
}
