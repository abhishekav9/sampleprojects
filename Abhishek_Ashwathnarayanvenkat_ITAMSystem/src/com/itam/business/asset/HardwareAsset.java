/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.asset;

import java.util.Date;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class HardwareAsset extends Asset {

    private String serialNumber;
    private Date warrantyExpiration;
    private String ipAddress;
    private String macAddress;

    public HardwareAsset() {
        super();
    }

    @Override
    public String toString() {
        return super.getAssetId(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getWarrantyExpiration() {
        return warrantyExpiration;
    }

    public void setWarrantyExpiration(Date warrantyExpiration) {
        this.warrantyExpiration = warrantyExpiration;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
    
}
