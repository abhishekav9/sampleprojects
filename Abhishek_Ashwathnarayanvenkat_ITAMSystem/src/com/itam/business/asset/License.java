/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.asset;

import com.itam.business.enterprise.Enterprise;
import java.util.Date;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class License {
    private String licenseKey;
    private String licenseType;
    private int noOfUsers;
    private int noOfWorkstation;
    private Enterprise enterprise;
    private Date trialEndDate;
    private Date trialStartDate;
    private int timeBasedHrs;

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }
    
    public String getLicenseKey() {
        return licenseKey;
    }

    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

    public int getNoOfUsers() {
        return noOfUsers;
    }

    public void setNoOfUsers(int noOfUsers) {
        this.noOfUsers = noOfUsers;
    }

    public int getNoOfWorkstation() {
        return noOfWorkstation;
    }

    public void setNoOfWorkstation(int noOfWorkstation) {
        this.noOfWorkstation = noOfWorkstation;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public Date getTrialEndDate() {
        return trialEndDate;
    }

    public void setTrialEndDate(Date trialEndDate) {
        this.trialEndDate = trialEndDate;
    }

    public Date getTrialStartDate() {
        return trialStartDate;
    }

    public void setTrialStartDate(Date trialStartDate) {
        this.trialStartDate = trialStartDate;
    }

    public int getTimeBasedHrs() {
        return timeBasedHrs;
    }

    public void setTimeBasedHrs(int timeBasedHrs) {
        this.timeBasedHrs = timeBasedHrs;
    }
    
    
    public enum LicenseType{
        
        SingleUser("Single User"),
        ConcurrentUser("Concurrent User"),
        Workstation("Workstation"),
        GroupWorkStation("Group WorkStation"),
        Site("Site"),
        Trial("Trial"),
        Perpetual("Perpetual");
        
        private String value;
        private LicenseType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
