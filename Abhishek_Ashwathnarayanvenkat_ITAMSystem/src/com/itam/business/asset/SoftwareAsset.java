/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.asset;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class SoftwareAsset extends Asset {

    //private int id;
    //private static int count = 1;
    //private String type;

    private License license;

    public License getLicense() {
        return license;
    }

    public void setLicense(License license) {
        this.license = license;
    }

    @Override
    public String toString() {
        return super.getAssetId(); //To change body of generated methods, choose Tools | Templates.
    }

//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
    public SoftwareAsset() {
        super();
    }

}
