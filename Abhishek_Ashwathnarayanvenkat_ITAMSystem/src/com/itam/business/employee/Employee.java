/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.employee;

import com.itam.business.Person;
import java.util.Random;

/**
 *
 * @author raunak
 */
public class Employee extends Person {

    private String firstName;
    private String lastName;
    private int id;

    public Employee() {
        int i = new Random().nextInt();
        id=i<0?(i*(-1)):i;

    }

    public int getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return firstName+" "+lastName;
    }

}
