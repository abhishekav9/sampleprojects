/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.enterprise;

import com.itam.business.organization.Organization;
import com.itam.business.organization.OrganizationDirectory;
import com.itam.business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class Enterprise extends Organization{

    OrganizationDirectory organizationDirectory;
    private Location location;

    public Enterprise(String name) {
        super(name);
        location = new Location();
        this.organizationDirectory = new OrganizationDirectory();
    }

    @Override
    public ArrayList<Role> getSupportedRole(Enterprise enterprise) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public enum EnterpriseType{
        
        ITOperation("IT Operation");
        private String value;
        
        private EnterpriseType (String value)
        {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value; //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }
}