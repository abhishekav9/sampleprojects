/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.enterprise;

import com.itam.business.asset.Asset;
import com.itam.business.asset.HardwareAsset;
import com.itam.business.asset.License;
import com.itam.business.asset.SoftwareAsset;
import com.itam.business.work.PurchaseOrder;
import com.itam.business.product.*;
import com.itam.business.product.HardwareProduct;
import com.itam.business.product.LaptopDesktopServerElement;
import com.itam.business.organization.AdminOrganization;
import com.itam.business.organization.DevelopmentOrganization;
import com.itam.business.organization.FinanceOrganization;
import com.itam.business.organization.ITAssetManagementOrganization;
import com.itam.business.organization.ITSecurityOrganization;
import com.itam.business.organization.ITSupportOrganization;
import com.itam.business.organization.SalesOrganization;
import com.itam.business.organization.SupplierOrganization;
import com.itam.business.product.Product;
import com.itam.business.product.SoftwareProduct;
import com.itam.business.role.EnterpriseAdminRole;
import com.itam.business.role.Role;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class ITOperationsEnterprise extends Enterprise {

    private int id;
    //private static int count = 1;
    private Map<SoftwareProduct, List<SoftwareAsset>> softwareProductAssetMap = new HashMap();
    private Map<HardwareProduct, List<HardwareAsset>> hardwareProductAssetMap = new HashMap();
    private List<Product> enterpriseSoftwareProductCatalog = new ArrayList();
    private List<Product> enterpriseHardwareProductCatalog = new ArrayList();
    //private AssetDirectory 
    private List<Asset> enterpriseSoftwareAssetCatalog = new ArrayList();
    private List<Asset> enterpriseHardwareAssetCatalog = new ArrayList();
    private List<PurchaseOrder> enterprisePurchaseOrderCatalog = new ArrayList();

    public Map<HardwareProduct, List<HardwareAsset>> getHardwareProductAssetMap() {
        return hardwareProductAssetMap;
    }

    public Map<SoftwareProduct, List<SoftwareAsset>> getSoftwareProductAssetMap() {
        return softwareProductAssetMap;
    }

    public List<Asset> getEnterpriseHardwareAssetCatalog() {
        return enterpriseHardwareAssetCatalog;
    }

    public void setEnterpriseHardwareAssetCatalog(List<Asset> enterpriseHardwareAssetCatalog) {
        this.enterpriseHardwareAssetCatalog = enterpriseHardwareAssetCatalog;
    }

    public List<Asset> getEnterpriseSoftwareAssetCatalog() {
        return enterpriseSoftwareAssetCatalog;
    }

    public void setEnterpriseSoftwareAssetCatalog(List<Asset> enterpriseSoftwareAssetCatalog) {
        this.enterpriseSoftwareAssetCatalog = enterpriseSoftwareAssetCatalog;
    }

    public List<Product> getEnterpriseHardwareProductCatalog() {
        return enterpriseHardwareProductCatalog;
    }

    public void setEnterpriseHardwareProductCatalog(List<Product> enterpriseHardwareProductCatalog) {
        this.enterpriseHardwareProductCatalog = enterpriseHardwareProductCatalog;
    }

    public List<Product> getEnterpriseSoftwareProductCatalog() {
        return enterpriseSoftwareProductCatalog;
    }

    public List<PurchaseOrder> getEnterprisePurchaseOrderCatalog() {
        return enterprisePurchaseOrderCatalog;
    }

    public List<Asset> getAssetsOfAnOrganization(String typeString) {
        List<Asset> assetList = new ArrayList<>();
        if (enterpriseHardwareAssetCatalog.size() > 0) {
            for (Asset asset : enterpriseHardwareAssetCatalog) {
                if (asset.getStatus().equals(Asset.AssetStatus.InUse)) {
                    if (asset.getCurrentOrganization().getName().equals(typeString)) {
                        assetList.add(asset);
                    }
                }
            }
        }
        if (enterpriseSoftwareAssetCatalog.size() > 0) {
            for (Asset asset : enterpriseSoftwareAssetCatalog) {
                if (asset.getStatus().equals(Asset.AssetStatus.InUse)) {
                    if (asset.getCurrentOrganization().getName().equals(typeString)) {
                        assetList.add(asset);
                    }
                }
            }
        }
        return assetList;
    }

    public ITOperationsEnterprise(String name) {
        super(name);
        int i = new Random().nextInt();
        id = i < 0 ? (-1*i) : i;
        //count++;
        AdminOrganization adminOrganization = (AdminOrganization) organizationDirectory.createOrganization(Type.AdminOrganization);
        DevelopmentOrganization developmentOrg = (DevelopmentOrganization) organizationDirectory.createOrganization(Type.DevelopmentOrganization);
        FinanceOrganization financeOrganization = (FinanceOrganization) organizationDirectory.createOrganization(Type.FinanceOrganization);
        ITAssetManagementOrganization iTAssetManagementOrg = (ITAssetManagementOrganization) organizationDirectory.createOrganization(Type.ITAssetManagementOrg);
        ITSecurityOrganization iTSecurityOrganization = (ITSecurityOrganization) organizationDirectory.createOrganization(Type.ITSecurityOrganization);
        ITSupportOrganization iTServiceDeskOrg = (ITSupportOrganization) organizationDirectory.createOrganization(Type.ITSupportOrganization);
        SalesOrganization salesOrganization = (SalesOrganization) organizationDirectory.createOrganization(Type.SalesOrganization);
        SupplierOrganization supplierOrganization = (SupplierOrganization) organizationDirectory.createOrganization(Type.SupplierOrganization);

        initializeProductsForEnterprise();
    }

    private void initializeProductsForEnterprise() {
        HardwareProduct hardwareProduct1 = new HardwareProduct();
        LaptopDesktopServerElement laptopDesktopServerElement = new LaptopDesktopServerElement();
        laptopDesktopServerElement.setLaptopHardDiskCapacityInGb(1000);
        laptopDesktopServerElement.setLaptopNoOfProcessors(4);
        laptopDesktopServerElement.setLaptopProcessorName("Intel(R) Pentium(R) CPU G2010");
        laptopDesktopServerElement.setLaptopProcessorSpeedInGhz(2.80f);
        laptopDesktopServerElement.setLaptopRamInGb(8);
        hardwareProduct1.setType(HardwareProduct.HardwareType.Laptop.getValue());
        hardwareProduct1.setHardwareElement(laptopDesktopServerElement);
        hardwareProduct1.setManufacturer("Dell");
        hardwareProduct1.setModel("Inspiron");
        hardwareProduct1.setVersion("15");
        hardwareProduct1.setProductNumber(String.valueOf(Math.random() * 1000000));
        hardwareProduct1.setName("Dell Inspiron 15 Laptop");
        hardwareProduct1.setProductCost(566);

        HardwareProduct hardwareProduct3 = new HardwareProduct();
        FirewallElement firewallElement = new FirewallElement();
        firewallElement.setFirewallMaxConnections(1000);
        firewallElement.setFirewallThroughPut(700);
        hardwareProduct3.setType(HardwareProduct.HardwareType.Firewall.getValue());
        hardwareProduct3.setHardwareElement(firewallElement);
        hardwareProduct3.setManufacturer("Cisco");
        hardwareProduct3.setModel("RV220W");
        hardwareProduct3.setVersion("5");
        hardwareProduct3.setProductNumber(String.valueOf(Math.random() * 1000000));
        hardwareProduct3.setName("Cisco RV220W 5 Firewall");
        hardwareProduct3.setProductCost(700);

        HardwareProduct hardwareProduct2 = new HardwareProduct();
        PrinterElement printerElement = new PrinterElement();
        printerElement.setPrinterPaperCapacity(1000);
        printerElement.setPrinterScannerDpi(560);
        hardwareProduct2.setType(HardwareProduct.HardwareType.Printer.getValue());
        hardwareProduct2.setHardwareElement(printerElement);
        hardwareProduct2.setManufacturer("HP");
        hardwareProduct2.setModel("InstaJet");
        hardwareProduct2.setVersion("500S");
        hardwareProduct2.setProductNumber(String.valueOf(Math.random() * 1000000));
        hardwareProduct2.setName("HP Instajet 500S");
        hardwareProduct2.setProductCost(355);

        HardwareProduct hardwareProduct4 = new HardwareProduct();
        LaptopDesktopServerElement laptopDesktopServerElement2 = new LaptopDesktopServerElement();
        laptopDesktopServerElement2.setLaptopHardDiskCapacityInGb(10000);
        laptopDesktopServerElement2.setLaptopNoOfProcessors(10);
        laptopDesktopServerElement2.setLaptopProcessorName("Intel(R) Xeon Haswell");
        laptopDesktopServerElement2.setLaptopProcessorSpeedInGhz(3.40f);
        laptopDesktopServerElement2.setLaptopRamInGb(128);
        hardwareProduct4.setType(HardwareProduct.HardwareType.Server.getValue());
        hardwareProduct4.setHardwareElement(laptopDesktopServerElement2);
        hardwareProduct4.setManufacturer("Lenovo");
        hardwareProduct4.setModel("ThinkServer");
        hardwareProduct4.setVersion("TS140");
        hardwareProduct4.setProductNumber(String.valueOf(Math.random() * 1000000));
        hardwareProduct4.setName("Lenovo ThinkServer TS140");
        hardwareProduct4.setProductCost(1200);

        HardwareAsset hardwareAsset1 = new HardwareAsset();
        hardwareAsset1.setProduct(hardwareProduct4);
        hardwareAsset1.setAssetId(String.valueOf(new Random().nextInt()));
        Random r = new Random();
        String ipAddress=r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256);
        ipAddress = ipAddress.replaceAll("-", "");
        hardwareAsset1.setIpAddress(ipAddress);
        hardwareAsset1.setSerialNumber("C0ASD12G");
        hardwareAsset1.setWarrantyExpiration(new Date());

        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setProduct(hardwareProduct4);
        purchaseOrder.setQuantity(1);
        purchaseOrder.setStatus(PurchaseOrder.PurchaseOrderStatus.MappedToAsset.getValue());
        purchaseOrder.setAmt(1200);
        purchaseOrder.setPurchaseDate(new Date());
        enterprisePurchaseOrderCatalog.add(purchaseOrder);
        hardwareAsset1.setPurchaseOrder(purchaseOrder);
        enterpriseHardwareAssetCatalog.add(hardwareAsset1);

        hardwareProductAssetMap.put(hardwareProduct4, new ArrayList<HardwareAsset>());
        hardwareProductAssetMap.put(hardwareProduct3, new ArrayList<HardwareAsset>());
        hardwareProductAssetMap.put(hardwareProduct2, new ArrayList<HardwareAsset>());
        hardwareProductAssetMap.put(hardwareProduct1, new ArrayList<HardwareAsset>());
        List mappedAsset = hardwareProductAssetMap.get(hardwareProduct4);
        mappedAsset.add(hardwareAsset1);
        hardwareProductAssetMap.put(hardwareProduct4, mappedAsset);

        enterpriseHardwareProductCatalog.add(hardwareProduct4);
        enterpriseHardwareProductCatalog.add(hardwareProduct2);
        enterpriseHardwareProductCatalog.add(hardwareProduct1);
        enterpriseHardwareProductCatalog.add(hardwareProduct3);

        SoftwareProduct softwareProduct1 = new SoftwareProduct();
        softwareProduct1.setManufacturer("Apache");
        softwareProduct1.setModel("Tomcat");
        softwareProduct1.setName("Tomcat Server");
        softwareProduct1.setSizeInMb(35);
        softwareProduct1.setType(SoftwareProduct.SoftwareType.WebServer.getValue());
        softwareProduct1.setVersion("5");
        softwareProduct1.setCategory(SoftwareProduct.SoftwareCategory.DevelopmentApplication.getValue());
        softwareProduct1.setProductCost(12);

        SoftwareProduct softwareProduct2 = new SoftwareProduct();
        softwareProduct2.setManufacturer("Microsoft");
        softwareProduct2.setModel("Windows");
        softwareProduct2.setName("Win 7 OS");
        softwareProduct2.setSizeInMb(2500);
        softwareProduct2.setType(SoftwareProduct.SoftwareType.OS.getValue());
        softwareProduct2.setVersion("7");
        softwareProduct2.setCategory(SoftwareProduct.SoftwareCategory.CommonApplication.getValue());
        softwareProduct2.setProductCost(1000);

        SoftwareProduct softwareProduct3 = new SoftwareProduct();
        softwareProduct3.setManufacturer("Microsoft");
        softwareProduct3.setModel("Office");
        softwareProduct3.setName("Office Suite");
        softwareProduct3.setSizeInMb(1500);
        softwareProduct3.setType(SoftwareProduct.SoftwareType.Application.getValue());
        softwareProduct3.setVersion("7");
        softwareProduct3.setCategory(SoftwareProduct.SoftwareCategory.CommonApplication.getValue());
        softwareProduct3.setProductCost(789);

        SoftwareProduct softwareProduct4 = new SoftwareProduct();
        softwareProduct4.setManufacturer("Oracle");
        softwareProduct4.setModel("CRM OnDemand");
        softwareProduct4.setName("Oracle CRM OnDemand");
        softwareProduct4.setSizeInMb(10000);
        softwareProduct4.setType(SoftwareProduct.SoftwareType.Application.getValue());
        softwareProduct4.setVersion("3.05f");
        softwareProduct4.setCategory(SoftwareProduct.SoftwareCategory.SalesApplication.getValue());
        softwareProduct4.setProductCost(3000);

        SoftwareProduct softwareProduct5 = new SoftwareProduct();
        softwareProduct5.setManufacturer("Oracle");
        softwareProduct5.setModel("10g");
        softwareProduct5.setName("Oracle 10g DB");
        softwareProduct5.setSizeInMb(1000);
        softwareProduct5.setType(SoftwareProduct.SoftwareType.Database.getValue());
        softwareProduct5.setVersion("5");
        softwareProduct5.setCategory(SoftwareProduct.SoftwareCategory.DevelopmentApplication.getValue());
        softwareProduct5.setProductCost(1200);

        SoftwareAsset softwareAsset = new SoftwareAsset();
        softwareAsset.setProduct(softwareProduct5);
        
        PurchaseOrder purchaseOrder1 = new PurchaseOrder();
        purchaseOrder1.setProduct(softwareProduct5);
        purchaseOrder1.setQuantity(1);
        purchaseOrder1.setStatus(PurchaseOrder.PurchaseOrderStatus.MappedToAsset.getValue());
        purchaseOrder1.setAmt(1200);
        purchaseOrder1.setPurchaseDate(new Date());

        softwareAsset.setPurchaseOrder(purchaseOrder);
        License license = new License();
        license.setLicenseKey("123-HG6-S6G-75V");
        license.setLicenseType(License.LicenseType.SingleUser.getValue());
        softwareAsset.setLicense(license);
        enterpriseSoftwareAssetCatalog.add(softwareAsset);
        enterprisePurchaseOrderCatalog.add(purchaseOrder1);

        softwareProductAssetMap.put(softwareProduct5, new ArrayList<SoftwareAsset>());
        softwareProductAssetMap.put(softwareProduct4, new ArrayList<SoftwareAsset>());
        softwareProductAssetMap.put(softwareProduct3, new ArrayList<SoftwareAsset>());
        softwareProductAssetMap.put(softwareProduct2, new ArrayList<SoftwareAsset>());
        softwareProductAssetMap.put(softwareProduct1, new ArrayList<SoftwareAsset>());
        softwareProductAssetMap.get(softwareProduct5).add(softwareAsset);

        enterpriseSoftwareProductCatalog.add(softwareProduct5);
        enterpriseSoftwareProductCatalog.add(softwareProduct4);
        enterpriseSoftwareProductCatalog.add(softwareProduct3);
        enterpriseSoftwareProductCatalog.add(softwareProduct2);
        enterpriseSoftwareProductCatalog.add(softwareProduct1);
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

    @Override
    public ArrayList<Role> getSupportedRole(Enterprise enterprise) {
        List<Role> roleList = new ArrayList<>();
        roleList.add(new EnterpriseAdminRole());
        return (ArrayList<Role>) roleList;
    }
}
