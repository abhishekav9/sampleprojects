/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.enterprise;

import com.itam.business.network.Network;


/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class Location {
    private String streetName;
    private String number;
    private String City;
    private Network state;
    private Network country;
    private Network region;

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public Network getState() {
        return state;
    }

    public void setState(Network state) {
        this.state = state;
    }

    public Network getCountry() {
        return country;
    }

    public void setCountry(Network country) {
        this.country = country;
    }

    public Network getRegion() {
        return region;
    }

    public void setRegion(Network region) {
        this.region = region;
    }
    
    
    

}
