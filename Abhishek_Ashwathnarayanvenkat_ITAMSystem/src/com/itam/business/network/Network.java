/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.network;

import com.itam.business.enterprise.EnterpriseDirectory;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class Network {

    private String name;
    private String networkType;
    private NetworkDirectory networkDirectory;
    private EnterpriseDirectory enterpriseDirectory;

    public Network(String name, String type) {
        this.name = name;
        networkType = type;
        networkDirectory = new NetworkDirectory();
        enterpriseDirectory = new EnterpriseDirectory();
    }

    public enum NetworkRegions {
        APAC("Asia Pacific"),
        EMEA("Europe, Middle East, Africa"),
        AMER("Americas");

        private String value;

        private NetworkRegions(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum NetworkType {

        Global("Global"),
        Region("Region"),
        Country("Country"),
        State("State"),
        City("City");

        private String value;

        private NetworkType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public Network findCityNetwork(String regionName, String countryName, String stateName, String cityName) {
        Network rootNetwork = this;
        for (Network regionNetwork : rootNetwork.getNetworkDirectory().getNetworkList()) {
            for (Network countryNetwork : regionNetwork.getNetworkDirectory().getNetworkList()) {
                for (Network stateNetwork : countryNetwork.getNetworkDirectory().getNetworkList()) {
                    for (Network cityNetwork : stateNetwork.getNetworkDirectory().getNetworkList()) {
                        if (regionNetwork.getName().equalsIgnoreCase(regionName) && countryNetwork.getName().equalsIgnoreCase(countryName) && stateNetwork.getName().equalsIgnoreCase(stateName) && cityNetwork.getName().equalsIgnoreCase(cityName)) {
                            return cityNetwork;
                        }
                    }
                }
            }
        }
        return null;
    }

    public Network findStateNetwork(String regionName, String countryName, String stateName) {
        Network rootNetwork = this;
        for (Network regionNetwork : rootNetwork.getNetworkDirectory().getNetworkList()) {
            for (Network countryNetwork : regionNetwork.getNetworkDirectory().getNetworkList()) {
                for (Network stateNetwork : countryNetwork.getNetworkDirectory().getNetworkList()) {
                    if (regionNetwork.getName().equalsIgnoreCase(regionName) && countryNetwork.getName().equalsIgnoreCase(countryName) && stateNetwork.getName().equalsIgnoreCase(stateName)) {
                        return stateNetwork;
                    }
                }
            }
        }
        return null;
    }

    public Network findCountryNetwork(String regionName, String countryName) {
        Network rootNetwork = this;
        for (Network regionNetwork : rootNetwork.getNetworkDirectory().getNetworkList()) {
            for (Network countryNetwork : regionNetwork.getNetworkDirectory().getNetworkList()) {
                if (regionNetwork.getName().equalsIgnoreCase(regionName) && countryNetwork.getName().equalsIgnoreCase(countryName)) {
                    return countryNetwork;
                }
            }
        }
        return null;
    }

    public Network findRegionNetwork(String regionName) {
        Network rootNetwork = this;
        for (Network regionNetwork : rootNetwork.getNetworkDirectory().getNetworkList()) {
            if (regionNetwork.getName().equalsIgnoreCase(regionName)) {
                return regionNetwork;
            }
        }
        return null;
    }

    public boolean isPresent(String name) {
        for (Network network : networkDirectory.getNetworkList()) {
            if (network.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public NetworkDirectory getNetworkDirectory() {
        return networkDirectory;
    }

    public void setNetworkDirectory(NetworkDirectory networkDirectory) {
        this.networkDirectory = networkDirectory;
    }

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

}
