/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.network;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class NetworkDirectory {
    private List<Network> networkList;

    public NetworkDirectory() {
    networkList = new ArrayList<>();
    }
    
    public void createAndAddNetwork(String name, String type){
        Network network = new Network(name, type);
        networkList.add(network);
    
    }
    public boolean isPresent(Network network)
    {
        for(Network net:networkList)
        {
            if(net==network)
            {
                return true;
            }
        }
        return false;
    }

    public List<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(List<Network> networkList) {
        this.networkList = networkList;
    }

    @Override
    public String toString() {
        return super.toString();
    }
    
}
