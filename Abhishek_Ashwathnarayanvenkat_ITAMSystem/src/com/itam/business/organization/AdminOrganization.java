/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.organization;

import com.itam.business.enterprise.Enterprise;
import com.itam.business.role.EnterpriseAdminRole;
import com.itam.business.role.Role;
import com.itam.business.role.SysAdminRole;
import java.util.ArrayList;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class AdminOrganization extends Organization{

    public AdminOrganization() {
        super(Type.AdminOrganization.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole(Enterprise enterprise) {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new EnterpriseAdminRole());
        roles.add(new SysAdminRole());
        return roles;
    }
    
}
