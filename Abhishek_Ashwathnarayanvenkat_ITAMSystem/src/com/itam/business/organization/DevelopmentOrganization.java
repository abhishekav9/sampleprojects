/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.organization;

import com.itam.business.product.Product;
import com.itam.business.product.SoftwareProduct;
import com.itam.business.enterprise.Enterprise;
import com.itam.business.enterprise.ITOperationsEnterprise;
import com.itam.business.role.DevManagerRole;
import com.itam.business.role.DevStaffRole;
import com.itam.business.role.Role;
import com.itam.business.role.SeniorDevStaffRole;
import java.util.ArrayList;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class DevelopmentOrganization extends Organization {

    public DevelopmentOrganization() {
        super(Type.DevelopmentOrganization.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole(Enterprise enterprise) {

        ArrayList<Role> roles = new ArrayList<>();
        DevStaffRole devStaffRole = new DevStaffRole();
        ITOperationsEnterprise iTOperationsEnterprise = (ITOperationsEnterprise) enterprise;
        for (Product softwareProduct : iTOperationsEnterprise.getEnterpriseSoftwareProductCatalog()) {
           if(((SoftwareProduct)softwareProduct).getCategory().equals(SoftwareProduct.SoftwareCategory.DevelopmentApplication.getValue()))
           {
               devStaffRole.getSoftwareProductBasePackage().add((SoftwareProduct)softwareProduct);
           }
        }
        DevManagerRole devManagerRole = new DevManagerRole();
        
        for (Product softwareProduct : iTOperationsEnterprise.getEnterpriseSoftwareProductCatalog()) {
           if(((SoftwareProduct)softwareProduct).getCategory().equals(SoftwareProduct.SoftwareCategory.DevelopmentApplication.getValue()) || ((SoftwareProduct)softwareProduct).getCategory().equals(SoftwareProduct.SoftwareCategory.CommonApplication.getValue()))
           {
               devManagerRole.getSoftwareProductBasePackage().add((SoftwareProduct)softwareProduct);
           }
        }
        SeniorDevStaffRole seniorDevStaffRole = new SeniorDevStaffRole();
        for (Product softwareProduct : iTOperationsEnterprise.getEnterpriseSoftwareProductCatalog()) {
           if(((SoftwareProduct)softwareProduct).getCategory().equals(SoftwareProduct.SoftwareCategory.DevelopmentApplication.getValue()) || ((SoftwareProduct)softwareProduct).getCategory().equals(SoftwareProduct.SoftwareCategory.CommonApplication.getValue()))
           {
               seniorDevStaffRole.getSoftwareProductBasePackage().add((SoftwareProduct)softwareProduct);
           }
        }
        
        roles.add(devStaffRole);
        roles.add(devManagerRole);
        roles.add(seniorDevStaffRole);
        return roles;
    }

}
