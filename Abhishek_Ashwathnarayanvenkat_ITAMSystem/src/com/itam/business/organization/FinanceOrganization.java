/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.organization;

import com.itam.business.enterprise.Enterprise;
import com.itam.business.role.CFORole;
import com.itam.business.role.ITFinanceManagerRole;
import com.itam.business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class FinanceOrganization extends Organization{

    public FinanceOrganization() {
        super(Type.FinanceOrganization.getValue());
    }

    
    @Override
    public ArrayList<Role> getSupportedRole(Enterprise enterprise) {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new ITFinanceManagerRole());
        roles.add(new CFORole());
        return roles;
    }
    
}
