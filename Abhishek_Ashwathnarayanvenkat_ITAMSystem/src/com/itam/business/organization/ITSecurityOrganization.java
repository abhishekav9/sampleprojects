/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.organization;

import com.itam.business.enterprise.Enterprise;
import com.itam.business.role.ITPatchManagerRole;
import com.itam.business.role.ITSecurityManagerRole;
import com.itam.business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class ITSecurityOrganization extends Organization{

    public ITSecurityOrganization() {
        super(Type.ITSecurityOrganization.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole(Enterprise enterprise) {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new ITSecurityManagerRole());
        roles.add(new ITPatchManagerRole());
        return roles;
    }
    
}