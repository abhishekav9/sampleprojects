/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.organization;

import com.itam.business.enterprise.Enterprise;
import com.itam.business.role.ITSupportManagerRole;
import com.itam.business.role.ITSupportStaffRole;
import com.itam.business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class ITSupportOrganization extends Organization {

    public ITSupportOrganization() {
        super(Type.ITSupportOrganization.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole(Enterprise enterprise) {

        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new ITSupportManagerRole());
        roles.add(new ITSupportStaffRole());
        return roles;
    }

}
