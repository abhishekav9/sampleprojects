/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.organization;

import com.itam.business.PersonDirectory;
import com.itam.business.enterprise.Enterprise;
import com.itam.business.work.WorkQueue;
import com.itam.business.role.Role;
import com.itam.business.useraccounts.UserAccountDirectory;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public abstract class Organization {

    private String name;
    private WorkQueue workQueue;
    private PersonDirectory personDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter;

    public enum Type {

        ITAssetManagementOrg("IT-Asset Management Organization"),
        ITSupportOrganization("IT-Support Organization"),
        FinanceOrganization("Finance Organization"),
        DevelopmentOrganization("Development Organization"),
        ITSecurityOrganization("IT-Security Organization"),
        SalesOrganization("Sales Organization"),
        AdminOrganization("Admin Organization"),
        SupplierOrganization("Supplier Organization");

        private String value;

        private Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        personDirectory = new PersonDirectory();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        ++counter;
    }

    public abstract ArrayList<Role> getSupportedRole(Enterprise enterprise);

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    @Override
    public String toString() {
        return name;
    }
}
