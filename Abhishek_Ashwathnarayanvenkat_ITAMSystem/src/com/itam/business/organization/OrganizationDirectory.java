/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.organization;

import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Organization.Type type){
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.AdminOrganization.getValue())){
            organization = new AdminOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.ITAssetManagementOrg.getValue())){
            organization = new ITAssetManagementOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.DevelopmentOrganization.getValue())){
            organization = new DevelopmentOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.FinanceOrganization.getValue())){
            organization = new FinanceOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.SalesOrganization.getValue())){
            organization = new SalesOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.SupplierOrganization.getValue())){
            organization = new SupplierOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.ITSecurityOrganization.getValue()))
        {
            organization = new ITSecurityOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.ITSupportOrganization.getValue()))
        {
            organization = new ITSupportOrganization();
            organizationList.add(organization);
        }
        return organization;
    }
}