/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.product;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class FirewallElement extends HardwareElement{
    
    private int firewallThroughPut;
    private int firewallMaxConnections;

    public int getFirewallThroughPut() {
        return firewallThroughPut;
    }

    public void setFirewallThroughPut(int firewallThroughPut) {
        this.firewallThroughPut = firewallThroughPut;
    }

    public int getFirewallMaxConnections() {
        return firewallMaxConnections;
    }

    public void setFirewallMaxConnections(int firewallMaxConnections) {
        this.firewallMaxConnections = firewallMaxConnections;
    }
}
