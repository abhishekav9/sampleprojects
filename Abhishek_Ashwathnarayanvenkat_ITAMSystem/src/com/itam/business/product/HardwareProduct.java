/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.product;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class HardwareProduct extends Product{
private HardwareElement hardwareElement;
private String type;

    public HardwareElement getHardwareElement() {
        return hardwareElement;
    }

    public void setHardwareElement(HardwareElement hardwareElement) {
        this.hardwareElement = hardwareElement;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


 public enum HardwareType{
        
        Laptop("Laptop"),
        Server("Server"),
        Desktop("Desktop"),
        Router("Router"),
        Printer("Printer"),
        Scanner("Scanner"),
        Firewall("Firewall");
        
        private String value;
        private HardwareType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    @Override
    public String toString() {
        return super.getName(); //To change body of generated methods, choose Tools | Templates.
    }
 
 
    
}
