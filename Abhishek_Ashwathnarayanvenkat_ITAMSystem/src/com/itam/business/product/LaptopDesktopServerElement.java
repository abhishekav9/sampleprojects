/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.product;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class LaptopDesktopServerElement extends HardwareElement{
 private int laptopRamInGb;
private String laptopProcessorName;
private float laptopProcessorSpeedInGhz;
private int laptopHardDiskCapacityInGb;
private int laptopNoOfProcessors;

    public int getLaptopRamInGb() {
        return laptopRamInGb;
    }

    public void setLaptopRamInGb(int laptopRamInGb) {
        this.laptopRamInGb = laptopRamInGb;
    }

    public String getLaptopProcessorName() {
        return laptopProcessorName;
    }

    public void setLaptopProcessorName(String laptopProcessorName) {
        this.laptopProcessorName = laptopProcessorName;
    }

    public float getLaptopProcessorSpeedInGhz() {
        return laptopProcessorSpeedInGhz;
    }

    public void setLaptopProcessorSpeedInGhz(float laptopProcessorSpeedInGhz) {
        this.laptopProcessorSpeedInGhz = laptopProcessorSpeedInGhz;
    }

    public int getLaptopHardDiskCapacityInGb() {
        return laptopHardDiskCapacityInGb;
    }

    public void setLaptopHardDiskCapacityInGb(int laptopHardDiskCapacityInGb) {
        this.laptopHardDiskCapacityInGb = laptopHardDiskCapacityInGb;
    }

    public int getLaptopNoOfProcessors() {
        return laptopNoOfProcessors;
    }

    public void setLaptopNoOfProcessors(int laptopNoOfProcessors) {
        this.laptopNoOfProcessors = laptopNoOfProcessors;
    }

}
