/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.product;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class PrinterElement extends HardwareElement{
    private int printerScannerDpi;
    private int printerPaperCapacity;

    public int getPrinterScannerDpi() {
        return printerScannerDpi;
    }

    public void setPrinterScannerDpi(int printerScannerDpi) {
        this.printerScannerDpi = printerScannerDpi;
    }

    public int getPrinterPaperCapacity() {
        return printerPaperCapacity;
    }

    public void setPrinterPaperCapacity(int printerPaperCapacity) {
        this.printerPaperCapacity = printerPaperCapacity;
    }

}
