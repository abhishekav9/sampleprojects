/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.product;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class RouterElement extends HardwareElement{
    
    private int routerDataTransferRateInMb;

    public int getRouterDataTransferRateInMb() {
        return routerDataTransferRateInMb;
    }

    public void setRouterDataTransferRateInMb(int routerDataTransferRateInMb) {
        this.routerDataTransferRateInMb = routerDataTransferRateInMb;
    }

}
