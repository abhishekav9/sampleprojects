/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.product;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class ScannerElement extends HardwareElement{
        private int printerScannerDpi;
        private int maxScanAreaInCm;

    public int getPrinterScannerDpi() {
        return printerScannerDpi;
    }

    public void setPrinterScannerDpi(int printerScannerDpi) {
        this.printerScannerDpi = printerScannerDpi;
    }

    public int getMaxScanAreaInCm() {
        return maxScanAreaInCm;
    }

    public void setMaxScanAreaInCm(int maxScanAreaInCm) {
        this.maxScanAreaInCm = maxScanAreaInCm;
    }
        
        
}
