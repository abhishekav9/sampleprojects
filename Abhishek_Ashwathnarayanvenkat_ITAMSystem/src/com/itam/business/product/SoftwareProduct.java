/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.product;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class SoftwareProduct extends Product{
    private String type;
    private String category;
    private float sizeInMb;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getSizeInMb() {
        return sizeInMb;
    }

    public void setSizeInMb(float sizeInMb) {
        this.sizeInMb = sizeInMb;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return super.getName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public enum SoftwareCategory{
        
        SalesApplication("Sales Application"),
        DevelopmentApplication("Development Application"),
        CommonApplication("Common Application");
        
        private String value;
        private SoftwareCategory(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    public enum SoftwareType{
        
        Multimedia("Multimedia"),
        Application("Application"),
        Database("Database"),
        VirtualMachine("Virtual Machine"),
        OS("Operating System"),
        WebServer("Web Server"),
        Game("Game"),
        Development("Development"),
        Graphics("Graphics");
        
        private String value;
        private SoftwareType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
