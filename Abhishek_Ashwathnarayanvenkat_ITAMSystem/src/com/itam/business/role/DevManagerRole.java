/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.role;

import com.itam.business.useraccounts.UserAccount;
import com.itam.business.GlobalSystem;
import com.itam.business.enterprise.Enterprise;
import com.itam.business.organization.Organization;
import javax.swing.JPanel;
import userInterface.DevelopmentManagerRole.DevelopmentManagerWorkAreaJPanel;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class DevManagerRole extends Role {

    public DevManagerRole() {
        super(RoleType.DevelopmentManager.getValue());
    }

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, GlobalSystem ecoSystem) {
        return new DevelopmentManagerWorkAreaJPanel(userProcessContainer, account, enterprise, organization, ecoSystem);
    }

}
