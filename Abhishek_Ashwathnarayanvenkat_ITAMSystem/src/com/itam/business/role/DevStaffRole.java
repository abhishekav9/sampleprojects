/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.role;

import com.itam.business.useraccounts.UserAccount;
import com.itam.business.GlobalSystem;
import com.itam.business.enterprise.Enterprise;
import com.itam.business.organization.Organization;
import javax.swing.JPanel;
import userInterface.DevelopmentStaffRole.DevStaffWorkAreaJPanel;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class DevStaffRole extends Role{

    public DevStaffRole() {
        super(RoleType.DevelopmentStaff.getValue());
    }
    
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, GlobalSystem ecoSystem) {
        //ITOperationsEnterprise iTOperationsEnterprise = (ITOperationsEnterprise)enterprise;
        //iTOperationsEnterprise.getEnterpriseProductCatalog()
        return new DevStaffWorkAreaJPanel(userProcessContainer, account, organization, enterprise);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
