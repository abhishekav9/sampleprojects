/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.role;


import com.itam.business.useraccounts.UserAccount;
import com.itam.business.GlobalSystem;
import com.itam.business.enterprise.Enterprise;
import com.itam.business.organization.Organization;
import com.itam.business.product.SoftwareProduct;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public abstract class Role {
    String name;
    Set<SoftwareProduct> softwareProductBasePackage;
    
    public Role(String name) {
        this.name = name;
       softwareProductBasePackage = new HashSet<>();
    }
    
    
    public enum RoleType{
        
        ITPatchManager("Patch Manager"),
        CFORole("CFO Role"),
        DevelopmentStaff("Development Staff"),
        SeniorDevelopmentStaff("Senior Development Staff"),
        DevelopmentManager("Development Manager"),
        ITComplianceManager("Compliance Manager"),
        ITSupportManager("Support Manager"),
        ITSupportStaff("Support Staff"),
        ITSecurityManager("Security Manager"),
        EnterpriseAdmin("Enterprise Admin"),
        ITAssetManager("Asset Manager"),
        ITFinanceManager("Finance Manager"),
        SysAdmin("System Admin"),
        SalesManager("Sales Manager"),
        SupplierManager("Supplier Manager");
        
        private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public Set<SoftwareProduct> getSoftwareProductBasePackage() {
        return softwareProductBasePackage;
    }

    
//    public List<SoftwareProduct> getSoftwareProductBasePackage() {
//        return softwareProductBasePackage;
//    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, GlobalSystem ecoSystem);

    @Override
    public String toString() {
        return name;
    }
    
}