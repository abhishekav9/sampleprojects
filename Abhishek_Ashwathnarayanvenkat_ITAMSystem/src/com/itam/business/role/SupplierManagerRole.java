/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.role;

import com.itam.business.GlobalSystem;
import com.itam.business.enterprise.Enterprise;
import com.itam.business.organization.Organization;
import com.itam.business.useraccounts.UserAccount;
import javax.swing.JPanel;
import userInterface.SupplierRole.SupplierWorkAreaJPanel;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class SupplierManagerRole extends Role {

    public SupplierManagerRole() {
        super(RoleType.SupplierManager.getValue());
    }

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, GlobalSystem ecoSystem) {
        return new SupplierWorkAreaJPanel(userProcessContainer, account, enterprise, organization, ecoSystem);
    }

}
