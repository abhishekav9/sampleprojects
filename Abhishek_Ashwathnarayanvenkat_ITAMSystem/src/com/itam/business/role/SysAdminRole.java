/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.role;

import com.itam.business.useraccounts.UserAccount;
import com.itam.business.GlobalSystem;
import com.itam.business.enterprise.Enterprise;
import com.itam.business.organization.Organization;
import javax.swing.JPanel;
import userInterface.SysAdminRole.SysAdminWorkAreaJPanel;
import userInterface.SysAdminRole.SysAdminWorkAreaJPanelUnused;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class SysAdminRole extends Role {

    public SysAdminRole() {
        super(RoleType.SysAdmin.getValue());
    }

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, GlobalSystem ecoSystem) {

        return new SysAdminWorkAreaJPanel(userProcessContainer, account, ecoSystem);

    }

}
