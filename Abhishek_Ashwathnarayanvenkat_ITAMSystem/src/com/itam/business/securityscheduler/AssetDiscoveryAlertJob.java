package com.itam.business.securityscheduler;

import com.itam.business.utility.CommonUtilities;
import com.itam.business.utility.ResourceBundleProperty;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class AssetDiscoveryAlertJob implements Job {

    private List<String> scannedIpList = new LinkedList<>();
    private List<String> existingIpList;
    private ResourceBundleProperty resourceBundleProperty = new ResourceBundleProperty();
    private static Properties rbProps;

    public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {

        //InputStream input = null;
        try {
            rbProps = CommonUtilities.getRbProps(resourceBundleProperty.getProperty("resourceBundlePath"));
            BufferedReader reader = new BufferedReader(new FileReader(rbProps.getProperty("scannedAssets")));
            String line = null;
            while ((line = reader.readLine()) != null) {
                scannedIpList.add(line);
            }
            existingIpList = getExistingAssetList();
            scannedIpList.removeAll(existingIpList);

            System.out.println(scannedIpList);
            if (scannedIpList.size() > 0 && rbProps.getProperty("securityManagerEmail") != null && !(rbProps.getProperty("securityManagerEmail").isEmpty())) {
                CommonUtilities.sendMailAlert(scannedIpList);
                try (PrintWriter writer = new PrintWriter(rbProps.getProperty("scannedAssets"))) {
                    writer.print("");
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AssetDiscoveryAlertJob.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static List<String> getExistingAssetList() {
        List<String> existingIpList = new LinkedList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(rbProps.getProperty("allowedAssets")));
            String line = null;
            while ((line = reader.readLine()) != null) {
                existingIpList.add(line);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AssetDiscoveryAlertJob.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return existingIpList;
    }
}
