package com.itam.business.securityscheduler;

import java.util.List;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;


/**
 * @author onlinetechvision.com
 * @since 17 Sept 2011
 * @version 1.0.0
 *
 */
public class JobScheduler {
	
	public static void main(String[] args, List dummList) {
		
		try {
			
                    // specify the job' s details..
                    JobDetail job = JobBuilder.newJob(AssetDiscoveryAlertJob.class).withIdentity("job1").build();
                    // specify the running period of the job
                    Trigger trigger = TriggerBuilder.newTrigger()
			    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
	                    .withIntervalInSeconds(30)
	                    .repeatForever())
                            .build();  
	    	
                    //schedule the job
                    SchedulerFactory schFactory = new StdSchedulerFactory();
                    Scheduler sch = schFactory.getScheduler();
	    		    	
	    	sch.scheduleJob(job, trigger);
                sch.start();
                
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
}
