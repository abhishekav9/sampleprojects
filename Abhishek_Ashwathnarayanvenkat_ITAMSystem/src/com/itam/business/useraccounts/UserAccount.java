/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.useraccounts;

import com.itam.business.Person;
import com.itam.business.asset.AssetDirectory;
import com.itam.business.organization.Organization;
import com.itam.business.role.Role;
import com.itam.business.work.WorkQueue;

/**
 *
 * @author raunak
 */
public class UserAccount {

    private String username;
    private String password;
    private Person person;
    private Role role;
    private WorkQueue workQueue;
    private String status;
    private Organization.Type organization;
    private AssetDirectory softwareAssetDirectory;
    private AssetDirectory hardwareAssetDirectory;


    public UserAccount() {
        workQueue = new WorkQueue();
        hardwareAssetDirectory = new AssetDirectory();
        softwareAssetDirectory = new AssetDirectory();
    }

    public Organization.Type getOrganization() {
        return organization;
    }

    public void setOrganization(Organization.Type organization) {
        this.organization = organization;
    }

    public AssetDirectory getHardwareAssetDirectory() {
        return hardwareAssetDirectory;
    }

    public AssetDirectory getSoftwareAssetDirectory() {
        return softwareAssetDirectory;
    }

    public void setHardwareAssetDirectory(AssetDirectory hardwareAssetDirectory) {
        this.hardwareAssetDirectory = hardwareAssetDirectory;
    }

    public void setSoftwareAssetDirectory(AssetDirectory softwareAssetDirectory) {
        this.softwareAssetDirectory = softwareAssetDirectory;
    }
    
    

//    public List<HardwareAsset> getHardwareAssetList() {
//        return hardwareAssetList;
//    }
//
//    public void setHardwareAssetList(List<HardwareAsset> hardwareAssetList) {
//        this.hardwareAssetList = hardwareAssetList;
//    }
//
//    public List<SoftwareAsset> getSoftwareAssetList() {
//        return softwareAssetList;
//    }
//
//    public void setSoftwareAssetList(List<SoftwareAsset> softwareAssetList) {
//        this.softwareAssetList = softwareAssetList;
//    }

    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    @Override
    public String toString() {
        return username;
    }

}
