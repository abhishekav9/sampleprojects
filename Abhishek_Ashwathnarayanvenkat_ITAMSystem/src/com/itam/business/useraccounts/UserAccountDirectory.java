/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.useraccounts;

import com.itam.business.Person;
import com.itam.business.role.ITSecurityManagerRole;
import com.itam.business.role.Role;
import com.itam.business.utility.CommonUtilities;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class UserAccountDirectory {

    private ArrayList<UserAccount> userAccountList;

    public UserAccountDirectory() {
        userAccountList = new ArrayList<>();
    }

    public ArrayList<UserAccount> getUserAccountList() {
        return userAccountList;
    }

    public UserAccount authenticateUser(String username, String password) {
        for (UserAccount ua : userAccountList) {
            if (ua.getUsername().equals(username) && ua.getPassword().equals(password)) {
                return ua;
            }
        }
        return null;
    }

    public UserAccount createUserAccount(String username, String password, Role role, Person person) {
        //do the scan here
        UserAccount userAccount = new UserAccount();
        try {
            userAccount.setUsername(username);
            userAccount.setPassword(password);
            userAccount.setPerson(person);
            //writePropertiesFile("securityManagerEmail", person.getEmailId());
            if (role instanceof ITSecurityManagerRole) {
                CommonUtilities.writePropertiesFile("securityManagerEmail", person.getEmailId());
//                Properties rbProps = new Properties();
//                InputStream input = null;
//                //OutputStream output = null;
//                input = new FileInputStream("/Users/abhishekashwathnarayanvenkat/NetBeansProjects/Abhishek_Ashwathnarayanvenkat_ITAMSystem/src/properties/ResourceBundle.properties");
//                rbProps.load(input);
//                //output = new FileOutputStream("/Users/abhishekashwathnarayanvenkat/NetBeansProjects/Abhishek_Ashwathnarayanvenkat_ITAMSystem/src/properties/ResourceBundle.properties");
//
//                if (rbProps.getProperty("securityManagerEmail") == null) {
//                    writePropertiesFile("securityManagerEmail", person.getEmailId());
//                } else {
//                    StringBuffer properValue = new StringBuffer(rbProps.getProperty("securityManagerEmail"));
//                    properValue.append("," + person.getEmailId());
//                }
            }
            userAccount.setRole(role);
            userAccountList.add(userAccount);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
        return userAccount;
    }

  

    public boolean checkUserNameUnique(String usname) {
        for (UserAccount userAccount : userAccountList) {
            if (userAccount.getUsername().equals(usname)) {
                return false;
            }
        }
        return true;
    }
}
