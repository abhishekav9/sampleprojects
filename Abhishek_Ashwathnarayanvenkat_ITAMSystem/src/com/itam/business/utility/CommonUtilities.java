/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class CommonUtilities {

    //private static Properties rbProps;// = DB4OUtil.getRbProps();
    //ResourceBundleProperty resourceBundleProperty = ResourceBundleProperty.getInstance();
    public static Properties getRbProps(String filePath) {
        Properties rbProps = new Properties();
        try (InputStream input = new FileInputStream(filePath)) {
            rbProps.load(input);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return rbProps;
    }

//    public synchronized static Properties getRbPropsInstance(boolean isReload) {
//        if (isReload) {
//            rbProps.clear();
//            try {
//                InputStream input = new FileInputStream("/Users/abhishekashwathnarayanvenkat/NetBeansProjects/Abhishek_Ashwathnarayanvenkat_ITAMSystem/src/properties/ResourceBundle.properties");
//                rbProps.load(input);
//            } catch (Exception ex) {
//                Logger.getLogger(DB4OUtil.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        return rbProps;
//    }
    public static boolean validateTableSelection(JTable table, int noOfRows) {
        if (table.getSelectedRowCount() == noOfRows) {
            return true;
        } else {
            return false;
        }
    }

    public static void sendMailAlert(List<String> scannedIpList) {
        //boolean b=false;

        try {
            ResourceBundleProperty rbProps = new ResourceBundleProperty();
            Properties resourceBundleProperties = getRbProps(rbProps.getProperty("resourceBundlePath"));
            String auth = "mail.smtp.auth";
            String tlsEnable = "mail.smtp.starttls.enable";
            String host = "mail.smtp.host";
            String port = "mail.smtp.port";
            EncryDecry encryDecry = new EncryDecry();
            //rb.g
            final String username = resourceBundleProperties.getProperty("mail.username");
            final String password = encryDecry.decode(resourceBundleProperties.getProperty("mail.password"));

            Properties props = new Properties();
            props.put(auth, resourceBundleProperties.getProperty(auth));
            props.put(tlsEnable, resourceBundleProperties.getProperty(tlsEnable));
            props.put(host, resourceBundleProperties.getProperty(host));
            props.put(port, resourceBundleProperties.getProperty(port));

            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });
            resourceBundleProperties.getProperty("securityManagerEmail");
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ITAM_Threat"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(resourceBundleProperties.getProperty("securityManagerEmail")));
            message.setSubject(resourceBundleProperties.getProperty("mail.subject"));
            message.setText(resourceBundleProperties.getProperty("mail.body") + scannedIpList);
            Transport.send(message);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public static void writePropertiesFile(String key, String data) {
        // = null;
        ResourceBundleProperty resourceBundleProperty = new ResourceBundleProperty();
        File file = new File(resourceBundleProperty.getProperty("resourceBundlePath"));
        Properties rbProps = new Properties();
        try (InputStream input = new FileInputStream(file)) {
            rbProps.load(input);
        } catch (IOException ex) {
            Logger.getLogger(CommonUtilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (OutputStream fileOut = new FileOutputStream(file)) {

            if (rbProps.getProperty("securityManagerEmail") == null || rbProps.getProperty("securityManagerEmail").equals("")) {
                rbProps.setProperty("securityManagerEmail", data);
                //writePropertiesFile("securityManagerEmail", data);
            } else {
                StringBuffer properValue = new StringBuffer(rbProps.getProperty("securityManagerEmail"));
                rbProps.setProperty("securityManagerEmail", properValue.append("," + data).toString());
            }
            rbProps.store(fileOut, "update emailID");
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public static boolean validateTextFieldsForAmount(JTextField textField) {
        try {
            Double value = Double.parseDouble(textField.getText().trim());
            if (value <= 0) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean validateIpAddress(final String ip) {
        if (ip != null && ip.trim().equals("")) {
            return false;
        }
        final String PATTERN
                = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(ip);

        return matcher.matches();
    }

    public static boolean validateMacAddress(String mac) {
        if (mac != null && !mac.trim().equals("")) {

            final String PATTERN
                    = "^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$";
            Pattern pattern = Pattern.compile(PATTERN);
            Matcher matcher = pattern.matcher(mac);
            return matcher.matches();
        } else {
            return false;
        }
        
    }

    public static boolean validateTextFieldForPercentage(JTextField textField) {
        try {
            Double percentage = Double.parseDouble(textField.getText().trim());
            if (percentage <= 100.0 && percentage >= 0.0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }

    public static boolean validateTextAreaForNonEmpty(JTextArea txtArea) {
        if (txtArea.getText().trim().equalsIgnoreCase("")) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean validateTextFieldsForNonEmpty(JTextField textField) {
        if (textField.getText().trim().equalsIgnoreCase("")) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean validateTextFieldsForString(JTextField textField) {
        String textString = textField.getText().trim();
        boolean ismatched = textString.matches("[a-zA-Z]*");
        //^[A-z]+$/
        return ismatched;
    }

    public static boolean validateTextFieldsForInteger(JTextField textField) {
        try {
            int a = Integer.parseInt(textField.getText().trim());
            if (a < 0) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean validateTextFieldsForNumber(JTextField textField) {
        return true;
    }

    public static boolean validateTextFieldsForPhoneNumber(JTextField textField) {
        Pattern patternMobileNumber = Pattern.compile("\\d{10}");
        Matcher matcher = patternMobileNumber.matcher(textField.getText().trim());
        return matcher.matches();
    }

    public static boolean validateTextFieldsForEmailId(JTextField textField) {
        try {
            String patternEmail = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            Boolean flag = textField.getText().trim().matches(patternEmail);
            return flag;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean validateTextFieldsForUserName(JTextField txtUserName) {
        return true;
    }

    public static boolean validateComboBoxForSelection(JComboBox comboBox) {
        if (comboBox.getSelectedIndex() != -1) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean validateTextFieldForPassword(JTextField txtPassowrd) {
        return true;
    }
}
