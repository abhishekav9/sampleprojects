package com.itam.business.utility;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.ta.TransparentPersistenceSupport;
import com.itam.business.GlobalSystem;
import com.itam.business.InitializeSystem;
import java.util.Properties;

/**
 *
 * @author raunak
 */
public class DB4OUtil {

    private static Properties rbProps;

    private static final String FILENAME = "/Users/abhishekashwathnarayanvenkat/NetBeansProjects/Abhishek_Ashwathnarayanvenkat_ITAMSystem/src/db4oFile/DataBank.db4o";
    private static DB4OUtil dB4OUtil;

    public synchronized static DB4OUtil getInstance() {
        if (dB4OUtil == null) {
            dB4OUtil = new DB4OUtil();
        }
        return dB4OUtil;
    }

    protected synchronized static void shutdown(ObjectContainer conn) {
        if (conn != null) {
            conn.close();
        }
    }

    private ObjectContainer createConnection() {
        try {

            EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
            config.common().add(new TransparentPersistenceSupport());
            //Controls the number of objects in memory
            config.common().activationDepth(Integer.MAX_VALUE);
            //Controls the depth/level of updation of Object
            config.common().updateDepth(Integer.MAX_VALUE);
            //Register your top most Class here
            config.common().objectClass(GlobalSystem.class).cascadeOnUpdate(true); // Change to the object you want to save

            ObjectContainer db = Db4oEmbedded.openFile(config, FILENAME);
            return db;
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
        return null;
    }

    public synchronized void storeSystem(GlobalSystem system) {
        ObjectContainer conn = createConnection();
        conn.store(system);
        conn.commit();
        conn.close();
    }

    public GlobalSystem retrieveSystem() {
        ObjectContainer conn = createConnection();
        ObjectSet<GlobalSystem> systems = null;
        if (conn != null) {
            systems = conn.query(GlobalSystem.class); // Change to the object you want to save
        }
        GlobalSystem system;
        if (systems == null || systems.size() == 0) {
            system = InitializeSystem.configure();  // If there's no System in the record, create a new one
        } else {
            system = systems.get(0);
        }
        if (conn != null) {
            conn.close();
        }
        return system;
    }
}
