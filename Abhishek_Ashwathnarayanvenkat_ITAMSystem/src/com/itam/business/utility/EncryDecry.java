/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import sun.misc.BASE64Decoder;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class EncryDecry {

    private String encoded = null;
    private String decoded = null;

    /**
     * This method is used to encrypt the data
     *
     * @param data - String
     * @return encoded - String
     */
    public String encode(String data) {
        encoded = new sun.misc.BASE64Encoder().encodeBuffer(data.getBytes());
        return encoded;
    }

    /**
     * This method is used to decode the data
     *
     * @param data - String
     * @return decoded - String
     * @throws IOException
     */
    public String decode(String data) throws IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] result = decoder.decodeBuffer(data);
        decoded = new String(result);
        return decoded;
    }

    public static void main(String[] args) throws Exception {
        //System.out.println(new EncryDecry().decode("dGVzdCMxMjM="));
        System.out.println(new EncryDecry().encode(""));
        //EncryDecry.writePropertiesFile("securityManagerEmail", "asd@gmaik.com");
    }
    
     
}
