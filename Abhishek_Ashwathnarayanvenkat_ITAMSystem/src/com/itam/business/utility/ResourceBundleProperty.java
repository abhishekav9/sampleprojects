/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.utility;

import java.util.ResourceBundle;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class ResourceBundleProperty {

    private ResourceBundle rb;
    //private static ResourceBundleProperty rbp = null;

    /**
     * Constructor to read resource bundle for key string.
     */
    public ResourceBundleProperty() {
        rb = ResourceBundle.getBundle("properties.ResourceBundle");
    }


    /**
     * This method will return the value from the resource bundle.
     *
     * @param s - a sting key.
     *
     * @return the value from the resource bundle.
     */
    public String getProperty(String s) {
        return rb.getString(s);
    }
}
