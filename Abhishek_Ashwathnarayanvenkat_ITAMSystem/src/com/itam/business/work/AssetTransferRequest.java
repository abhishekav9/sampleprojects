/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.work;


import com.itam.business.asset.HardwareAsset;
import com.itam.business.organization.Organization;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class AssetTransferRequest extends WorkRequest{
    private HardwareAsset hardwareAsset;
    private Organization toBeTransferred;

    public HardwareAsset getHardwareAsset() {
        return hardwareAsset;
    }

    public void setHardwareAsset(HardwareAsset hardwareAsset) {
        this.hardwareAsset = hardwareAsset;
    }

    public Organization getToBeTransferred() {
        return toBeTransferred;
    }

    public void setToBeTransferred(Organization toBeTransferred) {
        this.toBeTransferred = toBeTransferred;
    }

    @Override
    public String toString() {
        return String.valueOf(super.getId()); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
