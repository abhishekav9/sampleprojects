/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.work;

import com.itam.business.product.Product;
import java.util.Date;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class PurchaseOrder extends WorkRequest {

    private Product product;
    private int quantity;
    private float amt;
    private Date purchaseDate;

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public float getAmt() {
        return amt;
    }

    public void setAmt(float amt) {
        this.amt = amt;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return String.valueOf(super.getId()); //To change body of generated methods, choose Tools | Templates.
    }

    public enum PurchaseOrderStatus {

        Open("Open"),
        Completed("Completed"),
        InProgress("InProgress"),
        SupplierShipped("Supplier-Shipped"),
        MappedToAsset("MappedToAsset"),
        FinanceApproved("Finance-Approved");

        private String value;

        private PurchaseOrderStatus(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return getValue(); //To change body of generated methods, choose Tools | Templates.
        }

    }

}
