/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.work;

import com.itam.business.asset.Asset;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class ReleaseAssetRequest extends WorkRequest {

    private Asset asset;

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    @Override
    public String toString() {
        return String.valueOf(super.getId()); //To change body of generated methods, choose Tools | Templates.
    }

}
