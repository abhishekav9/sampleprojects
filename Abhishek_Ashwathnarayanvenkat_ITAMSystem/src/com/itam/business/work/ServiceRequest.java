/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.work;

import com.itam.business.product.Product;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class ServiceRequest extends WorkRequest {

    private String title;
    private String category;
    private String department;
    private String priority;
    private Product product;
    //private String site;
    private String type;
    //private UserAccount approvedBy;
    //private int id;
    //private static int count=500;

//    public ServiceRequest() {
//        id=count;
//        count++;
//    }
//    public ServiceRequest() {
//        id = new Random().nextInt();
//    }

    @Override
    public String toString() {
        return String.valueOf(super.getId()); //To change body of generated methods, choose Tools | Templates.
    }

    
    public enum IncidentPriority {

        Low("Low"),
        Medium("Medium"),
        High("High"),
        Critical("Critical");

        private String value;

        private IncidentPriority(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum IncidentStatus {

        Open("Open"),
        Rejected("Rejected"),
        Closed("Closed"),
        Reopen("Reopen"),
        InProgress("In Progress"),
        AwaitingInput("Awaiting Input"),
        Approved("Approved");

        private String value;

        private IncidentStatus(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum IncidentType {

        AssetTransfer("Asset Transfer"),
        AssetRequest("Asset Request");

        private String value;

        private IncidentType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

}
