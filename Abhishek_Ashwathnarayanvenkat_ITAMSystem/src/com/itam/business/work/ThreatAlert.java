/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.work;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class ThreatAlert extends WorkRequest{
    
    private String threatType;
    public String getThreatType() {
        return threatType;
    }

    public void setThreatType(String threatType) {
        this.threatType = threatType;
    }
    
        public enum ThreatAlertType{
        UnauthorizedSoftware("Unauthorized Software"),
        UnauthorizedHardware("ApprUnauthorized Hardwareoved");
        
        private String value;
        private ThreatAlertType(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
        
}
