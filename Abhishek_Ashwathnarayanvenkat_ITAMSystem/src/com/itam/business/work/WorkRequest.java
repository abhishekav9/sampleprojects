/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itam.business.work;

import com.itam.business.organization.Organization;
import com.itam.business.useraccounts.UserAccount;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author raunak
 */
public abstract class WorkRequest {
    private String message;
    private Organization organization;
    private UserAccount sender;
    private UserAccount receiver;
    private UserAccount approver;
    private String status;
    private Date requestDate;
    private Date resolveDate;
    private int id;
        
    public enum WorkRequestStatus{
        
        Open("Open"),
        Closed("Closed"),
        Reopen("Reopen"),
        InProgress("InProgress"),
        AwaitingInput("Awaiting Input"),
        Approved("Approved");
        
        private String value;
        private WorkRequestStatus(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public int getId() {
        return id;
    }
    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }
    
    public UserAccount getApprover() {
        return approver;
    }

    public void setApprover(UserAccount approver) {
        this.approver = approver;
    }
        
    public WorkRequest(){
        requestDate = new Date();
        int i =new Random().nextInt();
        id = i<0?(i*(-1)):i;
        status = ServiceRequest.IncidentStatus.Open.getValue();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }

    @Override
    public String toString() {
        return message; //To change body of generated methods, choose Tools | Templates.
    }
    
}
