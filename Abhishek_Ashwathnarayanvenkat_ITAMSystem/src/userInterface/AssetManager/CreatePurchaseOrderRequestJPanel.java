/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.AssetManager;

import com.itam.business.GlobalSystem;
import com.itam.business.product.FirewallElement;
import com.itam.business.product.HardwareElement;
import com.itam.business.product.HardwareProduct;
import com.itam.business.product.LaptopDesktopServerElement;
import com.itam.business.product.PrinterElement;
import com.itam.business.product.Product;
import com.itam.business.product.RouterElement;
import com.itam.business.product.ScannerElement;
import com.itam.business.product.SoftwareProduct;
import com.itam.business.enterprise.Enterprise;
import com.itam.business.enterprise.ITOperationsEnterprise;
import com.itam.business.organization.FinanceOrganization;
import com.itam.business.organization.Organization;
import com.itam.business.useraccounts.UserAccount;
import com.itam.business.work.PurchaseOrder;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class CreatePurchaseOrderRequestJPanel extends javax.swing.JPanel {

    /**
     * Creates new form CreatePurchaseOrderRequestJPanel
     */
    private JPanel userProcessContainer;
    private GlobalSystem systemOrg;
    private UserAccount userAccount;
    private ITOperationsEnterprise enterprise;
    private Organization organization;

    public CreatePurchaseOrderRequestJPanel(JPanel userProcessContainer, UserAccount userAccount, Enterprise enterprise, Organization organization, GlobalSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        systemOrg = system;
        this.userAccount = userAccount;
        this.enterprise = (ITOperationsEnterprise) enterprise;
        this.organization = organization;
        populateSelectComboBox();
        populateOrgComboBox();
    }

     public void populateSelectComboBox() {
        comboHWSW1.removeAllItems();
        comboHWSW1.addItem("Hardware");
        comboHWSW1.addItem("Software");
    }
     public void populateOrgComboBox() {
        cmbOrgBox.removeAllItems();
        for(Organization o:enterprise.getOrganizationDirectory().getOrganizationList())
        {
            cmbOrgBox.addItem(o);
        }
        
    }
    public void populateProductsCombo(String type) {
        comboProducts.removeAllItems();
        if (type.equals("Hardware")) {
            for (Product hardwareProduct : enterprise.getEnterpriseHardwareProductCatalog()) {
                comboProducts.addItem(hardwareProduct);
            }
        } else {
            for (Product hardwareProduct : enterprise.getEnterpriseSoftwareProductCatalog()) {
                comboProducts.addItem(hardwareProduct);
            }
        }
    }

    public void populateData() {
        Product product = null;

        if (comboProducts.getSelectedItem() != null) {
            if (comboProducts.getSelectedItem() instanceof Product) {
                txtType.setEnabled(false);
                lblType.setEnabled(false);
                txtProductName.setEnabled(false);
                lblProductName.setEnabled(false);
                product = (Product) comboProducts.getSelectedItem();

                txtProductName.setText(product.getName());
                HardwareProduct hardwareProduct;
                SoftwareProduct softwareProduct;
                if (product != null) {
                    if (comboHWSW1.getSelectedItem().equals("Hardware")) {
                        hardwareProduct = (HardwareProduct) product;
                        String type = hardwareProduct.getType();
                        HardwareElement hardwareElement = hardwareProduct.getHardwareElement();
                        txtType.setText(type);
                        if (type.equals(HardwareProduct.HardwareType.Laptop.getValue()) || type.equals(HardwareProduct.HardwareType.Desktop.getValue()) || type.equals(HardwareProduct.HardwareType.Server.getValue())) {
                            LaptopDesktopServerElement laptopDesktopServerElement = (LaptopDesktopServerElement) hardwareElement;
                        } else if (type.equals(HardwareProduct.HardwareType.Firewall.getValue())) {
                            FirewallElement firewallElement = (FirewallElement) hardwareElement;

                        } else if (type.equals(HardwareProduct.HardwareType.Printer.getValue())) {
                            PrinterElement printerElement = (PrinterElement) hardwareElement;

                        } else if (type.equals(HardwareProduct.HardwareType.Scanner.getValue())) {
                            ScannerElement scannerElement = (ScannerElement) hardwareElement;

                        } else if (type.equals(HardwareProduct.HardwareType.Router.getValue())) {
                            RouterElement routerElement = (RouterElement) hardwareElement;

                        }

                    } else {
                        softwareProduct = (SoftwareProduct) product;
                        txtType.setText(softwareProduct.getType());

                    }

                }
            } else if ((comboProducts.getSelectedItem() instanceof String) && (comboProducts.getSelectedItem().equals("Others"))) {
//                txtType.setEnabled(true);
//                lblType.setEnabled(true);
//                txtProductName.setEnabled(true);
//                lblProductName.setEnabled(true);

            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        comboProducts = new javax.swing.JComboBox();
        comboHWSW1 = new javax.swing.JComboBox();
        lblProductName = new javax.swing.JLabel();
        txtProductName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        lblType = new javax.swing.JLabel();
        txtType = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jSpinnerQty = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        btnCreate = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        cmbOrgBox = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        btnBack1 = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        comboProducts.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboProducts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboProductsActionPerformed(evt);
            }
        });
        add(comboProducts, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 110, 240, 30));

        comboHWSW1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboHWSW1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboHWSW1ActionPerformed(evt);
            }
        });
        add(comboHWSW1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 80, 170, -1));

        lblProductName.setText("Product Name:");
        lblProductName.setEnabled(false);
        add(lblProductName, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 220, -1, 20));

        txtProductName.setEnabled(false);
        add(txtProductName, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 220, 240, 30));

        jLabel5.setText("Quantity:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 260, -1, 30));

        lblType.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblType.setText("Product Type:");
        lblType.setEnabled(false);
        add(lblType, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 180, 100, 30));

        txtType.setEnabled(false);
        add(txtType, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 180, 240, 30));

        jLabel4.setText("Select:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, -1, 30));

        jSpinnerQty.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        add(jSpinnerQty, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 260, -1, -1));

        jLabel6.setText("Organization:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 150, -1, 20));

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });
        add(btnCreate, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 310, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel2.setText("Create Purchase Order");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 30, -1, -1));

        cmbOrgBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        add(cmbOrgBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 150, 170, -1));

        jLabel7.setText("Product:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 120, -1, 20));

        btnBack1.setText("<<BACK");
        btnBack1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack1ActionPerformed(evt);
            }
        });
        add(btnBack1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, 90, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void comboProductsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboProductsActionPerformed
        // TODO add your handling code here:
        populateData();
    }//GEN-LAST:event_comboProductsActionPerformed

    private void comboHWSW1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboHWSW1ActionPerformed
        // TODO add your handling code here:
        if (comboHWSW1.getSelectedItem() != null) {
            populateProductsCombo((String) comboHWSW1.getSelectedItem());
        }
    }//GEN-LAST:event_comboHWSW1ActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setProduct((Product) comboProducts.getSelectedItem());
        purchaseOrder.setQuantity((int) jSpinnerQty.getValue());
        purchaseOrder.setSender(userAccount);
        purchaseOrder.setOrganization((Organization)cmbOrgBox.getSelectedItem());
        for (Organization o : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (o instanceof FinanceOrganization) {
                o.getWorkQueue().getWorkRequestList().add(purchaseOrder);
                break;
            }
        }
        userAccount.getWorkQueue().getWorkRequestList().add(purchaseOrder);
        ((ITOperationsEnterprise)enterprise).getEnterprisePurchaseOrderCatalog().add(purchaseOrder);
        JOptionPane.showMessageDialog(this, "Purchase Order sent!!", "Success", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnBack1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack1ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length-1];
        ManagePurchaseOrderRequestJPanel sawajp = (ManagePurchaseOrderRequestJPanel)component;
        sawajp.populateTable();
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack1;
    private javax.swing.JButton btnCreate;
    private javax.swing.JComboBox cmbOrgBox;
    private javax.swing.JComboBox comboHWSW1;
    private javax.swing.JComboBox comboProducts;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JSpinner jSpinnerQty;
    private javax.swing.JLabel lblProductName;
    private javax.swing.JLabel lblType;
    private javax.swing.JTextField txtProductName;
    private javax.swing.JTextField txtType;
    // End of variables declaration//GEN-END:variables
}
