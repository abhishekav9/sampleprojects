/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.EnterpriseAdminRole;

import com.itam.business.GlobalSystem;
import com.itam.business.enterprise.Enterprise;
import com.itam.business.useraccounts.UserAccount;
import java.awt.CardLayout;
import javax.swing.JPanel;
import userInterface.SysAdminRole.ManagePersonJPanel;
import userInterface.SysAdminRole.ManageUserAccountJPanel;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class EnterpriseAdminWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form EnterpriseAdminWorkAreaJPanel
     */
    private Enterprise enterprise;
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private GlobalSystem globalSystem;

    public EnterpriseAdminWorkAreaJPanel(JPanel userProcessContainer, UserAccount userAccount, Enterprise enterprise, GlobalSystem globalSystem) {
        initComponents();
        this.globalSystem = globalSystem;
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        this.userProcessContainer = userProcessContainer;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblSystemAmdinistrator = new javax.swing.JLabel();
        btnManageUserAcc = new javax.swing.JButton();
        btnManageEmp = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblSystemAmdinistrator.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblSystemAmdinistrator.setText("Enterprise Work Area");
        add(lblSystemAmdinistrator, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 20, 160, 20));

        btnManageUserAcc.setText("Manage User Accounts");
        btnManageUserAcc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageUserAccActionPerformed(evt);
            }
        });
        add(btnManageUserAcc, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 190, 190, -1));

        btnManageEmp.setText("Manage People");
        btnManageEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageEmpActionPerformed(evt);
            }
        });
        add(btnManageEmp, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 150, 190, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnManageUserAccActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageUserAccActionPerformed
        // TODO add your handling code here:
        ManageUserAccountJPanel manageUserAccountJPanel = new ManageUserAccountJPanel(userProcessContainer, userAccount, enterprise, globalSystem);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("ManageUserAccountJPanel1", manageUserAccountJPanel);
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnManageUserAccActionPerformed

    private void btnManageEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageEmpActionPerformed
        // TODO add your handling code here:
        ManagePersonJPanel managePersonJPanel = new ManagePersonJPanel(userProcessContainer, enterprise.getOrganizationDirectory(), userAccount);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("ManageEnterpriseJPanel1", managePersonJPanel);
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnManageEmpActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnManageEmp;
    private javax.swing.JButton btnManageUserAcc;
    private javax.swing.JLabel lblSystemAmdinistrator;
    // End of variables declaration//GEN-END:variables
}
