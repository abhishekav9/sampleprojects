/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.algo.business;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class ValueComparator implements Comparator<String> {
 
    Map<String, Double> map;
 
    public ValueComparator(HashMap<String, Double> base) {
        this.map = base;
    }
 
    public int compare(String a, String b) {
        if (map.get(a) >= map.get(b)) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys 
    }

}
