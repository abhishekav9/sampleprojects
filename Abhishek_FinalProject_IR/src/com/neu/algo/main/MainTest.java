/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.algo.main;

import com.neu.algo.business.Posting;
import com.neu.algo.business.ValueComparator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import org.apache.commons.io.FileUtils;
import org.tartarus.snowball.ext.porterStemmer;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class MainTest {

    static List<String> stopWords = new LinkedList<>();
    static MainTest mt = new MainTest();
    static String searchPhrase = "Onto Whitesnake, Guns N' Roses";
//    static {
//        stopWords = Arrays.asList(
//                "a", "an", "and", "are", "as", "at", "be", "but", "by",
//                "for", "if", "in", "into", "is", "it",
//                "no", "not", "of", "on", "or", "such",
//                "that", "the", "their", "then", "there", "these",
//                "they", "this", "to", "was", "will", "with"
//        );
//    }

    public static void main(String[] args) throws IOException {
        //mt.readObjectFromFile("theIndex");
        ///////////////
        Scanner in = new Scanner(System.in);
        System.out.println("Enter searchWord");
        searchPhrase = in.nextLine();
        File directoryPath = new File("/Users/abhishekashwathnarayanvenkat/AlgoTextCorpus");
        List<File> fileList = (List<File>) FileUtils.listFiles(directoryPath, null, true);
        int docCOunt = 0;
        //Stopwords loaded
        getStopWords();
        ///////////////
        HashMap<String, HashMap<String, Posting>> mainInvertedIndex = new HashMap();
        HashMap<String, Integer> docFrequencyMap = new HashMap();
        HashMap<String, Integer> docWordCOunt = new HashMap();
        
        HashMap<String, Double> idfMap = new HashMap();
        HashMap<String, HashMap<String, Double>> tfMap = new HashMap();
        HashMap<String, HashMap<String, Double>> tfIdfMap = new HashMap();

        //mt.indexAllFiles(mainInvertedIndex, docFrequencyMap);
        for (File file : fileList) {
            //StringBuilder sbLine = new StringBuilder();
            //String docText = null;
            if (file.isFile()) {
                int posCount = 0;
                docCOunt += 1;
                String fileName = file.getPath();
                Scanner sc = new Scanner(file);
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    line = line.toLowerCase();
                    //Pattern pattern1 = Pattern.compile("\\W|\\S|"+stopWords);
                    //Pattern pattern1 = Pattern.compile("\\b(?:" + stopWords + ")\\b\\s*", Pattern.CASE_INSENSITIVE);

                    //Matcher matcher1 = pattern1.matcher(line);
                    //line = matcher1.replaceAll("");
                    //line = stemmer(line);
                    //line = line.replaceAll("[^a-zA-Z0-9]+","");
                    String eachWordArray[] = line.split(" ");
                    for (String eachWord : eachWordArray) {
                        if (stopWords.contains(eachWord.trim())) {
                            posCount += 1;

                        } else {
                            String word = eachWord.trim();
                            if (word != null || !word.isEmpty()) {
                                posCount++;

                                if (mainInvertedIndex.containsKey(word)) {
                                    HashMap<String, Posting> docPostingmap = mainInvertedIndex.get(word);
                                    if (docPostingmap.containsKey(fileName)) {
                                        Posting eachPosting = docPostingmap.get(fileName);
                                        eachPosting.term_frequency += 1;
                                        eachPosting.totalWords += 1;
                                        eachPosting.positionList.add(posCount);
                                    } else {
                                        Posting posting = new Posting();
                                        posting.positionList.add(posCount);
                                        posting.term_frequency += 1;
                                        docPostingmap.put(fileName, posting);
                                        int i = docFrequencyMap.get(word);
                                        docFrequencyMap.put(word, i + 1);
                                    }
                                } else {
                                    HashMap<String, Posting> docPostingmap = new HashMap<>();
                                    docFrequencyMap.put(word, 1);
                                    Posting posting = new Posting();
                                    posting.positionList.add(posCount);
                                    posting.term_frequency += 1;
                                    posting.totalWords += 1;
                                    docPostingmap.put(fileName, posting);
                                    mainInvertedIndex.put(word, docPostingmap);
                                }
                            }
                        }
                    }
                    if (docWordCOunt.containsKey(fileName)) {
                        int i = docWordCOunt.get(fileName);
                        docWordCOunt.put(fileName, i + posCount);
                    } else {
                        docWordCOunt.put(fileName, posCount);
                    }
                }
                sc.close();
            }
        }
        mt.saveObjectToFile("theIndex", mainInvertedIndex);
        //////start searching
        //populate TFmap and idf map
        for (Map.Entry<String, HashMap<String, Posting>> entrySet : mainInvertedIndex.entrySet()) {
            String key = entrySet.getKey();
            HashMap<String, Posting> postingMap = entrySet.getValue();
            int i = 0;
            for (Map.Entry<String, Posting> entrySet1 : postingMap.entrySet()) {
                i += 1;
                String fileName = entrySet1.getKey();
                Posting posting = entrySet1.getValue();
                double normTf = (float) posting.positionList.size() / (float) docWordCOunt.get(fileName);
                if (tfMap.containsKey(key)) {
                    HashMap<String, Double> normTfMap = tfMap.get(key);
                    if (normTfMap.containsKey(fileName)) {
                        continue;
                    } else {
                        normTfMap.put(fileName, normTf);
                        tfMap.put(key, normTfMap);
                    }
                } else {
                    HashMap<String, Double> fileNormMap = new HashMap<>();
                    fileNormMap.put(fileName, normTf);
                    tfMap.put(key, fileNormMap);
                }
            }
            docFrequencyMap.put(key, i);
            double idf = 1 + Math.log((float) docCOunt / i);
            idfMap.put(key, idf);
        }

        //Going through each entry in tf map and calculating its tf*idf value and storing it in a map
        for (Map.Entry<String, HashMap<String, Double>> entrySet : tfMap.entrySet()) {
            String eachTerm = entrySet.getKey();
            HashMap<String, Double> eachTermTf = entrySet.getValue();
            for (Map.Entry<String, Double> entrySet1 : eachTermTf.entrySet()) {
                String fileName = entrySet1.getKey();
                Double tfValue = entrySet1.getValue();

                double idfEachTerm = idfMap.get(eachTerm);
                double tfIdfValue = tfValue * idfEachTerm;

                if (tfIdfMap.containsKey(eachTerm)) {
                    HashMap<String, Double> tfIdfMapValues = tfIdfMap.get(eachTerm);
                    if (tfIdfMapValues.containsKey(fileName)) {
                        continue;
                    } else {
                        tfIdfMapValues.put(fileName, tfIdfValue);
                        tfIdfMap.put(eachTerm, tfIdfMapValues);
                    }
                } else {
                    HashMap<String, Double> eachTfIdf = new HashMap<>();
                    eachTfIdf.put(fileName, tfIdfValue);
                    tfIdfMap.put(eachTerm, eachTfIdf);
                }
            }
        }

        //start searching
        //////////////////////////////////////////////
        HashMap<String, Integer> searchWordCountMap = new HashMap();
        HashMap<String, Double> tfIdfSearchMap = new HashMap();
        searchPhrase = searchPhrase.toLowerCase();
        String searchWordArray[] = searchPhrase.split(" ");
        int searchCount = 0;
        for (String eachWord : searchWordArray) {
            if (stopWords.contains(eachWord.trim())) {
                searchCount += 1;
            } else {
                searchCount += 1;
                eachWord = eachWord.trim();
                if (searchWordCountMap.containsKey(eachWord)) {
                    int counter = searchWordCountMap.get(eachWord);
                    counter += 1;
                    searchWordCountMap.put(eachWord, counter);
                } else {
                    searchWordCountMap.put(eachWord, 1);
                }
            }
        }

        /////calculate tf of each word
        for (Map.Entry<String, Integer> entrySet : searchWordCountMap.entrySet()) {
            String key = entrySet.getKey();
            if (mainInvertedIndex.containsKey(key)) {
                Integer value = entrySet.getValue();
                double searchTf = (double) value / (double) searchCount;
                double idfValue = idfMap.get(key);
                double tfIdfSearchValue = idfValue * searchTf;
                tfIdfSearchMap.put(key, tfIdfSearchValue);
            }
        }

        //calculate TF-IDF of the search query
        HashMap<String, ArrayList<Double>> eachDocumentsTfIdfValues = new HashMap();
        for (Map.Entry<String, Double> entrySet : tfIdfSearchMap.entrySet()) {
            String eachWord = entrySet.getKey();
            Double value = entrySet.getValue();
            if (tfIdfMap.containsKey(eachWord)) {
                HashMap<String, Double> tfIdfValuesMap = tfIdfMap.get(eachWord);
                //getEachDoc
                for (Map.Entry<String, Double> entrySet1 : tfIdfValuesMap.entrySet()) {
                    String fileName = entrySet1.getKey();
                    Double tfIdfValuePerDoc = entrySet1.getValue();
                    if (eachDocumentsTfIdfValues.containsKey(fileName)) {
                        ArrayList<Double> tfIdfValueList = eachDocumentsTfIdfValues.get(fileName);
                        tfIdfValueList.add(tfIdfValuePerDoc);
                        eachDocumentsTfIdfValues.put(fileName, tfIdfValueList);
                    } else {
                        ArrayList<Double> tfIdfValueList = new ArrayList<>();
                        tfIdfValueList.add(tfIdfValuePerDoc);
                        eachDocumentsTfIdfValues.put(fileName, tfIdfValueList);
                    }
                }
            }
        }
        //get cos similarity values
        HashMap<String, Double> cosSimMap = new HashMap<>();
        if (eachDocumentsTfIdfValues.size() > 0) {
            for (Map.Entry<String, ArrayList<Double>> entrySet : eachDocumentsTfIdfValues.entrySet()) {
                String key = entrySet.getKey();
                ArrayList<Double> value = entrySet.getValue();
                List<Double> searchtfIdfValues = new LinkedList<>();
                for (Map.Entry<String, Double> entrySet1 : tfIdfSearchMap.entrySet()) {
                    String key1 = entrySet1.getKey();
                    Double value1 = entrySet1.getValue();
                    searchtfIdfValues.add(value1);
                }
                double cosSim = mt.cosineSimilarity(searchtfIdfValues, value);
                cosSimMap.put(key, cosSim);
            }
        }

        //sort based on the cos sim values
        //display search result
        TreeMap<String, Double> sortedMap = SortByValue(cosSimMap);
        if (sortedMap != null && sortedMap.size() > 0) {
            for (Map.Entry<String, Double> entrySet : sortedMap.entrySet()) {
                String key = entrySet.getKey();
                Double value = entrySet.getValue();
                System.out.println("Doc---->" + key);
                System.out.println("Cosine Sim Value---->" + value);

            }
        }
        else{
            System.out.println("No relevant documents");
        }
    }

    public void indexAllFiles(HashMap<String, HashMap<String, Posting>> mainInvertedIndex, HashMap<String, Integer> docFrequencyMap) throws IOException {

        File directoryPath = new File("/Users/abhishekashwathnarayanvenkat/AlgoTextCorpus");
        List<File> fileList = (List<File>) FileUtils.listFiles(directoryPath, null, true);
        //Stopwords loaded
        getStopWords();

        for (File file : fileList) {
            StringBuilder sbLine = new StringBuilder();
            String docText = null;
            if (file.isFile()) {
                int posCount = 0;
                String fileName = file.getPath();
                Scanner sc = new Scanner(file);
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    line = line.toLowerCase();
                    //Pattern pattern1 = Pattern.compile("\\W|\\S|"+stopWords);
                    //Pattern pattern1 = Pattern.compile("\\b(?:" + stopWords + ")\\b\\s*", Pattern.CASE_INSENSITIVE);

                    //Matcher matcher1 = pattern1.matcher(line);
                    //line = matcher1.replaceAll("");
                    //line = stemmer(line);
                    String eachWordArray[] = line.split(" ");
                    for (String eachWord : eachWordArray) {
                        if (stopWords.contains(eachWord.trim())) {
                            posCount += 1;
                        } else {
                            String word = eachWord.trim();
                            if (word != "" || word != null) {
                                posCount++;
                                if (mainInvertedIndex.containsKey(word)) {
                                    HashMap<String, Posting> docPostingmap = mainInvertedIndex.get(word);
                                    if (docPostingmap.containsKey(fileName)) {
                                        Posting eachPosting = docPostingmap.get(fileName);
                                        eachPosting.term_frequency += 1;
                                        eachPosting.totalWords += 1;
                                        eachPosting.positionList.add(posCount);
                                    } else {
                                        Posting posting = new Posting();
                                        posting.positionList.add(posCount);
                                        posting.term_frequency += 1;
                                        docPostingmap.put(fileName, posting);
                                        int i = docFrequencyMap.get(word);
                                        docFrequencyMap.put(word, i + 1);
                                    }
                                } else {
                                    HashMap<String, Posting> docPostingmap = new HashMap<>();
                                    docFrequencyMap.put(word, 1);
                                    Posting posting = new Posting();
                                    posting.positionList.add(posCount);
                                    posting.term_frequency += 1;
                                    posting.totalWords += 1;
                                    docPostingmap.put(fileName, posting);
                                    mainInvertedIndex.put(word, docPostingmap);
                                }
                            }
                        }
                    }
                }
                sc.close();
            }
        }
        mt.saveObjectToFile("theIndex", mainInvertedIndex);
    }

    /**
     *
     * @param map
     * @return
     */
    public static TreeMap<String, Double> SortByValue(HashMap<String, Double> map) {
        ValueComparator vc = new ValueComparator(map);
        TreeMap<String, Double> sortedMap = new TreeMap<String, Double>(vc);
        sortedMap.putAll(map);
        return sortedMap;
    }

    public String searchWord(String s) {

        return "";
    }

    ////////////////////
    /**
     *
     * @param docVector1
     * @param docVector2
     * @return
     */
    public double cosineSimilarity(List<Double> docVector1, List<Double> docVector2) {
        double dotProduct = 0.0;
        double magnitude1 = 0.0;
        double magnitude2 = 0.0;
        double cosineSimilarity = 0.0;

        while (true) {
            if (docVector2.size() < docVector1.size()) {
                docVector2.add(0.0);
            } else {
                break;
            }
        }
        for (int i = 0; i < docVector1.size(); i++) //docVector1 and docVector2 must be of same length
        {
            dotProduct += docVector1.get(i) * docVector2.get(i);  //a.b
            magnitude1 += Math.pow(docVector1.get(i), 2);  //(a^2)
            magnitude2 += Math.pow(docVector2.get(i), 2); //(b^2)
        }

        magnitude1 = Math.sqrt(magnitude1);//sqrt(a^2)
        magnitude2 = Math.sqrt(magnitude2);//sqrt(b^2)

        if (magnitude1 != 0.0 && magnitude2 != 0.0) {
            cosineSimilarity = dotProduct / (magnitude1 * magnitude2);
        } else {
            return 0.0;
        }
        return cosineSimilarity;
    }
    //////////////////////////

    /**
     *
     * @return @throws IOException
     */
    public static void getStopWords() throws IOException {
        //read each word from the stoplist to create regex string
        String filepath1 = "/Users/abhishekashwathnarayanvenkat/stopwordsFile.txt";
        Path path1 = Paths.get(filepath1);
        Scanner scanner1 = new Scanner(path1);
        //StringBuilder stopWords = new StringBuilder();

        while (scanner1.hasNextLine()) {
            String line = scanner1.nextLine();
            if (line != null && line != "") {
                stopWords.add(line);
            }
        }
        //String allStopwords = stopWords.toString();
        //System.out.println(""+allStopwords);
        //return allStopwords;
    }

    /**
     *
     * @param word
     * @return
     */
    public static String stemmer(String word) {
        StringBuffer stemmed = new StringBuffer();
        porterStemmer stemmer = new porterStemmer();
        String text[] = word.split(" ");
        for (int i = 0; i < text.length; i++) {
            stemmer.setCurrent(text[i]);
            stemmer.stem();
            stemmed.append(stemmer.getCurrent());
            stemmed.append(" ");
        }
        return stemmed.toString();
    }

    public void readObjectFromFile(String fileName) {
        FileInputStream fin = null;
        try {
            fin = new FileInputStream("/Users/abhishekashwathnarayanvenkat/invertedIndex/" + fileName + ".txt");
            ObjectInputStream ois = new ObjectInputStream(fin);
            HashMap<String, HashMap<String, Posting>> corpusInvertedIndex = (HashMap<String, HashMap<String, Posting>>) ois.readObject();
            for (String word : corpusInvertedIndex.keySet()) {
                System.out.print("[token]:" + word);

                HashMap<String, Posting> docPostingMap = corpusInvertedIndex.get(word);
                for (String document : docPostingMap.keySet()) {
                    System.out.print("...... [Doc]:" + document + "....");

                }
                System.out.println("");
            }

        } catch (FileNotFoundException ex) {
            //Logger.getLogger(CreateIndex.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            //Logger.getLogger(CreateIndex.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //Logger.getLogger(CreateIndex.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fin.close();
            } catch (IOException ex) {
                //Logger.getLogger(CreateIndex.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void saveObjectToFile(String fileName, HashMap<String, HashMap<String, Posting>> docInvertedIndex) {

        FileOutputStream fout;
        try {
            fout = new FileOutputStream("/Users/abhishekashwathnarayanvenkat/invertedIndex/" + fileName + ".txt");
            ObjectOutputStream oos = new ObjectOutputStream(fout);

            oos.writeObject(docInvertedIndex);

        } catch (FileNotFoundException ex) {
            //Logger.getLogger(CreateIndex.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //Logger.getLogger(CreateIndex.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
