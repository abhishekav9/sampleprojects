package com.socket.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import javax.swing.JPanel;

import com.socket.CreatePanel;
import com.socket.server.Service;

/**
 * Client thread which sends the XML to the specified IP and port
 * 
 * @author abhishekashwathnarayanvenkat
 *
 */
public class GreetingClient extends Thread {
	private String message;
	private int port;
	private String address;
	private CreatePanel createPanel;

	public GreetingClient(String message, String address, int port, CreatePanel createPanel) {
		this.message = message;
		this.address = address;
		this.port = port;
		this.createPanel = createPanel;
	}
	
	public GreetingClient(String message, String address, int port) {
		this.message = message;
		this.address = address;
		this.port = port;
	}

	// public void sendMessage() {
	// try {
	// System.out.println("Client says: Connecting to server '" + address + "'
	// on port " + port);
	// Socket client = new Socket(address, port);
	// System.out.println("Client says: Connected to: " +
	// client.getRemoteSocketAddress());
	// OutputStream outToServer = client.getOutputStream();
	// DataOutputStream out = new DataOutputStream(outToServer);
	// out.writeUTF(message);
	// // response from server
	// InputStream inFromServer = client.getInputStream();
	// DataInputStream in = new DataInputStream(inFromServer);
	// System.out.println("Client says: Received server response..." +
	// in.readUTF());
	// // response = in.readUTF();
	// client.close();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			if (message == null) {
				Service.sb.append(Thread.currentThread().getName()
						+ " says: Message is null!!! Hence did not send it to server\n");
				System.out.println(
						Thread.currentThread().getName() + " says: Message is null!!! Hence did not send it to server");
			} else {
				Service.sb.append(Thread.currentThread().getName() + " says: Connecting to server '" + address
						+ "' on port " + port + "\n");
				System.out.println(Thread.currentThread().getName() + " says: Connecting to server '" + address
						+ "' on port " + port);
				Socket client = new Socket(address, port);
				Service.sb.append(Thread.currentThread().getName() + " says: Connected to: "
						+ client.getRemoteSocketAddress() + "\n");
				System.out.println(
						Thread.currentThread().getName() + " says: Connected to: " + client.getRemoteSocketAddress());
				OutputStream outToServer = client.getOutputStream();
				DataOutputStream out = new DataOutputStream(outToServer);

				out.writeUTF(message);
				// response from server
				InputStream inFromServer = client.getInputStream();
				DataInputStream in = new DataInputStream(inFromServer);
				String readString = in.readUTF();
				Service.sb.append(
						Thread.currentThread().getName() + " says: Received server response..." + readString + "\n");
				System.out
						.println(Thread.currentThread().getName() + " says: Received server response..." + readString);
				// response = in.readUTF();
				client.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// public static void main(String[] args) {
	// String address = "localhost";
	// int port = 6000;
	// // Service service = Service.getService();
	// // service.getServer(port);
	// String message = "<?xml version='1.0'
	// encoding='UTF-8'?><Message><Command></Command><Data><Row><Description>\"Name\"</Description><Value>\"Mr.
	// Joe Chase\"</Value></Row></Data></Message>";
	// GreetingClient gc = new GreetingClient(message, address, port);
	// gc.sendMessage();
	// }
}