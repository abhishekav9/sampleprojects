package com.socket.pojo;

import java.net.Socket;
import java.util.Date;

public class Work {
	private Socket client;
	private String command;
	private Date date;

	public Socket getClient() {
		return client;
	}

	public Date getDate() {
		return date;
	}

	public String getCommand() {
		return command;
	}

	public Work(Socket client, String command, Date date) {
		this.client = client;
		this.command = command;
		this.date = date;
	}
}
