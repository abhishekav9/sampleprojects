package com.socket.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JPanel;

import com.socket.pojo.Work;

public class GreetingServer extends Thread {
	private ServerSocket serverSocket;
	// private String serverName;
	private Socket client;
	public static BlockingQueue<Work> workQueue = new LinkedBlockingQueue<Work>();
	private ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
	
	public GreetingServer(int port) throws IOException {
		this.serverSocket = new ServerSocket(port);
		// this.serverName = serverName;
		System.out.println("Server says: Waiting for client on port: " + serverSocket.getLocalPort() + "...");
		System.out.println("************************");
		// serverSocket.setSoTimeout(10000);
	}

	public void run() {
		while (true) {
			try {
				client = serverSocket.accept();
				Service.sb.append("Server says: Connected to client @ " + client.getRemoteSocketAddress()+"\n");
				System.out.println("Server says: Connected to client @ " + client.getRemoteSocketAddress());
				WorkerRunnable wr = new WorkerRunnable(client, "My Socket Server");
				threadPool.execute(wr);
			} catch (SocketTimeoutException s) {
				System.out.println("Socket timed out!");
				threadPool.shutdown();
				break;
			} catch (IOException e) {
				System.out.println("IOException!");
				threadPool.shutdown();
				break;
			}
		}
	}

//	public static void main(String[] args) {
//		int port = 6000;
//		try {
//			Thread t = new GreetingServer(port);
//			t.start();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
}