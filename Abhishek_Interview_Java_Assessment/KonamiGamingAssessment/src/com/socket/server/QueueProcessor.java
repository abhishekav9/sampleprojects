package com.socket.server;

import java.io.DataOutputStream;
import java.io.IOException;

import com.socket.pojo.Work;

public class QueueProcessor extends Thread {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			while (true) {
				Work work = Service.workQueue.take();
				//System.out.println("got work");
				DataOutputStream out = new DataOutputStream(work.getClient().getOutputStream());
				out.writeUTF(work.getCommand() + "_:_" + work.getDate().toString());
				work.getClient().close();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
