package com.socket.server;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.socket.pojo.Work;

public class Service {
	public static BlockingQueue<Work> workQueue = new LinkedBlockingQueue<Work>();
	public static StringBuffer sb = new StringBuffer();
	private static Service service = null;

	private Service() {
		// TODO Auto-generated constructor stub
		QueueProcessor qp = new QueueProcessor();
		qp.start();
	}

	public static Service getService() {
		if (service == null) {
			service = new Service();
		}
		return service;
	}

	public GreetingServer startServer(int port) {
		GreetingServer gs = null;
		try {
			gs = new GreetingServer(port);
			gs.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("Error in ServerSocket !!!"+ e.getMessage());
		}
		return gs;
	}

}
