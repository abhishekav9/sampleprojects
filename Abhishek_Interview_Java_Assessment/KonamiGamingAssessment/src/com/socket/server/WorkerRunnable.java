package com.socket.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.socket.pojo.Work;

public class WorkerRunnable implements Runnable {

	private Socket client = null;

	public WorkerRunnable(Socket clientSocket, String serverText) {
		this.client = clientSocket;
		// this.serverText = serverText;
	}

	public void run() {
		try {
			DataInputStream in = new DataInputStream(client.getInputStream());
			String inputData = in.readUTF();
			DataOutputStream out = new DataOutputStream(client.getOutputStream());
			// check if well formed xml or not

			if (validateXml(inputData)) {
				String command = getTagValue(inputData, "Command");
				Service.sb.append("ServerWorker says: XML Found!!: " + command+"\n");
				System.out.println("ServerWorker says: XML Found!!: " + command);
				Date date = new Date();
				Work work = new Work(client, command, date);
				Service.workQueue.put(work);
				Service.sb.append(
						"ServerWorker says: Completed serving client @ " + client.getRemoteSocketAddress() + "\n");
				System.out.println("ServerWorker says: Completed serving client @ " + client.getRemoteSocketAddress());
				// out.writeUTF(command + "_:_" + date.toString());
			} else {
				Service.sb.append("Unknown Command!!_:_" + new Date().toString() + "\n");
				out.writeUTF("Unknown Command!!_:_" + new Date().toString());
			}
		} catch (SocketTimeoutException s) {
			System.out.println("ServerWorker says: Socket timed out!");
		} catch (IOException e) {
			System.out.println("ServerWorker says: IOException!");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("ServerWorker says: Interrupted!");
		}
	}

	public static boolean validateXml(String xml) {
		// the "parse" method also validates XML, will throw an
		// exception if misformatted
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(xml)));
			return true;
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			return false;
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	public static String getTagValue(String xml, String tagName) {
		return xml.split("<" + tagName + ">")[1].split("</" + tagName + ">")[0];
	}
}
