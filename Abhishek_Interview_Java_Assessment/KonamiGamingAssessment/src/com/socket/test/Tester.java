package com.socket.test;

import com.socket.client.GreetingClient;
import com.socket.server.Service;

public class Tester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String address = "localhost";
		int port = 6000;
		Service service = Service.getService();
		service.startServer(port);
		String message1 = "<?xml version='1.0' encoding='UTF-8'?><Message><Command>Print</Command><Data><Row><Description>\"Name\"</Description><Value>\"Mr. Joe Chase\"</Value></Row></Data></Message>";
		String message2 = "<?xml version='1.0' encoding='UTF-8'?><Message><Command>Hello!!</Command><Data><Row><Description>\"Name\"</Description><Value>\"Mr. Joe Chase\"</Value></Row></Data></Message>";
		String message3 = "asdasd";
		GreetingClient gc1 = new GreetingClient(message1, address, port);
		GreetingClient gc2 = new GreetingClient(message2, address, port);
		GreetingClient gc3 = new GreetingClient(message3, address, port);
		gc1.setName("client 1");
		gc2.setName("client 2");
		gc3.setName("client 3");
		gc1.start();
		gc2.start();
		gc3.start();
	}

}
