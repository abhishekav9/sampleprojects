package cmsc433.p3;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.RecursiveTask;

/**
 * This file needs to hold your solver to be tested. You can alter the class to
 * extend any class that extends MazeSolver. It must have a constructor that
 * takes in a Maze. It must have a solve() method that returns the datatype List
 * <Direction> which will either be a reference to a list of steps to take or
 * will be null if the maze cannot be solved.
 */
public class StudentMTMazeSolver extends SkippingMazeSolver {
	// ExecutorService exececutorService;
	boolean isSolFound;
	ForkJoinPool fjPool;

	public StudentMTMazeSolver(Maze maze) {
		super(maze);
		// int processors = Runtime.getRuntime().availableProcessors();
		// exececutorService = Executors.newFixedThreadPool(processors);

	}

	@Override
	public List<Direction> solve() {
		isSolFound = false;
		// LinkedList<ForkJoinTask<LinkedList<Direction>>> forkJoinSolvers = new
		// LinkedList<ForkJoinTask<LinkedList<Direction>>>();
		LinkedList<Future<List<Direction>>> callableSolvers;
		Choice choice = null;
		List<Direction> solution = null;

		try {
			choice = firstChoice(maze.getStart());
		} catch (SolutionFound e) {
			return new LinkedList<Direction>();
		}

		// setting threshold of 200x100.mz beyond which threadPooling is to be
		// used
		if ((maze.height * maze.width) <= 20000) {
			LinkedList<Choice> choiceStack = new LinkedList<Choice>();
			Choice ch;

			System.out.println("height*width<=20000");
			try {
				choiceStack.push(choice);
				while (!choiceStack.isEmpty()) {
					ch = choiceStack.peek();
					if (ch.isDeadend()) {
						// backtrack.
						choiceStack.pop();
						if (!choiceStack.isEmpty())
							choiceStack.peek().choices.pop();
						continue;
					}
					choiceStack.push(follow(ch.at, ch.choices.peek()));
				}
				// No solution found.
				return null;
			} catch (SolutionFound e) {
				Iterator<Choice> iter = choiceStack.iterator();
				LinkedList<Direction> solutionPath = new LinkedList<Direction>();
				while (iter.hasNext()) {
					ch = iter.next();
					solutionPath.push(ch.choices.peek());
				}

				if (maze.display != null)
					maze.display.updateDisplay();
				return pathToFullPath(solutionPath);
			}
		} else {
			callableSolvers = new LinkedList<Future<List<Direction>>>();
			fjPool = new ForkJoinPool();
			// submit each solver and add to a linkedList
			for (Direction direction : choice.choices) {
				CallableSolver eachCallableSolver = new CallableSolver(choice, direction);
				callableSolvers.add(fjPool.submit(eachCallableSolver));
			}
			// shutdown gracefully
			fjPool.shutdown();

			// getting result from each solver
			List<Direction> result = null;
			// System.out.println(forkJoinSolvers.size());
			for (Future<List<Direction>> solver : callableSolvers) {
				try {
					result = solver.get();
					// System.out.println(result);
					if (result != null) {
						solution = result;
					}
				} catch (Exception e) {
					System.out.println("Got an Exception");
					result = null;
				}
			}
			return solution;
		}
	}

	/**
	 * ForkJoinTask used to solve the task
	 * 
	 * @author abhishekashwathnarayanvenkat
	 *
	 */
	public class CallableSolver extends RecursiveTask<List<Direction>> {
		Choice choice;

		public CallableSolver(Choice firstChoice, Direction direction) {
			this.choice = new Choice(firstChoice.at, firstChoice.from, new LinkedList<Direction>());
			this.choice.choices.push(direction);
		}

		@Override
		protected List<Direction> compute() {

			LinkedList<Choice> choiceStack = new LinkedList<Choice>();
			Choice choice;

			try {
				choiceStack.push(this.choice);
				// repeat till choicestack is empty or solution is found
				while (!choiceStack.isEmpty() && !isSolFound) {
					choice = choiceStack.peek();
					if (choice.isDeadend()) {
						// backtrack to pop the deadend choice
						choiceStack.pop();
						if (!choiceStack.isEmpty())
							choiceStack.peek().choices.pop();
						continue;
					}
					// push new choice got after following to a new choice point
					choiceStack.push(follow(choice.at, choice.choices.peek()));
				}
				// if nothing is found then return null
				return null;
			} catch (SolutionFound e) {
				// when solution is found then find the full path and return it
				isSolFound = true;
				Iterator<Choice> iter = choiceStack.iterator();
				LinkedList<Direction> solutionPath = new LinkedList<Direction>();
				while (iter.hasNext()) {
					choice = iter.next();
					solutionPath.push(choice.choices.peek());
				}

				// update the display with the path
				if (maze.display != null)
					maze.display.updateDisplay();
				return pathToFullPath(solutionPath);
			}
		}
	}
}