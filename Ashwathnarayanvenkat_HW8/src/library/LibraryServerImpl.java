package library;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import member.MemberData;

public class LibraryServerImpl implements LibraryServer {

	/**
	 * Constructor for the library server. It is given a number total books to
	 * have, number of copies per book, and maximum books per member. Creates a
	 * number of Book objects based on numBooks to give them to members when
	 * checking them out. The server maintains the properties and enforces them
	 * for future transactions.
	 * 
	 * @param numBooks
	 * @param copiesPerBook
	 * @param booksPerMember
	 */

	private static int memberNumber = 1;
	private int booksPerMember;

	// has map of bookNames and Book Objects
	public ConcurrentHashMap<String, Book> bookNameAndBookMap = new ConcurrentHashMap<String, Book>();
	// has map of memberNames and member ID
	public ConcurrentHashMap<String, Integer> memberNameAndIdMap = new ConcurrentHashMap<String, Integer>();
	// has checked out set of Books of each memberId
	public ConcurrentHashMap<Integer, Set<Book>> memberIdAndCheckedOutBooksMap = new ConcurrentHashMap<Integer, Set<Book>>();
	// has mapping of Books and No.OfCopies
	public ConcurrentHashMap<Book, Integer> bookAndCopiesMap = new ConcurrentHashMap<Book, Integer>();

	public LibraryServerImpl(int numBooks, int copiesPerBook, int booksPerMember) {
		// IMPLEMENT THIS
		this.booksPerMember = booksPerMember;
		StringBuffer sb;
		for (int i = 0; i < numBooks; i++) {
			sb = new StringBuffer("Book" + i);
			Book newBook = new Book("Book" + i);
			bookAndCopiesMap.put(newBook, copiesPerBook);
			bookNameAndBookMap.put(sb.toString(), newBook);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see library.LibraryServer#registerMember(member.Member)
	 */
	@Override
	public Integer registerMember(MemberData memberdata) throws RemoteException {
		// IMPLEMENT THIS
		if (!memberNameAndIdMap.containsKey(memberdata.getName())) {
			memberNameAndIdMap.put(memberdata.getName(), memberNumber);
			memberIdAndCheckedOutBooksMap.put(memberNumber, new HashSet<Book>());
			return memberNumber++;
		}
		// if member is already present
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see library.LibraryServer#checkoutBook(java.lang.String, member.Member)
	 */
	@Override
	public Book checkoutBook(String bookName, MemberData memberdata) throws RemoteException {
		// IMPLEMENT THIS
		if (bookName == null || bookName.isEmpty())
			return null;
		Book toBeCheckedOutBook = bookNameAndBookMap.get(bookName);
		int id = memberdata.getMemberId();
		if (toBeCheckedOutBook == null) {
			return null;
		} else if (!memberNameAndIdMap.containsKey(memberdata.getName())) {
			return null;
		} else if (!memberNameAndIdMap.containsValue(id)) {
			return null;
		} else if (bookAndCopiesMap.containsKey(toBeCheckedOutBook)) {
			if (memberIdAndCheckedOutBooksMap.get(id).contains(toBeCheckedOutBook))
				return null;
			if (memberIdAndCheckedOutBooksMap.get(id).size() >= booksPerMember)
				return null;
			if (bookAndCopiesMap.get(toBeCheckedOutBook) <= 0)
				return null;
		}
		bookAndCopiesMap.put(toBeCheckedOutBook, bookAndCopiesMap.get(toBeCheckedOutBook) - 1);
		memberIdAndCheckedOutBooksMap.get(id).add(toBeCheckedOutBook);
		return toBeCheckedOutBook;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see library.LibraryServer#returnBook(java.lang.String, member.Member)
	 */
	@Override
	public boolean returnBook(String bookName, MemberData memberdata) throws RemoteException {
		// IMPLEMENT THIS
		Book returnBook = bookNameAndBookMap.get(bookName);
		int id = memberdata.getMemberId();
		if (memberNameAndIdMap.containsValue(id)) {
			if (memberIdAndCheckedOutBooksMap.get(id).contains(returnBook)) {
				// add back the copy to the inventory
				bookAndCopiesMap.put(returnBook, bookAndCopiesMap.get(returnBook) + 1);
				// remove the book from checkedout map
				memberIdAndCheckedOutBooksMap.get(id).remove(returnBook);
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see library.LibraryServer#getBookListings()
	 */
	@Override
	public List<String> getBookListings() throws RemoteException {
		// IMPLEMENT THIS
		List<String> bookListing = new LinkedList<String>();
		for (String bookName : bookNameAndBookMap.keySet()) {
			bookListing.add(bookName);
		}
		return bookListing;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see library.LibraryServer#getAvailableBookListings()
	 */
	@Override
	public List<String> getAvailableBookListings() throws RemoteException {
		// IMPLEMENT THIS
		List<String> availableBooks = new LinkedList<String>();
		for (Book book : bookAndCopiesMap.keySet()) {
			if (bookAndCopiesMap.get(book) > 0) {
				availableBooks.add(book.getName());
			}
		}
		return availableBooks;
	}

	public static void main(String[] args) {

		try {
			LibraryServer obj = new LibraryServerImpl(4, 2, 2);
			LibraryServer stub = (LibraryServer) UnicastRemoteObject.exportObject(obj, 0);
			Registry registry = LocateRegistry.createRegistry(1099);
			registry.rebind("LibraryServer", stub);
			System.out.println("LibraryServer bound in registry on port:" + 0);
		} catch (Exception e) {
			System.out.println("ChatServerImpl err: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
