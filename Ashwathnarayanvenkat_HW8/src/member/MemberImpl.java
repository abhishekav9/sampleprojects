package member;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import library.Book;
import library.LibraryServer;

public class MemberImpl implements Member {

	/**
	 * Default constructor of the member client. Initializes variables. You may
	 * add other constructors if you need.
	 * 
	 */
	private LibraryServer libServer;
	private MemberData memberData;
	private static int idCount = 1;

	public MemberImpl() {
		// IMPLEMENT THIS
		memberData = new MemberData("Member" + idCount, new LinkedList<Book>(), null, new LinkedList<String>());
		idCount += 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see member.Member#getName()
	 */
	public String getName() throws RemoteException {
		// IMPLEMENT THIS
		return memberData.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see member.Member#register()
	 */
	public boolean register() throws RemoteException {
		// IMPLEMENT THIS
		int id = libServer.registerMember(memberData);
		if (id != 0) {
			memberData.setMemberId(id);
			return true;
		}
		return false;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see member.Member#checkoutBook(java.lang.String)
	 */
	public boolean checkoutBook(String bookName) throws RemoteException {
		// IMPLEMENT THIS
		Book checkedOutBook = libServer.checkoutBook(bookName, memberData);
		if (checkedOutBook != null) {
			memberData.getBooksCurrCheckedOut().add(checkedOutBook);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see member.Member#returnBook(java.lang.String)
	 */
	public boolean returnBook(String bookName) throws RemoteException {
		// IMPLEMENT THIS
		boolean isReturned = libServer.returnBook(bookName, memberData);
		if (isReturned) {
			for (Book eachBook : memberData.getBooksCurrCheckedOut()) {
				if (eachBook.getName().equals(bookName)) {
					memberData.getBooksCurrCheckedOut().remove(eachBook);
					break;
				}
			}
			memberData.getBooksRead().add(bookName);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see member.Member#getServer()
	 */
	public LibraryServer getServer() throws RemoteException {
		// IMPLEMENT THIS
		return this.libServer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see member.Member#setServer(library.LibraryServer)
	 */
	public void setServer(LibraryServer server) throws RemoteException {
		// IMPLEMENT THIS
		this.libServer = server;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see member.Member#getBooksCheckedOut()
	 */
	public List<Book> getBooksCurrCheckedOut() throws RemoteException {
		// IMPLEMENT THIS

		return memberData.getBooksCurrCheckedOut();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see member.Member#getBooksRead()
	 */
	public List<String> getBooksRead() throws RemoteException {
		// IMPLEMENT THIS
		return memberData.getBooksRead();
	}

	public static void main(String[] args) throws Exception {

/*		int numBooks = 3;
		int copiesPerBook = 1;
		int booksPerMember = 2;
		LibraryServerImpl server = new LibraryServerImpl(numBooks, copiesPerBook, booksPerMember);
		
		try {
			ArrayList<MemberImpl> members = createMembers(1, server);
			MemberImpl member = members.get(0);
			
			assertTrue(member.checkoutBook("Book0"));
			assertTrue(member.checkoutBook("Book1"));
			assertEquals(0,member.getBooksRead().size());
			assertTrue(member.returnBook("Book1"));
			assertEquals(1,member.getBooksRead().size());
			assertTrue(member.returnBook("Book0"));
			assertEquals(2,member.getBooksRead().size());
		} catch (Exception e) {
			fail(e.getMessage());
		}*/
		Registry registry=LocateRegistry.getRegistry("192.168.1.21",1099);
		LibraryServer remoteServer = (LibraryServer)(registry.lookup("LibraryServer"));
		ArrayList<MemberImpl> members = createMembers(1, remoteServer);
		MemberImpl member = members.get(0);
		assertTrue(member.checkoutBook("Book0"));
		assertTrue(member.checkoutBook("Book1"));
		assertEquals(0,member.getBooksRead().size());
		assertTrue(member.returnBook("Book1"));
		assertEquals(1,member.getBooksRead().size());
		assertTrue(member.returnBook("Book0"));
		assertEquals(2,member.getBooksRead().size());
	}
	
	private static ArrayList<MemberImpl> createMembers(int numMembers, LibraryServer server) throws Exception {
		ArrayList<MemberImpl> members = new ArrayList<MemberImpl>();
		// Initialize members
		for (int i=0;i<numMembers;i++) {
			MemberImpl member = new MemberImpl();
			member.setServer(server);
			boolean registerResult = member.register();
			assertTrue(registerResult);
			members.add(member);
		}
		
		return members;
	}

}
