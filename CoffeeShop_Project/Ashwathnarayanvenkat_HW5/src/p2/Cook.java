package p2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Cooks are simulation actors that have at least one field, a name. When
 * running, a cook attempts to retrieve outstanding orders placed by Eaters and
 * process them.
 */
public class Cook implements Runnable {
	private final String name;

	/**
	 * You can feel free modify this constructor. It must take at least the
	 * name, but may take other parameters if you would find adding them useful.
	 *
	 * @param: the
	 *             name of the cook
	 */
	public Cook(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}

	/**
	 * This method executes as follows. The cook tries to retrieve orders placed
	 * by Customers. For each order, a List<Food>, the cook submits each Food
	 * item in the List to an appropriate Machine, by calling makeFood(). Once
	 * all machines have produced the desired Food, the order is complete, and
	 * the Customer is notified. The cook can then go to process the next order.
	 * If during its execution the cook is interrupted (i.e., some other thread
	 * calls the interrupt() method on it, which could raise
	 * InterruptedException if the cook is blocking), then it terminates.
	 */
	public void run() {

		Simulation.logEvent(SimulationEvent.cookStarting(this));
		try {
			while (true) {
				// YOUR CODE GOES HERE...
				Order foodOrder = Simulation.orderQueue.take();
				System.out.println("Order has priority: "+ foodOrder.getPriority());
				Customer customer = foodOrder.getCustomer();
				LinkedList<Food> orderList = foodOrder.getOrder();

				List<Future<Food>> futureFood = new ArrayList<Future<Food>>();

				Simulation.logEvent(
						SimulationEvent.cookReceivedOrder(this, foodOrder.getOrder(), customer.getOrderNum()));
				for (Food food : orderList) {
					Simulation.logEvent(SimulationEvent.cookStartedFood(this, food, customer.getOrderNum()));
					futureFood.add(Simulation.machineMap.get(food).makeFood());
				}

				try {
					for (int i = 0; i < futureFood.size(); i++) {
						Food result = futureFood.get(i).get();
						Simulation.logEvent(SimulationEvent.cookFinishedFood(this, result, customer.getOrderNum()));
					}
				} catch (ExecutionException e) {

				}

				if (customer != null) {
					Simulation.logEvent(SimulationEvent.cookCompletedOrder(
							this, customer.getOrderNum()));
					Simulation.custFlagMap.put(customer, true);
				}
			}
		} catch (InterruptedException e) {
			// This code assumes the provided code in the Simulation class
			// that interrupts each cook thread when all customers are done.
			// You might need to change this if you change how things are
			// done in the Simulation class.
			Simulation.logEvent(SimulationEvent.cookEnding(this));
			Thread.currentThread().interrupt();
		}
	}
}