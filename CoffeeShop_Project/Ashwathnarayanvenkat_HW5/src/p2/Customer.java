package p2;

import java.util.LinkedList;
import java.util.List;

/**
 * Customers are simulation actors that have two fields: a name, and a list of
 * Food items that constitute the Customer's order. When running, an customer
 * attempts to enter the coffee shop (only successful if the coffee shop has a
 * free table), place its order, and then leave the coffee shop when the order
 * is complete.
 */
public class Customer implements Runnable {
	// JUST ONE SET OF IDEAS ON HOW TO SET THINGS UP...
	private final String name;
	private final List<Food> order;
	private final int orderNum;
	private final int priority;
	private static int runningCounter = -1;

	// private static Semaphore tableSemaphore;

	/**
	 * You can feel free modify this constructor. It must take at least the name
	 * and order but may take other parameters if you would find adding them
	 * useful.
	 */
	public Customer(String name, List<Food> order, int priority) {
		this.name = name;
		this.order = order;
		this.orderNum = ++runningCounter;
		this.priority = priority;
		Simulation.custFlagMap.put(this, false);
	}

	public int getPriority() {
		return priority;
	}

	public int getOrderNum() {
		return orderNum;
	}

	public String toString() {
		return name;
	}

	/**
	 * This method defines what an Customer does: The customer attempts to enter
	 * the coffee shop (only successful when the coffee shop has a free table),
	 * place its order, and then leave the coffee shop when the order is
	 * complete.
	 */
	public void run() {
		// YOUR CODE GOES HERE...
		Simulation.logEvent(SimulationEvent.customerStarting(this));
		try {
			Simulation.tableSemaphore.acquire();
			
			Simulation.logEvent(SimulationEvent.customerEnteredCoffeeShop(this));
			Order foodOrder = new Order(priority, this, (LinkedList<Food>) order);
			Simulation.orderQueue.put(foodOrder);
			//System.out.println(Simulation.orderQueue);
			Simulation.logEvent(SimulationEvent.customerPlacedOrder(this, order, orderNum));

			//wait for order to complete
			while (!Simulation.custFlagMap.get(this)) {
				Thread.sleep(10);
			}

			Simulation.logEvent(SimulationEvent.customerReceivedOrder(this, this.order, this.orderNum));

			Simulation.logEvent(SimulationEvent.customerLeavingCoffeeShop(this));

			Simulation.tableSemaphore.release();

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}