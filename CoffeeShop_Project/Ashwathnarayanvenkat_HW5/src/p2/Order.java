package p2;

import java.util.LinkedList;

/*
 * This class is to take the order of a customer with priority
 * To Order in priority queue we implement Comparable and overide compareTo
 */
public class Order implements Comparable<Order> {

	private int priority;
	private LinkedList<Food> order;
	private Customer customer;

	public Order(int priority, Customer customer, LinkedList<Food> order) {
		// TODO Auto-generated constructor stub
		this.priority = priority;
		this.setCustomer(customer);
		this.setOrder(order);
	}

	public int getPriority() {
		return priority;
	}

	public LinkedList<Food> getOrder() {
		return order;
	}

	public void setOrder(LinkedList<Food> order) {
		this.order = order;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/*
	 * Lower value means higher priority
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Order o) {
		// TODO Auto-generated method stub
		if (this.priority < o.priority)
			return -1;
		else if (this.priority > o.priority)
			return 1;
		else
			return 0;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "order of "+customer+" having priority "+priority;
	}
}
