
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/* Number of cancellations for year and month for Airports*/
public class AirportTypeDelayPeriod {

	public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable> {

		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();

		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			// StringTokenizer itr = new StringTokenizer(value.toString());
			// while (itr.hasMoreTokens()) {
			// word.set(itr.nextToken());
			// context.write(word, one);
			// }

			String line = value.toString();
			// String flight[] = line.split(",");
			String[] flight = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);

			boolean checkDelayed = true;
			boolean isOntime = false;
			boolean cancelled = false;
			boolean diverted = false;
			word.set("Total");
			context.write(word, one);
			float ontime = 0;

			String row1 = flight[0].toString().trim();
			row1 = row1.replaceAll("\"", "");

			if (!row1.equalsIgnoreCase("Year")) {

				// System.out.println(flight[22]);
				if (flight[31].isEmpty())
					ontime = 0;
				else
					ontime = Float.parseFloat(flight[31]);

				String row = flight[14].replaceAll("\"", "");

				if (flight[48].equals("A") || flight[48].equals("B") || flight[48].equals("C")
						|| flight[48].equals("D")) {
					word.set(row + " " + flight[0] + " " + flight[2] + " Cancelled");
					context.write(word, one);
					cancelled = true;
				}
				if (Float.parseFloat(flight[49]) > 0) {
					word.set(row + " " + flight[0] + " " + flight[2] + " Diverted");
					context.write(word, one);
					diverted = true;
				}
				if (!diverted && !cancelled && ontime < 14) {
					word.set(row + " " + flight[0] + " " + flight[2] + " OnTime");
					context.write(word, one);
					isOntime = true;
				}

				// Check if Delayed by Carrier
				if (!isOntime && !cancelled && !diverted) {

					// Check if Delayed by Carrier
					if (!flight[56].isEmpty() || !flight[57].isEmpty() || !flight[58].isEmpty()
							|| !flight[59].isEmpty()) {
						if (Float.parseFloat(flight[56]) > 15 || Float.parseFloat(flight[57]) > 15
								|| Float.parseFloat(flight[58]) > 15 || Float.parseFloat(flight[59]) > 15) {
							word.set(row + " " + flight[0] + " " + flight[2] + " Delay");
							context.write(word, one);
							isOntime = false;
						}
					}

				}
			}

		}
	}

	public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
		private IntWritable result = new IntWritable();

		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			result.set(sum);
			context.write(key, result);
		}
	}

	public static void main(String[] args) throws Exception {

		// mergeDataFiles();
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "delay count");
		job.setJarByClass(AirportTypeDelayPeriod.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(IntSumReducer.class);
		job.setReducerClass(IntSumReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
