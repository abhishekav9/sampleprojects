import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.Iterator;

/**
 * Created by Abhishek on 4/12/2016.
 */
public class DelayMonthByCarrier {

    public static class TokenizerMapper
            extends MapReduceBase implements Mapper<Object, Text, Text, IntWritable> {


        private static final IntWritable one = new IntWritable(1);
        private Text word = new Text();

        @Override
        public void map(Object key, Text value, OutputCollector<Text, IntWritable> context, Reporter reporter) throws IOException {

            String line = value.toString();
          //  System.out.println(line);
            String delay[] = line.split(",");

            word.set("Total");
            context.collect(word, one);
            String row1 = delay[0].toString().trim();
            row1 = row1.replaceAll("\"","");
            if (!row1.equalsIgnoreCase("Year")) {

                String emptyString = delay[32].toString().trim();
                emptyString = emptyString.replaceAll("\"","");
                float delayByMin = 0;
                //to check if the column is not empty
                if (!(emptyString.isEmpty())) {
                        delayByMin = Float.parseFloat(emptyString);
                }

                if (delayByMin > 0) {
                    String year = delay[0].toString().trim();
                    String month = month(Integer.parseInt(delay[2].toString().trim()));
                    String carrier = delay[6].toString().trim();
                    carrier = carrier.replaceAll("\"","");
                    String keyWord = carrier + ":" + month + ":" + year;
                    //String keyWord = carrier ;
                    word.set(keyWord);
                    context.collect(word, one);
                }
            }
        }

        public String month(int num){
            switch (num){
                case 1: return "Jan";
                case 2: return "Feb";
                case 3: return "Mar";
                case 4: return "Apr";
                case 5: return "May";
                case 6: return "June";
                case 7: return "July";
                case 8: return "Aug";
                case 9: return "Sept";
                case 10: return "Oct";
                case 11: return "Nov";
                case 12: return "Dec";
                default: return String.valueOf(num);
            }
        }

        public String airlines(String airCode){

            switch (airCode){
                case "9E" : return "Endeavor Air";
                case "AA" : return "American Airlines";
                case "AS" : return "Alaska Airlines";
                case "B6" : return "JetBlue";
                case "DL" : return "Delta Air Lines";
                case "EV" : return "ExpressJet Airlines";
                case "F9" : return "Frontier Airlines";
                case "FL" : return "AirTran";
                case "HA" : return "Hawaiian Airlines";
                case "MQ" : return "Envoy Air";
                case "OO" : return "SkyWest Airlines";
                case "UA" : return "United Airlines";
                case "US" : return "US Airlines";
                case "VX" : return "Virgin America";
                case "WN" : return "SouthWest Airlines";
                case "YV" : return "Mesa Airlines";

                default: return airCode;
            }
        }
    }

    public static class IntSumReducer  extends MapReduceBase implements Reducer<Text,IntWritable,Text,IntWritable>{

        private IntWritable result = new IntWritable();

        @Override
        public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> context, Reporter reporter) throws IOException {

            int sum = 0;
            //int count =0;
            while (values.hasNext()){
                sum += values.next().get();
            }
            result.set(sum);
            context.collect(key, result);
        }
    }


    public static void main(String[] args) throws Exception {

        JobClient client = new JobClient();
        JobConf job = new JobConf(DelayMonthByCarrier.class);
        job.setJobName("Number of passengers between cities");
        job.setJarByClass(DelayMonthByCarrier.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.setInputPaths(job,new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        client.setConf(job);
        try {
            JobClient.runJob(job);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
