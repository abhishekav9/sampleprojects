package com.edu.neu;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;

/**
 * Created by Yashdeep on 4/12/2016.
 */
public class AccidentAnalysis {

    public static class AccidentMapper extends MapReduceBase implements Mapper<Object, Text, Text, IntWritable> {

        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        public void map(Object key, Text value, OutputCollector<Text, IntWritable> context, Reporter reporter) throws IOException{
            String line = value.toString();
            String[] flight = line.split(" (?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);

            word.set("Total");
            context.collect(word, one);

            String dateAttr = flight[4];
            int month = Integer.parseInt(dateAttr.substring(11,13));
            if(month < 4){
                word.set("1st Quarter");
                context.collect(word, one);
            }else if(month < 7){
                word.set("2nd Quarter");
                context.collect(word, one);
            }else if(month < 10){
                word.set("3rd Quarter");
                context.collect(word, one);
            }else{
                word.set("4th Quarter");
                context.collect(word, one);
            }
        }
    }

    public static class AccidentReducer extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable>{

        private IntWritable result = new IntWritable();

        public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> context, Reporter reporter) throws IOException{
            int sum = 0;
            while(values.hasNext())
                sum += values.next().get();
            result.set(sum);
            context.collect(key, result);
        }
    }

    public static void main(String[] args) throws Exception {

        JobClient client = new JobClient();
        JobConf job = new JobConf(AccidentAnalysis.class);
        job.setJobName("accident count");
        job.setJarByClass(AccidentAnalysis.class);
        job.setMapperClass(AccidentMapper.class);
        job.setCombinerClass(AccidentReducer.class);
        job.setReducerClass(AccidentReducer.class);
        job.setInputFormat(XmlInputFormat.class);
        job.set("xmlinput.start", "<ROW");
        job.set("xmlinput.end", "/>");
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.setInputPaths(job,new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        client.setConf(job);
        try {
            JobClient.runJob(job);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





























}
