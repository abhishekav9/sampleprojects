package com.edu.neu;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by Yashdeep on 4/13/2016.
 */
public class AccidentLocation {

    public static class LocationMapper extends MapReduceBase implements Mapper<Object, Text, Text, IntWritable>{

        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        public void map(Object key, Text value, OutputCollector<Text, IntWritable> context, Reporter reporter) throws IOException{
            String line = value.toString();
//            String[] flight = line.split(" ");
            String[] flight = line.split(" (?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
            word.set("Total");
            context.collect(word, one);

            if(flight[6].equals("Country=\"United States\"")){
                String locationAttr = flight[5];
                String location = (locationAttr.substring(9)).toUpperCase();
                if(location.equals("\"\""))
                    word.set("\"Unknown\"");
                else
                    word.set(location);
                context.collect(word, one);
            }


        }
    }

    public static class LocationReducer extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

        private IntWritable result = new IntWritable();
        public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> context, Reporter reporter) throws IOException{
            int sum = 0;
            while(values.hasNext())
                sum += values.next().get();
            result.set(sum);
            context.collect(key, result);
        }
    }

    public static void main(String[] args) throws Exception {

        JobClient client = new JobClient();
        JobConf job = new JobConf(AccidentLocation.class);
        job.setJobName("Location count");
        job.setJarByClass(AccidentLocation.class);
        job.setMapperClass(AccidentLocation.LocationMapper.class);
        job.setCombinerClass(AccidentLocation.LocationReducer.class);
        job.setReducerClass(AccidentLocation.LocationReducer.class);
        job.setInputFormat(XmlInputFormat.class);
        job.set("xmlinput.start", "<ROW");
        job.set("xmlinput.end", "/>");
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.setInputPaths(job,new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        client.setConf(job);
        try {
            JobClient.runJob(job);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

