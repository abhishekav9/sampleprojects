//package com.edu.neu;
//
//import java.io.IOException;
//import java.util.Iterator;
//
//import org.apache.hadoop.fs.Path;
//import org.apache.hadoop.io.IntWritable;
//import org.apache.hadoop.io.Text;
//import org.apache.hadoop.mapred.*;
//import org.apache.hadoop.mapred.MapReduceBase;
//import org.apache.hadoop.mapred.Mapper;
//import org.apache.hadoop.mapred.Reducer;
//import org.apache.hadoop.mapred.FileInputFormat;
//import org.apache.hadoop.mapred.FileOutputFormat;
//
//public class DelayedFlightsRatio {
//
//    public static class TokenizerMapper
//            extends MapReduceBase implements Mapper<Object, Text, Text, IntWritable>{
//
//        private final static IntWritable one = new IntWritable(1);
//        private Text word = new Text();
//
//        @Override
//        public void map(Object key, Text value, OutputCollector<Text, IntWritable> context, Reporter reporter) throws IOException {
//
//            String line     = value.toString();
//            String flight[] = line.split(",");
//
//            boolean checkDelayed = true;
//            boolean isOntime =false;
//            boolean cancelled=false;
//            boolean diverted=false;
//
//            word.set("Total");
//            context.collect(word,one);
//
//
//            //Check if Delayed by Carrier
//            if(Integer.parseInt(flight[22]) > 0) {
//                word.set("Cancelled");
//                context.collect(word,one);
//                cancelled=true;
//            }
//            if(Integer.parseInt(flight[24]) > 0) {
//                word.set("Diverted");
//                context.collect(word,one);
//                diverted=true;
//            }
//            if (!diverted && !cancelled && Integer.parseInt(flight[15]) < 14) {
//                word.set("OnTime");
//                context.collect(word,one);
//                isOntime=true;
//            }
//
//            //Check if Delayed by Carrier
//            if (!isOntime && !cancelled && !diverted) {
//
//                //Check if Delayed by Carrier
//                if(Integer.parseInt(flight[25]) > 15) {
//                    word.set("CarrierDelay");
//                    context.collect(word,one);
//                    isOntime=false;
//                }
//                //Check if Delayed by Weather
//                if(Integer.parseInt(flight[26]) > 15) {
//                    word.set("WeatherDelay");
//                    context.collect(word,one);
//                    isOntime=false;
//                }
//                //Check if Delayed by NAS
//                if(Integer.parseInt(flight[27]) > 15) {
//                    word.set("NASDelay");
//                    context.collect(word,one);
//                    isOntime=false;
//                }
//                //Check if Delayed by Security
//                if(Integer.parseInt(flight[28]) > 15) {
//                    word.set("SecurityDelay");
//                    context.collect(word,one);
//                    isOntime=false;
//                }
//                //  Check if Late Aircraft Delay
////                if(Integer.parseInt(flight[29]) > 15) {
////                    word.set("LateAircraftDelay");
////                    context.collect(word,one);
////                    isOntime=false;
////                }
//            }
//        }
//    }
//
//    public static class IntSumReducer  extends MapReduceBase
//            implements Reducer<Text,IntWritable,Text,IntWritable>{
//
//        private IntWritable result = new IntWritable();
//
//        @Override
//        public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> context, Reporter reporter) throws IOException {
//            int sum = 0;
//            while(values.hasNext()){
//                sum += values.next().get();
//            }
//            result.set(sum);
//            context.collect(key, result);
//        }
//    }
//
//    public static void main(String[] args) throws Exception {
//
//        JobClient client = new JobClient();
//        JobConf job = new JobConf(DelayedFlightsRatio.class);
//        job.setJobName("delay count");
//        job.setJarByClass(DelayedFlightsRatio.class);
//        job.setMapperClass(TokenizerMapper.class);
//        job.setCombinerClass(IntSumReducer.class);
//        job.setReducerClass(IntSumReducer.class);
//        job.setOutputKeyClass(Text.class);
//        job.setOutputValueClass(IntWritable.class);
//        FileInputFormat.setInputPaths(job,new Path(args[0]));
//        FileOutputFormat.setOutputPath(job, new Path(args[1]));
//
//        client.setConf(job);
//        try {
//            JobClient.runJob(job);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
