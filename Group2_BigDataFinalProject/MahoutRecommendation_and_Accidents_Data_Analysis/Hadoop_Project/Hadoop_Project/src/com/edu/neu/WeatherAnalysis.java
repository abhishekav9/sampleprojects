package com.edu.neu;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.Iterator;

/**
 * Created by Yashdeep on 4/13/2016.
 */
public class WeatherAnalysis {

    public static class WeatherMapper extends MapReduceBase implements Mapper<Object, Text, Text, IntWritable> {

        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        public void map(Object key, Text value, OutputCollector<Text, IntWritable> context, Reporter reporter) throws IOException {
            String line = value.toString();
//            String[] flight = line.split(" ");
            String[] flight = line.split(" (?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
            word.set("Total");
            context.collect(word, one);

            String weatherAttr = flight[28];
            String weather = weatherAttr.substring(17);
            if(weather.equals("\"\"") || weather.equals("\"UNK\""))
                word.set("\"Unknown\"");
            else
                word.set(weather);
            context.collect(word, one);
        }
    }

    public static class WeatherReducer extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

        private IntWritable result = new IntWritable();
        public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> context, Reporter reporter) throws IOException{
            int sum = 0;
            while(values.hasNext())
                sum += values.next().get();
            result.set(sum);
            context.collect(key, result);
        }
    }

    public static void main(String[] args) throws Exception {

        JobClient client = new JobClient();
        JobConf job = new JobConf(WeatherAnalysis.class);
        job.setJobName("Weather analysis");
        job.setJarByClass(WeatherAnalysis.class);
        job.setMapperClass(WeatherAnalysis.WeatherMapper.class);
        job.setCombinerClass(WeatherAnalysis.WeatherReducer.class);
        job.setReducerClass(WeatherAnalysis.WeatherReducer.class);
        job.setInputFormat(XmlInputFormat.class);
        job.set("xmlinput.start", "<ROW");
        job.set("xmlinput.end", "/>");
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.setInputPaths(job,new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        client.setConf(job);
        try {
            JobClient.runJob(job);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
