import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/* Number of passengers according to month and destination  */
public class PopularDestinations {

	public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable> {

		//private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();
		private IntWritable passengers = new IntWritable();
		
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

			String line = value.toString();
			 String[] flight = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);

			 passengers.set(Integer.parseInt(flight[0]));
			// passengers.set(Integer.parseInt(flight[0]));
			 /*
			  "A","Scheduled First Class Passenger/ Cargo Service A"
			  "C","Scheduled Coach Passenger/ Cargo Service C"
			  "E","Scheduled Mixed First Class And Coach, Passenger/ Cargo Service E"
              "F","Scheduled Passenger/ Cargo Service F"
			  "L","Non-Scheduled Civilian Passenger/ Cargo Service L"

			  */
			 
			if(flight[35].equals("A") || flight[35].equals("C") || flight[35].equals("E") || flight[35].equals("F")|| flight[35].equals("L")){	 
				  
			word = new Text(flight[33] + " " +flight[25]);
			context.write(word, passengers);
			}
		}
	}

	public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
		private IntWritable result = new IntWritable();
		private int count=0;
		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int cnt = 0;
			for (IntWritable value : values) {
				cnt += value.get();
			}
			count +=cnt; 
			result.set(cnt);
			context.write(key, result);
			//System.out.println(count);
		}
		
	}
	
	public static class IntComparator extends WritableComparator {

	     public IntComparator() {
	         super(IntWritable.class);
	     }

	     @Override
	     public int compare(byte[] b1, int s1, int l1,
	             byte[] b2, int s2, int l2) {

	         Integer v1 = ByteBuffer.wrap(b1, s1, l1).getInt();
	         Integer v2 = ByteBuffer.wrap(b2, s2, l2).getInt();

	         return v1.compareTo(v2) * (-1);
	     }
	 }


	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "Popular Destinations");
		job.setJarByClass(PopularDestinations.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(IntSumReducer.class);
		job.setReducerClass(IntSumReducer.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		//job.setSortComparatorClass(IntComparator.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
