-- MySQL dump 10.13  Distrib 5.6.20, for osx10.8 (x86_64)
--
-- Host: localhost    Database: IT_Asset_Management_Schema
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `pk_asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_name` varchar(45) NOT NULL,
  `fk_product_id` int(11) NOT NULL,
  `fk_invoice_id` int(11) NOT NULL,
  `fk_status_id` int(11) NOT NULL,
  `fk_enterprise_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_asset_id`),
  KEY `fk_asset_enterprise_product_catalog1_idx` (`fk_product_id`),
  KEY `fk_asset_invoice1_idx` (`fk_invoice_id`),
  KEY `fk_asset_asset_status_table1_idx` (`fk_status_id`),
  KEY `fk_asset_enterprise1_idx` (`fk_enterprise_id`),
  CONSTRAINT `fk_asset_asset_status_table1` FOREIGN KEY (`fk_status_id`) REFERENCES `asset_status_table` (`pk_asset_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_asset_enterprise1` FOREIGN KEY (`fk_enterprise_id`) REFERENCES `enterprise` (`pk_enterprise_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_asset_enterprise_product_catalog1` FOREIGN KEY (`fk_product_id`) REFERENCES `enterprise_product_catalog` (`pk_enterprise_product_catalog_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_asset_invoice1` FOREIGN KEY (`fk_invoice_id`) REFERENCES `invoice` (`pk_invoice_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
INSERT INTO `asset` VALUES (1,'Macbook Pro Early 2014',4,1,1,2),(2,'CRMOnDemand Pro',11,2,2,2),(3,'Lenovo ThinkServer',6,3,1,2),(4,'Windows Pro 7',10,4,2,2),(5,'Oracle Relational Database',9,5,2,2),(6,'HP Instajet',7,6,2,2);
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_ownership`
--

DROP TABLE IF EXISTS `asset_ownership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_ownership` (
  `pk_asset_ownership_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_asset_id` int(11) NOT NULL,
  `asset_assigned_date` date DEFAULT NULL,
  `asset_released_date` date DEFAULT NULL,
  `user_account_pk_useraccount_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_asset_ownership_id`),
  KEY `fk_asset_ownership_asset1_idx` (`fk_asset_id`),
  KEY `fk_asset_ownership_user_account1_idx` (`user_account_pk_useraccount_id`),
  CONSTRAINT `fk_asset_ownership_asset1` FOREIGN KEY (`fk_asset_id`) REFERENCES `asset` (`pk_asset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_asset_ownership_user_account1` FOREIGN KEY (`user_account_pk_useraccount_id`) REFERENCES `user_account` (`pk_useraccount_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_ownership`
--

LOCK TABLES `asset_ownership` WRITE;
/*!40000 ALTER TABLE `asset_ownership` DISABLE KEYS */;
INSERT INTO `asset_ownership` VALUES (1,1,'2014-12-09',NULL,10),(2,3,'2014-12-09',NULL,13);
/*!40000 ALTER TABLE `asset_ownership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_status_table`
--

DROP TABLE IF EXISTS `asset_status_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_status_table` (
  `pk_asset_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pk_asset_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_status_table`
--

LOCK TABLES `asset_status_table` WRITE;
/*!40000 ALTER TABLE `asset_status_table` DISABLE KEYS */;
INSERT INTO `asset_status_table` VALUES (1,'In-Use'),(2,'Spare'),(3,'Maintenance'),(4,'Disposed'),(5,'Spare-Maintenance');
/*!40000 ALTER TABLE `asset_status_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_history`
--

DROP TABLE IF EXISTS `comment_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_history` (
  `pk_comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(45) DEFAULT NULL,
  `fk_user_account_comment_id` varchar(45) DEFAULT NULL,
  `fk_work_request_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_comment_id`),
  KEY `fk_comment_history_work_request1_idx` (`fk_work_request_id`),
  CONSTRAINT `fk_comment_history_work_request1` FOREIGN KEY (`fk_work_request_id`) REFERENCES `work_request` (`pk_id_work_request`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_history`
--

LOCK TABLES `comment_history` WRITE;
/*!40000 ALTER TABLE `comment_history` DISABLE KEYS */;
INSERT INTO `comment_history` VALUES (1,'Approved','6',1),(2,'Delivered','9',1),(3,'Approved (Conditionally)','6',2),(4,'Requested for new device','9',2),(5,'Delivered','9',2);
/*!40000 ALTER TABLE `comment_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enterprise`
--

DROP TABLE IF EXISTS `enterprise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise` (
  `pk_enterprise_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `fk_network_id` int(11) NOT NULL,
  `fk_location_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_enterprise_id`),
  KEY `fk_enterprise_location1_idx` (`fk_location_id`),
  KEY `fk_enterprise_network1_idx` (`fk_network_id`),
  CONSTRAINT `fk_enterprise_location1` FOREIGN KEY (`fk_location_id`) REFERENCES `location` (`pk_location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_enterprise_network1` FOREIGN KEY (`fk_network_id`) REFERENCES `network` (`pk_network_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprise`
--

LOCK TABLES `enterprise` WRITE;
/*!40000 ALTER TABLE `enterprise` DISABLE KEYS */;
INSERT INTO `enterprise` VALUES (1,'ITAM-Amer',3,2),(2,'ITAM-USA',4,1);
/*!40000 ALTER TABLE `enterprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enterprise_product_catalog`
--

DROP TABLE IF EXISTS `enterprise_product_catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_product_catalog` (
  `pk_enterprise_product_catalog_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_enterprise_id` int(11) NOT NULL,
  `supplier_product_catalog_pk_product_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_enterprise_product_catalog_id`),
  KEY `fk_enterprise_product_catalog_enterprise1_idx` (`fk_enterprise_id`),
  KEY `fk_enterprise_product_catalog_supplier_product_catalog1_idx` (`supplier_product_catalog_pk_product_id`),
  CONSTRAINT `fk_enterprise_product_catalog_enterprise1` FOREIGN KEY (`fk_enterprise_id`) REFERENCES `enterprise` (`pk_enterprise_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_enterprise_product_catalog_supplier_product_catalog1` FOREIGN KEY (`supplier_product_catalog_pk_product_id`) REFERENCES `supplier_product_catalog` (`pk_product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprise_product_catalog`
--

LOCK TABLES `enterprise_product_catalog` WRITE;
/*!40000 ALTER TABLE `enterprise_product_catalog` DISABLE KEYS */;
INSERT INTO `enterprise_product_catalog` VALUES (4,2,1),(5,2,2),(6,2,3),(7,2,4),(8,2,5),(9,2,6),(10,2,7),(11,2,8);
/*!40000 ALTER TABLE `enterprise_product_catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hardware_asset`
--

DROP TABLE IF EXISTS `hardware_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hardware_asset` (
  `pk_hardware_asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_asset_id` int(11) NOT NULL,
  `serial_number` varchar(45) NOT NULL,
  `warranty_expiration` date NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `mac_address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pk_hardware_asset_id`),
  UNIQUE KEY `ip_address_UNIQUE` (`ip_address`),
  KEY `fk_hardware_asset_asset1_idx` (`fk_asset_id`),
  CONSTRAINT `fk_hardware_asset_asset1` FOREIGN KEY (`fk_asset_id`) REFERENCES `asset` (`pk_asset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hardware_asset`
--

LOCK TABLES `hardware_asset` WRITE;
/*!40000 ALTER TABLE `hardware_asset` DISABLE KEYS */;
INSERT INTO `hardware_asset` VALUES (1,1,'123CD456','2015-12-12','45.45.45.45','adad'),(3,3,'321AB654','2015-12-12','192.168.1.2','zxc'),(4,5,'C20ABGFD34','2015-12-12','192.168.3.4','asda');
/*!40000 ALTER TABLE `hardware_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hardware_product`
--

DROP TABLE IF EXISTS `hardware_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hardware_product` (
  `fk_product_id` int(11) NOT NULL,
  `fk_hardware_type` int(11) NOT NULL,
  PRIMARY KEY (`fk_product_id`),
  KEY `fk_hardware_product_hardware_type_table1_idx` (`fk_hardware_type`),
  KEY `fk_hardware_product_enterprise_product_catalog1_idx` (`fk_product_id`),
  CONSTRAINT `fk_hardware_product_enterprise_product_catalog1` FOREIGN KEY (`fk_product_id`) REFERENCES `supplier_product_catalog` (`pk_product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_hardware_product_hardware_type_table1` FOREIGN KEY (`fk_hardware_type`) REFERENCES `hardware_type_table` (`pk_hardware_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hardware_product`
--

LOCK TABLES `hardware_product` WRITE;
/*!40000 ALTER TABLE `hardware_product` DISABLE KEYS */;
INSERT INTO `hardware_product` VALUES (1,1),(2,1),(3,2),(4,3),(9,4);
/*!40000 ALTER TABLE `hardware_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hardware_type_table`
--

DROP TABLE IF EXISTS `hardware_type_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hardware_type_table` (
  `pk_hardware_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `hardware_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pk_hardware_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hardware_type_table`
--

LOCK TABLES `hardware_type_table` WRITE;
/*!40000 ALTER TABLE `hardware_type_table` DISABLE KEYS */;
INSERT INTO `hardware_type_table` VALUES (1,'Laptop'),(2,'Server'),(3,'Printer'),(4,'Scanner');
/*!40000 ALTER TABLE `hardware_type_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `pk_invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_amt` float NOT NULL,
  `fk_invoice_status` int(11) DEFAULT NULL,
  `qty` tinyint(4) NOT NULL,
  `fk_supplier_product_id` int(11) NOT NULL,
  `fk_enterprise_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_invoice_id`),
  KEY `fk_invoice_supplier_product_catalog1_idx` (`fk_supplier_product_id`),
  KEY `fk_enterprise_invoice_id_idx` (`fk_enterprise_id`),
  KEY `fk_invoice_status_id_idx` (`fk_invoice_status`),
  CONSTRAINT `fk_enterprise_invoice_id` FOREIGN KEY (`fk_enterprise_id`) REFERENCES `enterprise` (`pk_enterprise_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invoice_supplier_product_catalog1` FOREIGN KEY (`fk_supplier_product_id`) REFERENCES `supplier_product_catalog` (`pk_product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_invoice_id` FOREIGN KEY (`fk_invoice_status`) REFERENCES `invoice_status` (`pk_invoice_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (1,1200,5,1,1,2),(2,30000,5,1,8,2),(3,65000,5,1,3,2),(4,1000,5,1,7,2),(5,1000,5,1,6,2),(6,500,5,1,4,2);
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_status`
--

DROP TABLE IF EXISTS `invoice_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_status` (
  `pk_invoice_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pk_invoice_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_status`
--

LOCK TABLES `invoice_status` WRITE;
/*!40000 ALTER TABLE `invoice_status` DISABLE KEYS */;
INSERT INTO `invoice_status` VALUES (1,'Open'),(2,'Completed'),(3,'Inprogress'),(4,'Shipped'),(5,'Mapped-to-Asset'),(6,'Finance-Approved');
/*!40000 ALTER TABLE `invoice_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laptop_desktop_server`
--

DROP TABLE IF EXISTS `laptop_desktop_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `laptop_desktop_server` (
  `pk_laptop_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_hardware_product_id` int(11) NOT NULL,
  `ram_in_gb` int(11) NOT NULL,
  `processor_name` varchar(45) NOT NULL,
  `processor_speed_in_ghz` decimal(4,2) NOT NULL,
  `hard_disk_capacity_in_gb` decimal(9,2) NOT NULL,
  `no_of_procesors` tinyint(4) NOT NULL,
  PRIMARY KEY (`pk_laptop_id`),
  KEY `fk_laptop_desktop_server_hardware_product1_idx` (`fk_hardware_product_id`),
  CONSTRAINT `fk_hardware_id` FOREIGN KEY (`fk_hardware_product_id`) REFERENCES `hardware_product` (`fk_product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laptop_desktop_server`
--

LOCK TABLES `laptop_desktop_server` WRITE;
/*!40000 ALTER TABLE `laptop_desktop_server` DISABLE KEYS */;
INSERT INTO `laptop_desktop_server` VALUES (1,1,8,'Intel Core i5',2.80,128.00,5),(2,2,4,'Intel Core i5',2.00,128.00,5),(3,3,32,'Intel Xeon Haswell',8.00,12000.00,20);
/*!40000 ALTER TABLE `laptop_desktop_server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `license`
--

DROP TABLE IF EXISTS `license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `license` (
  `pk_license_id` int(11) NOT NULL AUTO_INCREMENT,
  `license_key` varchar(45) NOT NULL,
  `fk_license_type_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_license_id`),
  KEY `fk_license_license_type_table1_idx` (`fk_license_type_id`),
  CONSTRAINT `fk_license_license_type_table1` FOREIGN KEY (`fk_license_type_id`) REFERENCES `license_type_table` (`pk_license_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `license`
--

LOCK TABLES `license` WRITE;
/*!40000 ALTER TABLE `license` DISABLE KEYS */;
INSERT INTO `license` VALUES (1,'123-asd-345-asd',1),(2,'234-zxc-254-zxc',1),(3,'345-poi-543-poi',1);
/*!40000 ALTER TABLE `license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `license_type_table`
--

DROP TABLE IF EXISTS `license_type_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `license_type_table` (
  `pk_license_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `license_type` varchar(45) NOT NULL,
  PRIMARY KEY (`pk_license_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `license_type_table`
--

LOCK TABLES `license_type_table` WRITE;
/*!40000 ALTER TABLE `license_type_table` DISABLE KEYS */;
INSERT INTO `license_type_table` VALUES (1,'Single User'),(2,'Concurrent User'),(3,'Single Workstation'),(4,'Group WorkStation'),(5,'Trial'),(6,'Perpetual');
/*!40000 ALTER TABLE `license_type_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `pk_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `address_line_1` varchar(45) DEFAULT NULL,
  `address_line_2` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `zipcode` char(5) DEFAULT NULL,
  PRIMARY KEY (`pk_location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Business Tech Park',NULL,'Cambridge','MA','USA','02410'),(2,'124, Park Drive','Block D','Boston','MA','USA','02125');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturer` (
  `pk_manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pk_manufacturer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturer`
--

LOCK TABLES `manufacturer` WRITE;
/*!40000 ALTER TABLE `manufacturer` DISABLE KEYS */;
INSERT INTO `manufacturer` VALUES (1,'Apple'),(2,'Dell'),(3,'Lenovo'),(4,'Oracle'),(5,'Microsoft'),(6,'HP'),(7,'Adobe');
/*!40000 ALTER TABLE `manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `network`
--

DROP TABLE IF EXISTS `network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `network` (
  `pk_network_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `fk_network_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk_network_id`),
  KEY `fk_network_network1_idx` (`fk_network_id`),
  CONSTRAINT `fk_network_network1` FOREIGN KEY (`fk_network_id`) REFERENCES `network` (`pk_network_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `network`
--

LOCK TABLES `network` WRITE;
/*!40000 ALTER TABLE `network` DISABLE KEYS */;
INSERT INTO `network` VALUES (1,'Asia-Pacific',NULL),(2,'India',1),(3,'Americas',NULL),(4,'USA',3);
/*!40000 ALTER TABLE `network` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organization`
--

DROP TABLE IF EXISTS `organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization` (
  `pk_organization_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `fk_org_type_id` int(11) NOT NULL,
  `fk_enterprise_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_organization_id`),
  KEY `fk_organization_organization_types1_idx` (`fk_org_type_id`),
  KEY `fk_org_enterprise_idx` (`fk_enterprise_id`),
  CONSTRAINT `fk_org_enterprise` FOREIGN KEY (`fk_enterprise_id`) REFERENCES `enterprise` (`pk_enterprise_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_organization_organization_types1` FOREIGN KEY (`fk_org_type_id`) REFERENCES `organization_types` (`pk_org_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization`
--

LOCK TABLES `organization` WRITE;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;
INSERT INTO `organization` VALUES (1,'Admin-USA',1,2),(2,'Development-USA',2,2),(3,'Finance-USA',6,2),(4,'Sales-USA',3,2),(5,'Support-USA',4,2),(6,'Supplier-USA',7,2),(7,'Asset Mngt-USA',8,2);
/*!40000 ALTER TABLE `organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organization_types`
--

DROP TABLE IF EXISTS `organization_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_types` (
  `pk_org_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pk_org_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization_types`
--

LOCK TABLES `organization_types` WRITE;
/*!40000 ALTER TABLE `organization_types` DISABLE KEYS */;
INSERT INTO `organization_types` VALUES (1,'Admin'),(2,'Development'),(3,'Sales'),(4,'Support'),(6,'Finance'),(7,'Supplier'),(8,'Asset Mngt');
/*!40000 ALTER TABLE `organization_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `printer`
--

DROP TABLE IF EXISTS `printer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `printer` (
  `pk_printer_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_hardware_product_id` int(11) NOT NULL,
  `printer_dpi` int(11) NOT NULL,
  `printer_paper_capacity` int(11) NOT NULL,
  PRIMARY KEY (`pk_printer_id`),
  KEY `fk_printer_hardware_product1_idx` (`fk_hardware_product_id`),
  CONSTRAINT `fk_hardware_print_id` FOREIGN KEY (`fk_hardware_product_id`) REFERENCES `hardware_product` (`fk_product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `printer`
--

LOCK TABLES `printer` WRITE;
/*!40000 ALTER TABLE `printer` DISABLE KEYS */;
INSERT INTO `printer` VALUES (3,4,450,300);
/*!40000 ALTER TABLE `printer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `products_in_use`
--

DROP TABLE IF EXISTS `products_in_use`;
/*!50001 DROP VIEW IF EXISTS `products_in_use`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `products_in_use` AS SELECT 
 1 AS `model`,
 1 AS `product_name`,
 1 AS `version`,
 1 AS `name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `products_in_use_by_users`
--

DROP TABLE IF EXISTS `products_in_use_by_users`;
/*!50001 DROP VIEW IF EXISTS `products_in_use_by_users`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `products_in_use_by_users` AS SELECT 
 1 AS `model`,
 1 AS `product_name`,
 1 AS `version`,
 1 AS `name`,
 1 AS `username`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `role_organization`
--

DROP TABLE IF EXISTS `role_organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_organization` (
  `pk_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_organization_id` int(11) NOT NULL,
  `fk_role_type` int(11) NOT NULL,
  PRIMARY KEY (`pk_role_id`),
  KEY `fk_role_organization1_idx` (`fk_organization_id`),
  KEY `fk_role_type_idx` (`fk_role_type`),
  CONSTRAINT `fk_role_organization1` FOREIGN KEY (`fk_organization_id`) REFERENCES `organization` (`pk_organization_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_type` FOREIGN KEY (`fk_role_type`) REFERENCES `role_types` (`pk_role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_organization`
--

LOCK TABLES `role_organization` WRITE;
/*!40000 ALTER TABLE `role_organization` DISABLE KEYS */;
INSERT INTO `role_organization` VALUES (1,1,10),(2,1,4),(3,2,2),(4,2,3),(5,2,7),(6,3,5),(7,4,6),(8,5,9),(9,6,8),(10,7,1);
/*!40000 ALTER TABLE `role_organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_types`
--

DROP TABLE IF EXISTS `role_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_types` (
  `pk_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) NOT NULL,
  PRIMARY KEY (`pk_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_types`
--

LOCK TABLES `role_types` WRITE;
/*!40000 ALTER TABLE `role_types` DISABLE KEYS */;
INSERT INTO `role_types` VALUES (1,'Asset Manager'),(2,'Developer'),(3,'Development Manager'),(4,'Enterprise Administrator'),(5,'Finance Manager'),(6,'Sales Manager'),(7,'Senior Developer'),(8,'Supplier'),(9,'Support Staff'),(10,'System Administrator');
/*!40000 ALTER TABLE `role_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scanner`
--

DROP TABLE IF EXISTS `scanner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scanner` (
  `pk_scanner_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_hardware_product_id` int(11) NOT NULL,
  `scanner_dpi` int(11) NOT NULL,
  `max_scan_area_in_cm` decimal(4,2) NOT NULL,
  PRIMARY KEY (`pk_scanner_id`),
  KEY `fk_scanner_hardware_product1_idx` (`fk_hardware_product_id`),
  CONSTRAINT `fk_hardware_scan_id` FOREIGN KEY (`fk_hardware_product_id`) REFERENCES `hardware_product` (`fk_product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scanner`
--

LOCK TABLES `scanner` WRITE;
/*!40000 ALTER TABLE `scanner` DISABLE KEYS */;
INSERT INTO `scanner` VALUES (1,9,450,13.00);
/*!40000 ALTER TABLE `scanner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software_asset`
--

DROP TABLE IF EXISTS `software_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `software_asset` (
  `pk_software_asset_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_asset_id` int(11) NOT NULL,
  `fk_license_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_software_asset_id`),
  KEY `fk_software_asset_license_idx` (`fk_license_id`),
  KEY `fk_software_asset_asset1_idx` (`fk_asset_id`),
  CONSTRAINT `fk_software_asset_asset1` FOREIGN KEY (`fk_asset_id`) REFERENCES `asset` (`pk_asset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_software_asset_license` FOREIGN KEY (`fk_license_id`) REFERENCES `license` (`pk_license_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software_asset`
--

LOCK TABLES `software_asset` WRITE;
/*!40000 ALTER TABLE `software_asset` DISABLE KEYS */;
INSERT INTO `software_asset` VALUES (1,2,1),(2,4,2),(3,5,3);
/*!40000 ALTER TABLE `software_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software_category`
--

DROP TABLE IF EXISTS `software_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `software_category` (
  `pk_software_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) NOT NULL,
  PRIMARY KEY (`pk_software_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software_category`
--

LOCK TABLES `software_category` WRITE;
/*!40000 ALTER TABLE `software_category` DISABLE KEYS */;
INSERT INTO `software_category` VALUES (1,'Development'),(2,'Sales'),(3,'General'),(5,'Marketing'),(6,'Education');
/*!40000 ALTER TABLE `software_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software_product`
--

DROP TABLE IF EXISTS `software_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `software_product` (
  `fk_category_id` int(11) NOT NULL,
  `fk_type_id` int(11) NOT NULL,
  `size_in_mb` int(11) NOT NULL,
  `fk_product_id` int(11) NOT NULL,
  PRIMARY KEY (`fk_product_id`),
  KEY `fk_software_product_software_category1_idx` (`fk_category_id`),
  KEY `fk_software_product_software_type1_idx` (`fk_type_id`),
  KEY `fk_software_product_enterprise_product_catalog1_idx` (`fk_product_id`),
  CONSTRAINT `fk_software_product_enterprise_product_catalog1` FOREIGN KEY (`fk_product_id`) REFERENCES `supplier_product_catalog` (`pk_product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_software_product_software_category1` FOREIGN KEY (`fk_category_id`) REFERENCES `software_category` (`pk_software_category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_software_product_software_type1` FOREIGN KEY (`fk_type_id`) REFERENCES `software_type` (`pk_software_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software_product`
--

LOCK TABLES `software_product` WRITE;
/*!40000 ALTER TABLE `software_product` DISABLE KEYS */;
INSERT INTO `software_product` VALUES (5,7,900,5),(1,4,2000,6),(3,1,15000,7),(2,8,40000,8);
/*!40000 ALTER TABLE `software_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software_type`
--

DROP TABLE IF EXISTS `software_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `software_type` (
  `pk_software_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`pk_software_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software_type`
--

LOCK TABLES `software_type` WRITE;
/*!40000 ALTER TABLE `software_type` DISABLE KEYS */;
INSERT INTO `software_type` VALUES (1,'Operating System'),(2,'Multimedia'),(3,'Web Server'),(4,'Database'),(5,'Virtual Machine'),(6,'Games'),(7,'Graphics'),(8,'Application');
/*!40000 ALTER TABLE `software_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_product_catalog`
--

DROP TABLE IF EXISTS `supplier_product_catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_product_catalog` (
  `pk_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(45) NOT NULL,
  `model` varchar(45) NOT NULL,
  `version` varchar(45) NOT NULL,
  `cost` float NOT NULL,
  `manufacturer_pk_manufacturer_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_product_id`),
  KEY `fk_supplier_product_catalog_manufacturer1_idx` (`manufacturer_pk_manufacturer_id`),
  CONSTRAINT `fk_supplier_product_catalog_manufacturer1` FOREIGN KEY (`manufacturer_pk_manufacturer_id`) REFERENCES `manufacturer` (`pk_manufacturer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_product_catalog`
--

LOCK TABLES `supplier_product_catalog` WRITE;
/*!40000 ALTER TABLE `supplier_product_catalog` DISABLE KEYS */;
INSERT INTO `supplier_product_catalog` VALUES (1,'Macbook','Pro','Early 2014',1200,1),(2,'Macbook','Air','Early 2014',700,1),(3,'ThinkServer','TS140','1500',65000,3),(4,'Instajet','500S','11x8.13',500,6),(5,'Photoshop','CS','5',300,7),(6,'Relational Database','10g','9.1',1000,4),(7,'Windows','Pro','7',1000,5),(8,'CRMOnDemand','Pro','13.67',30000,4),(9,'ScanIt','S450','Late 2014',300,6);
/*!40000 ALTER TABLE `supplier_product_catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `pk_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email_id` varchar(45) NOT NULL,
  `fk_organization_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_user_id`),
  KEY `fk_user_organization1_idx` (`fk_organization_id`),
  CONSTRAINT `fk_user_organization1` FOREIGN KEY (`fk_organization_id`) REFERENCES `organization` (`pk_organization_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Admin','Admin','admin@qweqw.net',1),(2,'Dev-staff','Dev_staff','afaf@1.com',2),(3,'Dev-Man','Dev_man','asfa@afa.com',2),(4,'Finance','Fin','ads@afa.net',3),(5,'Sales','SManager','fd@aefa.com',4),(6,'Support','supManger','sfdsf@.com',5),(7,'Supplier','supplier','adasd@aefa.com',6),(8,'itman','Itman','aef@afa.com',7);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `person_validate_insert` before INSERT on USER FOR each row
BEGIN 
CALL procedure_to_validate_email_id(NEW.email_id);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `person_validate_update` before UPDATE on USER FOR each row
BEGIN 
CALL procedure_to_validate_email_id(NEW.email_id);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user_account`
--

DROP TABLE IF EXISTS `user_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_account` (
  `pk_useraccount_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_user_id` int(11) NOT NULL,
  `fk_role_id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`pk_useraccount_id`),
  KEY `fk_user_account_user1_idx` (`fk_user_id`),
  KEY `fk_user_account_role1_idx` (`fk_role_id`),
  CONSTRAINT `fk_user_account_role1` FOREIGN KEY (`fk_role_id`) REFERENCES `role_organization` (`pk_role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_account_user1` FOREIGN KEY (`fk_user_id`) REFERENCES `user` (`pk_user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_account`
--

LOCK TABLES `user_account` WRITE;
/*!40000 ALTER TABLE `user_account` DISABLE KEYS */;
INSERT INTO `user_account` VALUES (4,1,1,'sysadmin','a159b7ae81ba3552af61e9731b20870515944538'),(5,1,2,'eadmin','2811611c9c58acd4913e21dd9cdb4f5ece563df0'),(6,3,4,'man','8175e3c8753aeb1696959f72ede260ebf3ea14c5'),(7,4,6,'fin','b76049e61518de9781f18a52d2d89b5f4ac6ae9f'),(8,5,7,'sales','59248c4dae276a021cb296d2ee0e6a0c962a8d7f'),(9,6,8,'sup','3844b17b367801f41a3ff27aab7d5ca297c2b984'),(10,2,3,'dev','34c6fceca75e456f25e7e99531e2425c6c1de443'),(11,7,9,'qwe','056eafe7cf52220de2df36845b8ed170c67e23e3'),(12,8,10,'itam','f8c27acc5a6d9dc1555fc641173f5be63e047ded'),(13,2,5,'sen','4fc5e2d447ed635b4de680b8ffd419b8f08346f8');
/*!40000 ALTER TABLE `user_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_request`
--

DROP TABLE IF EXISTS `work_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_request` (
  `pk_id_work_request` int(11) NOT NULL AUTO_INCREMENT,
  `enterprise_product_catalog_pk_enterprise_product_catalog_id` int(11) NOT NULL,
  `fk_sender_id` int(11) NOT NULL,
  `fk_reciever_id` int(11) DEFAULT NULL,
  `work_status_pk_status_id` int(11) NOT NULL,
  `organization_pk_organization_id` int(11) NOT NULL,
  PRIMARY KEY (`pk_id_work_request`),
  KEY `fk_work_request_enterprise_product_catalog1_idx` (`enterprise_product_catalog_pk_enterprise_product_catalog_id`),
  KEY `fk_work_request_user_account1_idx` (`fk_sender_id`),
  KEY `fk_work_request_user_account2_idx` (`fk_reciever_id`),
  KEY `fk_work_request_work_status1_idx` (`work_status_pk_status_id`),
  KEY `fk_work_request_organization1_idx` (`organization_pk_organization_id`),
  CONSTRAINT `fk_work_request_enterprise_product_catalog1` FOREIGN KEY (`enterprise_product_catalog_pk_enterprise_product_catalog_id`) REFERENCES `enterprise_product_catalog` (`pk_enterprise_product_catalog_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_request_organization1` FOREIGN KEY (`organization_pk_organization_id`) REFERENCES `organization` (`pk_organization_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_request_user_account1` FOREIGN KEY (`fk_sender_id`) REFERENCES `user_account` (`pk_useraccount_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_request_user_account2` FOREIGN KEY (`fk_reciever_id`) REFERENCES `user_account` (`pk_useraccount_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_request_work_status1` FOREIGN KEY (`work_status_pk_status_id`) REFERENCES `work_status` (`pk_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_request`
--

LOCK TABLES `work_request` WRITE;
/*!40000 ALTER TABLE `work_request` DISABLE KEYS */;
INSERT INTO `work_request` VALUES (1,4,10,9,5,2),(2,6,13,9,5,2);
/*!40000 ALTER TABLE `work_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_status`
--

DROP TABLE IF EXISTS `work_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_status` (
  `pk_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pk_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_status`
--

LOCK TABLES `work_status` WRITE;
/*!40000 ALTER TABLE `work_status` DISABLE KEYS */;
INSERT INTO `work_status` VALUES (1,'Open'),(2,'Approved'),(3,'In-Progress'),(4,'Awaiting-Input'),(5,'Closed'),(6,'Reopen');
/*!40000 ALTER TABLE `work_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `products_in_use`
--

/*!50001 DROP VIEW IF EXISTS `products_in_use`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `products_in_use` AS select distinct `spc`.`model` AS `model`,`spc`.`product_name` AS `product_name`,`spc`.`version` AS `version`,`manufacturer`.`name` AS `name` from ((((`supplier_product_catalog` `spc` join `enterprise_product_catalog` `epc` on((`spc`.`pk_product_id` = `epc`.`supplier_product_catalog_pk_product_id`))) join `asset` on((`asset`.`fk_product_id` = `epc`.`pk_enterprise_product_catalog_id`))) join `asset_ownership` on((`asset`.`pk_asset_id` = `asset_ownership`.`fk_asset_id`))) join `manufacturer` on((`manufacturer`.`pk_manufacturer_id` = `spc`.`manufacturer_pk_manufacturer_id`))) where (`asset`.`fk_status_id` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `products_in_use_by_users`
--

/*!50001 DROP VIEW IF EXISTS `products_in_use_by_users`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `products_in_use_by_users` AS select distinct `spc`.`model` AS `model`,`spc`.`product_name` AS `product_name`,`spc`.`version` AS `version`,`manufacturer`.`name` AS `name`,`user_account`.`username` AS `username` from (((((`supplier_product_catalog` `spc` join `enterprise_product_catalog` `epc` on((`spc`.`pk_product_id` = `epc`.`supplier_product_catalog_pk_product_id`))) join `asset` on((`asset`.`fk_product_id` = `epc`.`pk_enterprise_product_catalog_id`))) join `asset_ownership` on((`asset`.`pk_asset_id` = `asset_ownership`.`fk_asset_id`))) join `user_account` on((`asset_ownership`.`user_account_pk_useraccount_id` = `user_account`.`pk_useraccount_id`))) join `manufacturer` on((`manufacturer`.`pk_manufacturer_id` = `spc`.`manufacturer_pk_manufacturer_id`))) where (`asset`.`fk_status_id` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-09 23:02:01
