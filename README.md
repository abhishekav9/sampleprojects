# README #

This repository has some of my sample projects that I have worked on.
I upload it once I am done with application and then update if there are some bug fixes / enhancements.

### What is this repository for? ###

* Self_Service_Portal - 
• Developed a Spring MVC web-app where a university can manage a coursework and career system with its students.
• Employed Velocity, Freemarker, JSP with Hibernate inheritance mappings and used hibernate named query, criteria API and HQL with joins. Performed session validation, role management and cookie handling.

* CoffeeShop_Project -
• Developed a multithreaded and parallelized Java program for simulation of customers entering and ordering different items in a coffee shop with limited resources using Java’s concurrent collection packages.
• Designed using Blocking queues, Semaphores, FutureTask, Concurrent Hashmaps and Synchronized Lists.

* IT_Asset_Manegement_DB/Abhishek_Ashwathnarayanvenkat_Database-
• Designed a database for the system by creating an effective ER-diagram with 32 entities and forward engineered it.
• Designed to manage an enterprise’s assets and it’s finances with role based visibility of users and to answer BI questions.

* WebDesignProject 4-
• A webdesign project involving HTML5, CSS3, bootstrap, javascript and jQuery.

* Abhishek_Ashwathnarayanvenkat_ITAMSystem - 
• Designed a Java swing application using best practices to manage an enterprise’s assets and it’s finances with role based visibility of users and to answer BI questions using Java 7, MySQL, Swing UI plugins and Quartz schedulers.

* Ashwathnarayanvenkat_HW2
• It is an Auction Server simulation project in which sellers offer items and bidders bid on them by requesting listing of current items, place a bid and check outcome of a bid.

* Ashwathnarayanvenkat_HW8
• It is a basic RMI project involving members of a library trying to checkout the books remotely.
• The library has exposed the method to the client (members) to access the books and check them out and return the books.

### How do I get set up? ###

* IT_Asset_Manegement_DB/Abhishek_Ashwathnarayanvenkat_Database - Needs MySQL 5.x
* Self_Service_Portal - This is a Spring and Hibernate project is best run on Spring Tool Suite
* WebDesignProject 4 - This is not a server project (has only html pages) and can be opened in the browser starting from index.html
*** All other projects can be configured in eclipse and can be run from the class having main method***

### Who do I talk to? ###

Please email me for any clarifications: abhishekav9@gmail.com