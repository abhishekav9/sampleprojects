CREATE DATABASE  IF NOT EXISTS `WebTools_Final_Project` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `WebTools_Final_Project`;
-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: 127.0.0.1    Database: WebTools_Final_Project
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `announcement`
--

DROP TABLE IF EXISTS `announcement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement` (
  `announcementId` int(11) NOT NULL AUTO_INCREMENT,
  `announcementTitle` varchar(100) DEFAULT NULL,
  `announcementBody` varchar(500) DEFAULT NULL,
  `announcementType` varchar(50) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `student_studentId` int(11) DEFAULT NULL,
  `announcementCategory` varchar(45) DEFAULT NULL,
  `course_offering_courseRegnNum` int(11) DEFAULT NULL,
  PRIMARY KEY (`announcementId`),
  KEY `fk_announcement_student1_idx` (`student_studentId`),
  KEY `fk_announcement_course_offering1_idx` (`course_offering_courseRegnNum`),
  CONSTRAINT `fk_announcement_course_offering1` FOREIGN KEY (`course_offering_courseRegnNum`) REFERENCES `course_offering` (`courseRegnNum`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_announcement_student1` FOREIGN KEY (`student_studentId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcement`
--

LOCK TABLES `announcement` WRITE;
/*!40000 ALTER TABLE `announcement` DISABLE KEYS */;
INSERT INTO `announcement` VALUES (1,'Direct access to NEU Blackboard','We recommend logging into Blackboard directly using the address http://blackboard.neu.edu.  Bookmark the address so you will always have direct access to Blackboard.','System Announcement','2015-04-18 00:00:00',NULL,'Information',NULL),(2,'No Wi-Fi from 2015-04-18 00:00 to 2015-04-19 00:00','There will be no Wi-Fi for the above mentioned dates. Sorry for the inconvenience cause.','System Announcement','2015-04-18 00:00:00',NULL,'Warning',NULL),(3,'Renew your clicker account','Renew your account before it expires on 04/20/2015','Personal Announcement','2015-04-18 00:00:00',1,'Warning',NULL);
/*!40000 ALTER TABLE `announcement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment`
--

DROP TABLE IF EXISTS `assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment` (
  `idassignment` int(11) NOT NULL AUTO_INCREMENT,
  `dueDate` date DEFAULT NULL,
  `course_offering_courseRegnNum` int(11) NOT NULL,
  `maxMarks` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idassignment`),
  KEY `fk_assignment_course_offering1_idx` (`course_offering_courseRegnNum`),
  CONSTRAINT `fk_assignment_course_offering1` FOREIGN KEY (`course_offering_courseRegnNum`) REFERENCES `course_offering` (`courseRegnNum`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment`
--

LOCK TABLES `assignment` WRITE;
/*!40000 ALTER TABLE `assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `isbn` int(11) NOT NULL AUTO_INCREMENT,
  `bookName` varchar(100) NOT NULL,
  `bookDesc` varchar(500) DEFAULT NULL,
  `bookCategory` varchar(45) NOT NULL,
  `bookAvailability` int(11) NOT NULL,
  `bookAuthor` varchar(100) NOT NULL,
  `bookEdition` varchar(45) DEFAULT NULL,
  `bookPrice` float DEFAULT NULL,
  `bookPath` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'Database Design','Setting objectives for your database, and transforming those objectives into real designs\nAnalyzing a current database so you can identify ways to improve it\nEstablishing table structures and relationships, assigning primary keys, setting field specifications, and setting up views\nEnsuring the appropriate level of data integrity for each application\nIdentifying and establishing business rules','ENG',12,'Alexis Sanchez','1',35.45,NULL),(2,'Algorithms','This book surveys the most important computer algorithms currently in use and provides a full treatment of data structures and algorithms for sorting, searching, graph processing, and string processing','CSC',15,'Sedgewick Wayne','15',67,NULL),(4,'Allegiant','What if your whole world was a lie? What if a single revelation—like a single choice—changed everything? What if love and loyalty made you do things you never expected?\nThe explosive conclusion to Veronica Roth\'s #1 New York Times bestselling Divergent trilogy reveals the secrets of the dystopian world that has captivated millions of readers in Divergent and Insurgent.','FIC',1,'Veronica Roth  ','1',11,NULL),(5,'American Sniper: The Autobiography of the Most Lethal Sniper in U.S. Military History','From 1999 to 2009, U.S. Navy SEAL Chris Kyle recorded the most career sniper kills in United States military history. The Pentagon has officially confirmed more than 150 of Kyles kills (the previous American record was 109), but it has declined to verify the astonishing total number for this book.','BIO',2,'Chris Kyle','1',9,NULL),(6,'Machinery\'s Handbook','Micromachining section is entirely new to this edition\nExpanded material on the calculation of hole coordinates\nIntroduction to Metrology, the science of measurement\nAdditional content in sheet metal and presses; shaft alignment; taps and tapping; helical coil screw thread inserts; solid geometry; how to distinguish between bolts and screws; statistics, calculating thread dimensions, keys and keyways, miniature screws, metric screw threads and fluid mechanics.','ENG',15,'Erik Oberg','29',67.98,NULL),(7,'The Pipe Fitters Blue Book',NULL,'ENG',12,'W. V. Graves','15',20,NULL);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `companyId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `industry` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`companyId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'IBM','Software','www.ibm.com'),(2,'TripAdvisor','Travel','www.tripadvisor.com'),(3,'PayPal','Finance','www.paypal.com'),(4,'Amazon','e-Commerce','www.amazon.com'),(5,'Staples','Retail','www.staples.com'),(6,'Abine, Inc.','Software','www.abine.com'),(7,'Acme Packet, Inc.','Software','www.oracle.com/us/corporate/acquisitions/ac'),(8,'Affirmed Networks','Telecom','http://affirmednetworks.com'),(9,'Hubway','Transportation','www.thehubway.com/');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `courseId` int(11) NOT NULL AUTO_INCREMENT,
  `courseDesc` varchar(500) NOT NULL,
  `courseName` varchar(45) NOT NULL,
  `credits` int(11) NOT NULL,
  `courseDuration` int(11) NOT NULL,
  `costPerCredit` float NOT NULL,
  `program_programId` int(11) NOT NULL,
  `courseCode` varchar(45) NOT NULL,
  PRIMARY KEY (`courseId`),
  UNIQUE KEY `courseCode_UNIQUE` (`courseCode`),
  KEY `fk_course_program1_idx` (`program_programId`),
  CONSTRAINT `fk_course_program1` FOREIGN KEY (`program_programId`) REFERENCES `program` (`programId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (3,'Data structures and Algo','Algorithms and Data structures',4,4,1000,1,'INFO6015'),(4,'Web development','Web Tools and Methodologies',4,4,1000,1,'INFO6250'),(5,'Case studies of business and their policies','Business policies',4,4,1000,4,'TSM6350'),(6,'IT Auditing - Auditing techniques in IT involving IP, security and etc.','IT Auditing',4,4,900,1,'INFO6001'),(7,'Concepts of Parallel Programming and multitasking, involving deadlocks','Multithreaded Programming',4,2,1300,2,'CSYE1200'),(8,'Computer Networking - advanced and basics','Fundamentals of Networking',4,4,1300,3,'CS6390'),(9,'Career Managmnt for Engineers','Career Managmnt for Engineers',1,4,900,1,'ENCP6000'),(10,'Fluid Dynamics and problem solving of existing fluid problems','Fluid Dynamics',4,4,1200,7,'ME1200'),(11,'Database design and modelling. Writing SQL and PL/SQL scripts and creating complex joins, views, etc.','Database Design and Management',4,4,1000,1,'INFO6000');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courseMaterials`
--

DROP TABLE IF EXISTS `courseMaterials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseMaterials` (
  `idcourseMaterials` int(11) NOT NULL AUTO_INCREMENT,
  `uploadedPath` varchar(100) NOT NULL,
  `course_offering_courseRegnNum` int(11) NOT NULL,
  PRIMARY KEY (`idcourseMaterials`),
  KEY `fk_courseMaterials_course_offering1_idx` (`course_offering_courseRegnNum`),
  CONSTRAINT `fk_courseMaterials_course_offering1` FOREIGN KEY (`course_offering_courseRegnNum`) REFERENCES `course_offering` (`courseRegnNum`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courseMaterials`
--

LOCK TABLES `courseMaterials` WRITE;
/*!40000 ALTER TABLE `courseMaterials` DISABLE KEYS */;
INSERT INTO `courseMaterials` VALUES (2,'Sample-Ch07.pdf',1350),(3,'DZ_CHK_WebApplicationDevelopment.pdf',1350),(4,'Academic Advisor Approval Form v31429688265995.pdf',1350),(5,'SpringSecurity1429818623867.7z',1350);
/*!40000 ALTER TABLE `courseMaterials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_has_book`
--

DROP TABLE IF EXISTS `course_has_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_has_book` (
  `course_courseId` int(11) NOT NULL,
  `book_isbn` int(11) NOT NULL,
  PRIMARY KEY (`course_courseId`,`book_isbn`),
  KEY `fk_course_has_book_book1_idx` (`book_isbn`),
  KEY `fk_course_has_book_course1_idx` (`course_courseId`),
  CONSTRAINT `fk_course_has_book_book1` FOREIGN KEY (`book_isbn`) REFERENCES `book` (`isbn`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_has_book_course1` FOREIGN KEY (`course_courseId`) REFERENCES `course` (`courseId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_has_book`
--

LOCK TABLES `course_has_book` WRITE;
/*!40000 ALTER TABLE `course_has_book` DISABLE KEYS */;
INSERT INTO `course_has_book` VALUES (11,1),(3,2),(10,6),(10,7);
/*!40000 ALTER TABLE `course_has_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_offering`
--

DROP TABLE IF EXISTS `course_offering`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_offering` (
  `courseRegnNum` int(11) NOT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `course_courseId` int(11) NOT NULL,
  `secNo` int(11) DEFAULT NULL,
  `location_locationId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  PRIMARY KEY (`courseRegnNum`),
  KEY `fk_offering_course1_idx` (`course_courseId`),
  KEY `fk_course_offering_location1_idx` (`location_locationId`),
  KEY `fk_course_offering_employee1_idx` (`employeeId`),
  CONSTRAINT `fk_course_offering_employee1` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`person_personId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_offering_location1` FOREIGN KEY (`location_locationId`) REFERENCES `location` (`locationId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_offering_course1` FOREIGN KEY (`course_courseId`) REFERENCES `course` (`courseId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_offering`
--

LOCK TABLES `course_offering` WRITE;
/*!40000 ALTER TABLE `course_offering` DISABLE KEYS */;
INSERT INTO `course_offering` VALUES (1350,'2015-01-12','2015-04-27',35,11,1,3,4),(6250,'2015-01-12','2015-04-27',50,3,1,2,4),(31767,'2015-01-12','2015-04-27',90,4,1,1,3);
/*!40000 ALTER TABLE `course_offering` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_prerequisite`
--

DROP TABLE IF EXISTS `course_prerequisite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_prerequisite` (
  `coursePreId` int(11) NOT NULL,
  `course_prerequisite_Id` int(11) NOT NULL,
  PRIMARY KEY (`coursePreId`),
  CONSTRAINT `fk_course_prerequisite_course1` FOREIGN KEY (`coursePreId`) REFERENCES `course` (`courseId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_prerequisite`
--

LOCK TABLES `course_prerequisite` WRITE;
/*!40000 ALTER TABLE `course_prerequisite` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_prerequisite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `type` varchar(45) NOT NULL,
  `person_personId` int(11) NOT NULL,
  `location_locationId` int(11) NOT NULL,
  PRIMARY KEY (`person_personId`),
  KEY `fk_employee_location1_idx` (`location_locationId`),
  CONSTRAINT `fk_employee_location1` FOREIGN KEY (`location_locationId`) REFERENCES `location` (`locationId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_employee_person1` FOREIGN KEY (`person_personId`) REFERENCES `person` (`personId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES ('FULL-TIME',3,1),('CONTRACT',4,2);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `idevents` int(11) NOT NULL AUTO_INCREMENT,
  `eventTitle` varchar(100) NOT NULL,
  `eventDesc` varchar(500) NOT NULL,
  `organizer` varchar(100) DEFAULT NULL,
  `startDate` datetime NOT NULL,
  `endDate` datetime NOT NULL,
  `eventType` varchar(45) DEFAULT NULL,
  `eventPath` varchar(100) DEFAULT NULL,
  `location_locationId` int(11) NOT NULL,
  PRIMARY KEY (`idevents`),
  KEY `fk_events_location1_idx` (`location_locationId`),
  CONSTRAINT `fk_events_location1` FOREIGN KEY (`location_locationId`) REFERENCES `location` (`locationId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (3,'Confronting Guantánamo','Northeastern University is pleased to host the Guantánamo Public Memory Project Exhibit. Confronting Guantánamo locates Guantánamo Bay, Cuba, in time and space.  It also examines the nature of place, the reality of the refugee experience, the ethics of public health, the implications of indefinite incarceration, and the scope of human justice.','College of Social Sciences and Humanities','2015-04-19 00:00:00','2015-04-20 00:00:00','Exhibition',NULL,7),(4,'Create and Critique','Bahareh & Farzaneh Safarani, Iranian twin artists, studying MFA In Studio Art at Northeastern will conduct a series of weekend workshops. Saturday Workshops are about creaation','Safarani sisters','2015-04-18 12:00:00','2015-04-18 16:00:00','Workshop',NULL,6);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `invoiceId` int(11) NOT NULL AUTO_INCREMENT,
  `dueDate` date DEFAULT NULL,
  `totalAmt` float DEFAULT NULL,
  `paidAmt` float DEFAULT NULL,
  `balanceAmt` float DEFAULT NULL,
  `generatedDate` date DEFAULT NULL,
  `student_studentId` int(11) NOT NULL,
  PRIMARY KEY (`invoiceId`),
  KEY `fk_invoice_student1_idx` (`student_studentId`),
  CONSTRAINT `fk_invoice_student1` FOREIGN KEY (`student_studentId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_item`
--

DROP TABLE IF EXISTS `invoice_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_item` (
  `dueAmt` float NOT NULL,
  `invoice_item_id` varchar(45) NOT NULL,
  `invoice_invoiceId` int(11) NOT NULL,
  `invoiceType` varchar(45) NOT NULL,
  PRIMARY KEY (`invoice_item_id`),
  KEY `fk_invoice_item_invoice1_idx` (`invoice_invoiceId`),
  CONSTRAINT `fk_invoice_item_invoice1` FOREIGN KEY (`invoice_invoiceId`) REFERENCES `invoice` (`invoiceId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_item`
--

LOCK TABLES `invoice_item` WRITE;
/*!40000 ALTER TABLE `invoice_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `jobId` int(11) NOT NULL AUTO_INCREMENT,
  `jobTitle` varchar(100) DEFAULT NULL,
  `jobDesc` varchar(2000) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `carRequired` varchar(3) DEFAULT NULL,
  `jobLocation` varchar(100) DEFAULT NULL,
  `minGpa` float DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `minWage` float DEFAULT NULL,
  `maxWage` float DEFAULT NULL,
  `jobType` varchar(45) DEFAULT NULL,
  `company_companyId` int(11) NOT NULL,
  PRIMARY KEY (`jobId`),
  KEY `fk_job_company1_idx` (`company_companyId`),
  CONSTRAINT `fk_job_company1` FOREIGN KEY (`company_companyId`) REFERENCES `company` (`companyId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
INSERT INTO `job` VALUES (1,'Software Development Engineer','In this role, a commitment to team work, hustle, and strong communication skills (to both business and technical partners) are absolute requirements. Creating a reliable, scalable, and high performance service requires exceptional technical expertise and a sound understanding of the fundamentals of Computer Science and large-scale distributed systems. ','2015-04-19 00:00:00','Yes','US, WA, Seattle',3.5,4,35,40,'Internship',4),(2,'Software Development Engineer - Test','Master’s degree with 3+ years of experience in Computer Science, Computer or Electrical Engineering, Software, Technology, Operations, or a related field\n5+ years of experience in the job offered or related occupation-must involve supporting distributed software systems\nStrong development (SDE or SDE/T) background on large projects on collaborative teams in an OO language\nYou show good judgment and instincts in decision-making opportunities\nStrong written and verbal communication (can I move this to basic qualifications?)','2015-04-19 00:00:00','Yes','US, WA, Seattle',3.5,4,35,40,'Internship',4),(3,'Software Engineer','PayPal is one of eBay, Inc.\'s three revenue-generating business units, in addition to eBay Marketplaces and Skype. PayPal’s mission is to build the web\'s most convenient, secure, and cost-effective payment solution. With more than 130 million accounts, presence in 103 countries and 17 currencies, and payment volume in excess of $40 B per year, PayPal is the clear leader in global online payments.','2015-04-19 00:00:00','No','San Jose',3.2,6,30,38,'Co-op',3),(4,'Fluid Dynamics Engineer','Hubway is your bike sharing system providing more than 1,300 bikes at 140 stations throughout Boston, Brookline, Cambridge, and Somerville.','2015-04-19 00:00:00','No','Boston',3,4,20,27,'Co-op',9);
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobProfile`
--

DROP TABLE IF EXISTS `jobProfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobProfile` (
  `jobProfileId` int(11) NOT NULL AUTO_INCREMENT,
  `resume` varchar(100) DEFAULT NULL,
  `coverLetter` varchar(100) DEFAULT NULL,
  `student_studentId` int(11) NOT NULL,
  PRIMARY KEY (`jobProfileId`),
  KEY `fk_jobAccount_student1_idx` (`student_studentId`),
  CONSTRAINT `fk_jobAccount_student1` FOREIGN KEY (`student_studentId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobProfile`
--

LOCK TABLES `jobProfile` WRITE;
/*!40000 ALTER TABLE `jobProfile` DISABLE KEYS */;
INSERT INTO `jobProfile` VALUES (1,'1_Abhishek_AV_Resume_132.pdf',NULL,1);
/*!40000 ALTER TABLE `jobProfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `locationId` int(11) NOT NULL AUTO_INCREMENT,
  `buildingName` varchar(45) DEFAULT NULL,
  `roomNo` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`locationId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Shillman',0123),(2,'Snell',0131),(3,'Snell',0321),(4,'Snell Library',0012),(5,'West Village F',0001),(6,'Blackman Auditorium',NULL),(7,'Fenway Center',NULL);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `personId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `dateOfBirth` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `emailId` varchar(45) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `address1` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `state` varchar(45) NOT NULL,
  `country` varchar(45) NOT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `address2` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`personId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'Abhishek','AV','1990/01/14','M','abhishekav9@gmail.com','1234567890','75 Saint Alphonsus St.','Boston','MA','USA','02120','Apt 1115'),(2,'Yash','Bindal','1992/08/08','M','ybindal@outlook.com','1235437890','123 Peterborough','Boston','MA','USA','02113',NULL),(3,'Zlatan','Ibrahimovic','1982/12/12','M','dareToZlatan@gmail.com','9871237654','321 Horadon','New York City','NY','USA','01341',NULL),(4,'Alexis','Sanchez','1985/12/12','M','arsenal@arsenal.com','2347651890','Emirates Stadium','London','LO','UK','123456',NULL),(5,'Solo','Hope','1989/11/30','F','hopeSolo@gmail.com','2347651891','32 Texas High','Dallas','TX','USA','12354',NULL);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `program`
--

DROP TABLE IF EXISTS `program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `program` (
  `programId` int(11) NOT NULL AUTO_INCREMENT,
  `programName` varchar(45) NOT NULL,
  `programCode` varchar(45) NOT NULL,
  `totalSemesters` int(11) NOT NULL,
  `schoolName` varchar(45) NOT NULL,
  PRIMARY KEY (`programId`),
  UNIQUE KEY `programCode_UNIQUE` (`programCode`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `program`
--

LOCK TABLES `program` WRITE;
/*!40000 ALTER TABLE `program` DISABLE KEYS */;
INSERT INTO `program` VALUES (1,'Information Systems','INFO',4,'COE'),(2,'Computer Systems Engineering','CSYE',4,'COE'),(3,'Computer Science','CS',4,'CCIS'),(4,'Telecommunication Systems Mangement','TSM',4,'COE'),(5,'Engineering Management','EM',3,'COE'),(6,'Project Management','PM',3,'CPS'),(7,'Mechanical Engineering','ME',4,'COE');
/*!40000 ALTER TABLE `program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `roleId` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(45) NOT NULL,
  PRIMARY KEY (`roleId`),
  UNIQUE KEY `roleName_UNIQUE` (`roleName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (4,'Career Staff'),(2,'Professor'),(3,'Registrar'),(1,'Student');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `studentId` int(11) NOT NULL,
  `status` varchar(45) NOT NULL,
  `studentNum` varchar(45) NOT NULL,
  `localAddress` varchar(45) DEFAULT NULL,
  `localCity` varchar(45) DEFAULT NULL,
  `localState` varchar(45) DEFAULT NULL,
  `localZipCode` varchar(45) DEFAULT NULL,
  `international` varchar(3) NOT NULL,
  `program_programId` int(11) DEFAULT NULL,
  `study` varchar(45) NOT NULL,
  `ssn` char(9) DEFAULT NULL,
  `studentImage` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`studentId`),
  UNIQUE KEY `studentNum_UNIQUE` (`studentNum`),
  UNIQUE KEY `ssn_UNIQUE` (`ssn`),
  KEY `fk_student_program1_idx` (`program_programId`),
  CONSTRAINT `fk_student_person1` FOREIGN KEY (`studentId`) REFERENCES `person` (`personId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_program1` FOREIGN KEY (`program_programId`) REFERENCES `program` (`programId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'ACTIVE','NU123456789',NULL,NULL,NULL,NULL,'Y',NULL,'GR','123456789',NULL),(2,'ACTIVE','NU987123765',NULL,NULL,NULL,NULL,'Y',NULL,'GR','312098567',NULL);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_course_registered`
--

DROP TABLE IF EXISTS `student_course_registered`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_course_registered` (
  `student_studentId` int(11) NOT NULL,
  `offering_courseRegnNum` int(11) NOT NULL,
  `grade` float DEFAULT NULL,
  `semester` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `studentRegId` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`studentRegId`),
  KEY `fk_student_has_offering_offering1_idx` (`offering_courseRegnNum`),
  KEY `fk_student_has_offering_student1_idx` (`student_studentId`),
  CONSTRAINT `fk_student_has_offering_offering1` FOREIGN KEY (`offering_courseRegnNum`) REFERENCES `course_offering` (`courseRegnNum`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_offering_student1` FOREIGN KEY (`student_studentId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_course_registered`
--

LOCK TABLES `student_course_registered` WRITE;
/*!40000 ALTER TABLE `student_course_registered` DISABLE KEYS */;
INSERT INTO `student_course_registered` VALUES (1,1350,NULL,1,2014,1),(1,6250,NULL,1,2014,2),(1,31767,NULL,1,2014,3);
/*!40000 ALTER TABLE `student_course_registered` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_has_assignment`
--

DROP TABLE IF EXISTS `student_has_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_has_assignment` (
  `student_studentId` int(11) NOT NULL,
  `assignment_idassignment` int(11) NOT NULL,
  `submissionId` int(11) NOT NULL AUTO_INCREMENT,
  `marksObtained` float DEFAULT NULL,
  `uploadPath` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`submissionId`),
  KEY `fk_student_has_assignment_assignment1_idx` (`assignment_idassignment`),
  KEY `fk_student_has_assignment_student1_idx` (`student_studentId`),
  CONSTRAINT `fk_student_has_assignment_assignment1` FOREIGN KEY (`assignment_idassignment`) REFERENCES `assignment` (`idassignment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_assignment_student1` FOREIGN KEY (`student_studentId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_has_assignment`
--

LOCK TABLES `student_has_assignment` WRITE;
/*!40000 ALTER TABLE `student_has_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_has_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_has_book`
--

DROP TABLE IF EXISTS `student_has_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_has_book` (
  `student_studentId` int(11) NOT NULL,
  `book_isbn` int(11) NOT NULL,
  `returnDate` date DEFAULT NULL,
  `transactionId` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`transactionId`),
  KEY `fk_student_has_book_book1_idx` (`book_isbn`),
  KEY `fk_student_has_book_student1_idx` (`student_studentId`),
  CONSTRAINT `fk_student_has_book_book1` FOREIGN KEY (`book_isbn`) REFERENCES `book` (`isbn`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_book_student1` FOREIGN KEY (`student_studentId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_has_book`
--

LOCK TABLES `student_has_book` WRITE;
/*!40000 ALTER TABLE `student_has_book` DISABLE KEYS */;
INSERT INTO `student_has_book` VALUES (1,1,'2015-05-19',1);
/*!40000 ALTER TABLE `student_has_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_job_application`
--

DROP TABLE IF EXISTS `student_job_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_job_application` (
  `student_studentId` int(11) NOT NULL,
  `job_jobId` int(11) NOT NULL,
  `jobProfileId` int(11) NOT NULL,
  `appliedDate` datetime NOT NULL,
  `applicationStatus` varchar(45) NOT NULL,
  `applicationId` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`applicationId`),
  KEY `fk_student_has_job_job1_idx` (`job_jobId`),
  KEY `fk_student_has_job_student1_idx` (`student_studentId`),
  KEY `fk_student_has_job_jobAccount1_idx` (`jobProfileId`),
  CONSTRAINT `fk_student_has_job_job1` FOREIGN KEY (`job_jobId`) REFERENCES `job` (`jobId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_job_jobAccount1` FOREIGN KEY (`jobProfileId`) REFERENCES `jobProfile` (`jobProfileId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_job_student1` FOREIGN KEY (`student_studentId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_job_application`
--

LOCK TABLES `student_job_application` WRITE;
/*!40000 ALTER TABLE `student_job_application` DISABLE KEYS */;
INSERT INTO `student_job_application` VALUES (1,1,1,'2015-04-19 00:00:00','Submitted',1),(1,2,1,'2015-04-19 00:00:00','Processing',2);
/*!40000 ALTER TABLE `student_job_application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userAccounts`
--

DROP TABLE IF EXISTS `userAccounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userAccounts` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(45) NOT NULL,
  `userPassword` varchar(45) NOT NULL,
  `role_roleId` int(11) NOT NULL,
  `hintQuestion` varchar(45) DEFAULT NULL,
  `hintAnswer` varchar(45) DEFAULT NULL,
  `person_personId` int(11) NOT NULL,
  `userType` varchar(45) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userName_UNIQUE` (`userName`),
  KEY `fk_userAccounts_role1_idx` (`role_roleId`),
  KEY `fk_userAccounts_person1_idx` (`person_personId`),
  CONSTRAINT `fk_userAccounts_person1` FOREIGN KEY (`person_personId`) REFERENCES `person` (`personId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_userAccounts_role1` FOREIGN KEY (`role_roleId`) REFERENCES `role` (`roleId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userAccounts`
--

LOCK TABLES `userAccounts` WRITE;
/*!40000 ALTER TABLE `userAccounts` DISABLE KEYS */;
INSERT INTO `userAccounts` VALUES (1,'abhi','abhish',1,NULL,NULL,1,'Student'),(2,'yash','yash',1,NULL,NULL,2,'Student'),(3,'zim','zim',2,NULL,NULL,3,'Employee'),(4,'alex','alex',2,NULL,NULL,4,'Employee'),(5,'solo','solo',3,NULL,NULL,5,'Employee');
/*!40000 ALTER TABLE `userAccounts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-24 16:34:27
