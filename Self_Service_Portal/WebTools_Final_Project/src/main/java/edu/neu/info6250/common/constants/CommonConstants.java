package edu.neu.info6250.common.constants;

public interface CommonConstants {

	public static String ACTIVE = "ACTIVE";
	public static String GENERAL = "System Announcement";
	public static String INACTIVE = "INACTIVE";
	public static String UNDERGRAD = "Undergraduate";
	public static String GRAD = "Graduate";
	public static String YES = "YES";
	public static String NO = "NO";
	public static String INTERNATIONAL = "INTERNATIONAL";
	public static String CITIZEN = "CITIZEN";
	public static String COOP = "Co-op";
	public static String FULLTIME = "Full-time";
	public static String CONTRACT = "Contract";
	public static String STUDENT = "Student";
	public static String INSTRUCTOR = "INSTRUCTOR";
	public static String ADMIN = "ADMIN";
	public static String FINANCE = "FINANCE";
	public static String EMPLOYEE = "Employee";

	public static String SESSION_USERNAME = "userName";
	public static String SESSION_USERROLE = "userRole";
	public static String SESSION_USERID = "userId";
	public static String SESSION_USERFULLNAME = "fullName";
	public static String SESSION_USERTYPE = "userType";
	public static String SESSION_USERPROFILE = "userProfile";
	public static String SESSION_USERACCOUNTS = "userAccounts";
	public static String SESSION_PERSONPROFILE = "personProfile";

	public static String COOKIE_USERNAME = "userNameCookie";
	public static String COOKIE_USERID = "userIdCookie";
	public static String COOKIE_USERROLE = "userRoleCookie";
	public static String COOKIE_USERTYPE = "userTypeCookie";
	public static String REMEMBER = "remember";
}
