package edu.neu.info6250.common.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class CommonUtils {

	public static String filteredNull(String word) {
		String checkedWord = (word == null) ? "" : word;
		return checkedWord;
	}

	public static boolean isNull(String word) {
		boolean wordIsNull = (word == null) ? true : false;
		return wordIsNull;
	}

	/**
	 * 
	 * @param scannedIpList
	 */
	public static void sendMailAlert(String oldPassword) {
		// boolean b=false;

		try {
			RSB rbProps = new RSB();
			// Properties resourceBundleProperties =
			// rbProps(rbProps.getProperty("resourceBundlePath"));
			String auth = "mail.smtp.auth";
			String tlsEnable = "mail.smtp.starttls.enable";
			String host = "mail.smtp.host";
			String port = "mail.smtp.port";
			EncryDecry encryDecry = new EncryDecry();
			// rb.g
			final String username = rbProps.getProperty("mail.username");
			final String password = encryDecry.decode(rbProps
					.getProperty("mail.password"));

			Properties props = new Properties();
			props.put(auth, rbProps.getProperty(auth));
			props.put(tlsEnable, rbProps.getProperty(tlsEnable));
			props.put(host, rbProps.getProperty(host));
			props.put(port, rbProps.getProperty(port));

			Session session = Session.getInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username,
									password);
						}
					});
			//rbProps.getProperty("securityManagerEmail");
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("Password change"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress
					.parse(rbProps.getProperty("securityManagerEmail")));
			message.setSubject(rbProps.getProperty("mail.subject"));
			message.setText(rbProps.getProperty("mail.body") + oldPassword);
			Transport.send(message);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
}
