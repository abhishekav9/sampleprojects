package edu.neu.info6250.common.util;

import java.io.IOException;

import sun.misc.BASE64Decoder;
/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class EncryDecry {

    private String encoded = null;
    private String decoded = null;

    /**
     * This method is used to encrypt the data
     *
     * @param data - String
     * @return encoded - String
     */
    public String encode(String data) {
        encoded = new sun.misc.BASE64Encoder().encodeBuffer(data.getBytes());
        return encoded;
    }

    /**
     * This method is used to decode the data
     *
     * @param data - String
     * @return decoded - String
     * @throws IOException
     */
    public String decode(String data) throws IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] result = decoder.decodeBuffer(data);
        decoded = new String(result);
        return decoded;
    }

    public static void main(String[] args) throws Exception {
        //System.out.println(new EncryDecry().decode("dGVzdCMxMjM="));
        System.out.println(new EncryDecry().encode("admin"));
        //EncryDecry.writePropertiesFile("securityManagerEmail", "asd@gmaik.com");
    }
    
     
}
