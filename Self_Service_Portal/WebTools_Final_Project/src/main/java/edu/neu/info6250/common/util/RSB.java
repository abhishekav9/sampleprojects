package edu.neu.info6250.common.util;

import java.util.ResourceBundle;

public class RSB {
	private ResourceBundle rb;

	public RSB() {
		rb = ResourceBundle.getBundle("ResourceBundle");
	}

	public String getProperty(String s) {
		return CommonUtils.filteredNull(rb.getString(s));
	}

}