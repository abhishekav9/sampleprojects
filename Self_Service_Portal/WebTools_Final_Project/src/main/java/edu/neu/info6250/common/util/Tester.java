package edu.neu.info6250.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Tester {

	public static void main(String[] args) {

		try {

			URL url = new URL("http://services.groupkt.com/country/get/all");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			JSONTokener jsonTokener = new JSONTokener(new InputStreamReader(
					(conn.getInputStream())));
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			try {
				JSONObject jsonObject = new JSONObject(jsonTokener);
				JSONObject jsonObject2 = (JSONObject) jsonObject
						.get("RestResponse");
				JSONArray jsonArray = new JSONArray(jsonObject2.get("result")
						.toString());
				for (int i = 0; i < jsonArray.length(); i++) {
					System.out.println(jsonArray.getJSONObject(i));
					JSONObject jsonObject3 = jsonArray.getJSONObject(i);
					System.out.println(jsonObject3.get("name"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String output;
			JSONObject jsonObject;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				// jsonObject = new JSONObject(output);
				// System.out.println(jsonObject.toString());
				System.out.println(output);
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}

}
