package edu.neu.info6250.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.neu.info6250.common.constants.CommonConstants;
import edu.neu.info6250.dao.UserDao;
import edu.neu.info6250.model.UserAccounts;
import edu.neu.info6250.validator.PasswordChange;

@Controller
public class AccountController {

	@Autowired
	@Qualifier("passValidator")
	private Validator validator;

	@Autowired
	private UserDao userDao;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
	public String initUserLoginForm(Model model) {
		PasswordChange passwordChange = new PasswordChange();
		model.addAttribute("passwordChange", passwordChange);
		return "passwordChange";
	}

	/**
	 * 
	 * @param request
	 * @param model
	 * @param passwordChange
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public String submitForm(HttpServletRequest request, Model model,
			@Validated PasswordChange passwordChange, BindingResult result) {

		model.addAttribute("passwordChange", passwordChange);
		String returnVal = "passwordChange";
		if (result.hasErrors()) {
			// request.getSession().invalidate();
			model.addAttribute("error", "Enter proper values");
			return returnVal;
		} else {
			if (passwordChange.getNewPass().equals(
					passwordChange.getNewPassAgain())) {
				try {
					HttpSession session = request.getSession();
					String user = (String) session
							.getAttribute(CommonConstants.SESSION_USERNAME);
					if (user != null) {
						UserAccounts userAccounts = userDao
								.queryUserAccountByNameAndPassword(user,
										passwordChange.getOldPass());
						if (userAccounts != null) {
							userDao.setNewPassword(userAccounts,
									passwordChange.getNewPass());
							returnVal = "success";
							return returnVal;
						} else {
							model.addAttribute("error",
									"Wrong existing password entered");
							result.rejectValue("oldPass", "error.oldPass",
									"Wrong existing password entered.");
							return returnVal;
						}
					} else {
						model.addAttribute("error",
								"Session timed out. Login again");
						result.rejectValue("oldPass", "error.oldPass",
								"Session timed out. Login again");

						return returnVal;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return returnVal;
				}
			} else {
				model.addAttribute("error", "Retype the new password again");
				result.rejectValue("newPassAgain", "error.newPassAgain",
						"Retype the new password again");
				return returnVal;
			}
		}
	}
}
