package edu.neu.info6250.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.neu.info6250.common.constants.CommonConstants;
import edu.neu.info6250.dao.BookDao;

@Controller
public class BookController {

	@Autowired
	private BookDao libraryDao;

	/**
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/sendMessage", method = RequestMethod.GET)
	public String sendMessage(HttpServletRequest request, Model model) {

		String sendTo = request.getParameter("sendTo");
		model.addAttribute("sendTo", sendTo);
		String returnVal = "sendReply";

		return returnVal;
	}

	/**
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/books", method = RequestMethod.GET)
	public String queryMyBooks(HttpServletRequest request, Model model) {

		String returnVal = "sendReply";
		try {
			HttpSession httpSession = request.getSession();
			String userId = (String) httpSession
					.getAttribute(CommonConstants.SESSION_USERID);
			libraryDao.queryBooksCheckedOut(Integer.parseInt(userId));
			String sendTo = request.getParameter("sendTo");
			model.addAttribute("sendTo", sendTo);

			return returnVal;
		} catch (Exception e) {

		}
		return returnVal;
	}
}
