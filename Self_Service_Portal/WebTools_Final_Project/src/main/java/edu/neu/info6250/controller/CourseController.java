package edu.neu.info6250.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import edu.neu.info6250.common.constants.CommonConstants;
import edu.neu.info6250.common.util.RSB;
import edu.neu.info6250.dao.CourseDao;
import edu.neu.info6250.dao.UserDao;
import edu.neu.info6250.model.Course;
import edu.neu.info6250.model.CourseMaterials;
import edu.neu.info6250.model.CourseOffering;
import edu.neu.info6250.model.Employee;
import edu.neu.info6250.model.Person;
import edu.neu.info6250.model.Student;

@Controller
public class CourseController {

	private RSB bundleProperty = new RSB();

	@Autowired
	private CourseDao courseDao;

	@Autowired
	private UserDao userDao;

	/**
	 * 
	 * @param request
	 * @param response
	 * @param materialId
	 * @throws IOException
	 * @throws ServletException
	 */
	@RequestMapping(value = "/downloadCourseMaterial/{materialId}/{courseId}", method = RequestMethod.GET)
	public @ResponseBody void downloadFiles(HttpServletRequest request,
			HttpServletResponse response, @PathVariable String materialId,
			@PathVariable String courseId) throws IOException, ServletException {

		ServletContext context = request.getServletContext();

		File downloadFile = null;
		OutputStream outStream = null;
		String path = null;
		try {
			// CourseOffering courseOffering = courseDao
			// .getCourseOfferingFromId(Integer.parseInt(courseId));
			path = courseDao.getCourseMaterialFromId(Integer
					.parseInt(materialId));
			downloadFile = new File(
					bundleProperty.getProperty("fileServer.courseMaterialPath")
							+ path);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			request.setAttribute("error", "Error in fetching file");
			request.getRequestDispatcher("error").forward(request, response);

		}
		try (InputStream inputStream = new FileInputStream(downloadFile)) {

			response.setContentLength((int) downloadFile.length());
			response.setContentType(context.getMimeType(bundleProperty
					.getProperty("fileServer.courseMaterialPath") + path));

			// response header
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"",
					downloadFile.getName());
			response.setHeader(headerKey, headerValue);

			outStream = response.getOutputStream();
			IOUtils.copy(inputStream, outStream);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("error", "Error in fetching file");
			request.getRequestDispatcher("error").forward(request, response);
		} finally {
			if (outStream != null) {
				outStream.close();
			}
		}
	}

	/**
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/courses", method = RequestMethod.GET)
	public String getMyCourses(HttpServletRequest request, Model model) {

		try {
			HttpSession session = request.getSession();
			Person person = (Person) session
					.getAttribute(CommonConstants.SESSION_USERPROFILE);
			if (person instanceof Student) {
				Student student = (Student) person;
				List<CourseOffering> registeredCourseList = courseDao
						.getRegisteredCourses(student.getPersonId());
				// courseDao.getEmployeeFromCourseRegnNum(registeredCourseList);
				model.addAttribute("eventList", registeredCourseList);
				return "course_home";
			} else if (person instanceof Employee) {
				Employee employee = (Employee) person;
				List<CourseOffering> registeredCourseList = courseDao
						.getCourseOfferingFromEmployee(employee);
				List<Course> courseList = new LinkedList<>();
				for (CourseOffering courseOffering : registeredCourseList) {
					courseList.add(courseDao
							.getCoursesFromCourseOfferingId(courseOffering
									.getCourseRegnNum()));
				}
				model.addAttribute("courseList", courseList);
				model.addAttribute("eventList", registeredCourseList);
				return "course_home_emp";
			}
		} catch (Exception e) {
			return "error";
		}
		return "error";
	}

	/**
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/viewCourseMaterial", method = RequestMethod.GET)
	public String viewCourseMaterial(HttpServletRequest request, Model model) {

		String returnVal = "course_material_list";
		try {
			String sendTo = request.getParameter("sendTo");
			model.addAttribute("sendTo", sendTo);
			List<CourseMaterials> courseMaterialList = courseDao
					.getCourseMaterials(Integer.parseInt(sendTo));
			Course course = courseDao.getCoursesFromCourseOfferingId((Integer
					.parseInt(sendTo)));
			model.addAttribute("eventList", courseMaterialList);
			model.addAttribute("course", course);
			return returnVal;
		} catch (Exception e) {
			returnVal = "error";
			return returnVal;
		}
	}

	// uploadController

	/**
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/uploadController", method = RequestMethod.GET)
	public String sendToUploadPage(HttpServletRequest request, Model model) {

		String returnVal = "uploadPage";
		try {
			String sendTo = request.getParameter("sendTo");
			model.addAttribute("sendTo", sendTo);
			List<CourseMaterials> courseMaterialList = courseDao
					.getCourseMaterials(Integer.parseInt(sendTo));
			Course course = courseDao.getCoursesFromCourseOfferingId((Integer
					.parseInt(sendTo)));
			model.addAttribute("eventList", courseMaterialList);
			model.addAttribute("course", course);
			return returnVal;
		} catch (Exception e) {
			returnVal = "error";
			return returnVal;
		}
	}

	@RequestMapping(value = "/courseMaterialUpload", method = RequestMethod.GET)
	public String getUploadForm(@RequestParam("file") MultipartFile[] file,
			Model map, HttpServletRequest request) {
		return "error";
	}

	/**
	 * 
	 * @param file
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/courseMaterialUpload", method = RequestMethod.POST)
	public String uploadForm(@RequestParam("file") MultipartFile[] file,
			Model map, HttpServletRequest request) {

		List<String> fileNames = new ArrayList<String>();
		String cOfferId = request.getParameter("sendTo");
		String returnval = "error";
		int i = 0;
		if (null != file && file.length > 0) {
			for (MultipartFile multipartFile : file) {

				String fileName = null;
				String fileNameWithOutExt = FilenameUtils
						.removeExtension(multipartFile.getOriginalFilename());
				String ext = FilenameUtils.getExtension(multipartFile
						.getOriginalFilename());
				if (!"".equalsIgnoreCase(fileNameWithOutExt)) {
					fileName = fileNameWithOutExt + System.currentTimeMillis();
					fileName = fileName + "." + ext;
					i = +1;
					try {
						multipartFile.transferTo(new File(bundleProperty
								.getProperty("fileServer.courseMaterialPath")
								+ fileName));
						// /////
						CourseMaterials courseMaterials = new CourseMaterials();
						courseMaterials.setUploadedPath(fileName);
						courseMaterials.setCourseOffering(courseDao
								.getCourseOfferingFromId(Integer
										.parseInt(cOfferId)));
						courseDao.insertCourseMaterial(courseMaterials);
						// /////
						returnval = "success";
					} catch (Exception e) {
						System.out.println(e.getMessage());
						returnval = "error";
					}
					fileNames.add(fileName);
				}
			}
		}

		map.addAttribute("files", fileNames);
		map.addAttribute("count", i);
		return returnval;
	}

}
