package edu.neu.info6250.controller;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.neu.info6250.common.constants.CommonConstants;
import edu.neu.info6250.dao.CourseDao;
import edu.neu.info6250.dao.HomeDao;
import edu.neu.info6250.dao.UserDao;
import edu.neu.info6250.model.Announcement;
import edu.neu.info6250.model.CourseOffering;
import edu.neu.info6250.model.Employee;
import edu.neu.info6250.model.Events;
import edu.neu.info6250.model.Person;
import edu.neu.info6250.model.Student;
import edu.neu.info6250.model.UserAccounts;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	@Autowired
	private UserDao userDao;

	@Autowired
	private HomeDao homeDao;

	@Autowired
	private CourseDao courseDao;

	/**
	 * login login mapping
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String initUserLoginForm(HttpServletRequest request, Model model,
			HttpServletResponse response) {

		String returnVal = "login";
		try {
			HttpSession httpSession = request.getSession(false);
			if (httpSession != null) {
				String userId = (String) httpSession
						.getAttribute(CommonConstants.SESSION_USERID);
				int userIdInt = Integer.parseInt(userId);
				String userRole = (String) httpSession
						.getAttribute(CommonConstants.SESSION_USERROLE);
				String userType = (String) httpSession
						.getAttribute(CommonConstants.SESSION_USERTYPE);

				if (userRole != null && !userRole.isEmpty()
						&& userId.toString() != null
						&& !userId.toString().isEmpty() && userType != null
						&& !userType.isEmpty()) {
					Person user = null;
					UserAccounts userAccounts = null;
					userAccounts = userDao.queryUserAccountById(userIdInt);

					user = userDao
							.queryUserProfileByUserId(userIdInt, userType);
					// ////////////////////
					// set cookie and session
					setCookieAndSession(request, user, response, userAccounts,
							userAccounts.getUserType());
					//
					String roleNme = userAccounts.getRole().getRoleName();

					List<Events> eventList = homeDao.fetchEvents();
					model.addAttribute("eventList", eventList);
					switch (roleNme) {
					case "Professor":
						List<CourseOffering> courseOfferingList1 = courseDao
								.getRegisteredCoursesForProfessor(userAccounts
										.getUserId());
						List<Announcement> announcementList = userDao
								.queryAnnouncement(user, "Professor",
										courseOfferingList1);
						model.addAttribute("announcementList", announcementList);
						returnVal = "instructor_home";
						break;
					case "Student":
						List<CourseOffering> courseOfferingList = courseDao
								.getRegisteredCourses(userAccounts.getUserId());
						List<Announcement> announcementListStudent = userDao
								.queryAnnouncement(user, "Student",
										courseOfferingList);
						model.addAttribute("announcementList",
								announcementListStudent);
						returnVal = "student_home";
						break;
					case "Career Staff":
						returnVal = "career_staff_home";
						break;
					default:
						returnVal = "login";
						break;
					}

					model.addAttribute(CommonConstants.SESSION_USERACCOUNTS,
							userAccounts);
					model.addAttribute(CommonConstants.SESSION_USERPROFILE,
							user);
				} else {
					UserAccounts loginForm = new UserAccounts();
					model.addAttribute("user", loginForm);
					return "login";
				}
				return returnVal;
			} else {
				UserAccounts loginForm = new UserAccounts();
				model.addAttribute("user", loginForm);
				return "login";
			}
		} catch (Exception e) {
			UserAccounts loginForm = new UserAccounts();
			model.addAttribute("user", loginForm);
			return "login";
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param user
	 * @param result
	 * @return
	 */

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String submitForm(HttpServletRequest request,
			HttpServletResponse response, Model model,
			@Valid UserAccounts user, BindingResult result) {
		model.addAttribute("user", user);
		String returnVal = "login";
		if (result.hasErrors()) {
			returnVal = "login";
		} else {
			try {
				UserAccounts userAccounts = userDao
						.queryUserAccountByNameAndPassword(user.getUserName(),
								user.getUserPassword());

				if (userAccounts != null) {
					Person userProfile = userDao.queryUserProfileByUserId(
							userAccounts.getUserId(),
							userAccounts.getUserType());
					// set cookie and session
					setCookieAndSession(request, userProfile, response,
							userAccounts, userAccounts.getUserType());
					//
					String roleNme = userAccounts.getRole().getRoleName();

					List<Events> eventList = homeDao.fetchEvents();
					model.addAttribute("eventList", eventList);
					switch (roleNme) {
					case "Professor":
						List<CourseOffering> courseOfferingList1 = courseDao
								.getRegisteredCoursesForProfessor(userAccounts
										.getUserId());
						List<Announcement> announcementList = userDao
								.queryAnnouncement(userProfile, "Professor",
										courseOfferingList1);
						model.addAttribute("announcementList", announcementList);
						returnVal = "instructor_home";
						break;
					case "Student":
						List<CourseOffering> courseOfferingList = courseDao
								.getRegisteredCourses(userAccounts.getUserId());
						List<Announcement> announcementListStudent = userDao
								.queryAnnouncement(userProfile, "Student",
										courseOfferingList);
						model.addAttribute("announcementList",
								announcementListStudent);
						returnVal = "student_home";
						break;
					case "Career Staff":
						returnVal = "career_staff_home";
						break;
					default:
						returnVal = "login";
						break;
					}

					model.addAttribute(CommonConstants.SESSION_USERACCOUNTS,
							userAccounts);
					model.addAttribute(CommonConstants.SESSION_USERPROFILE,
							userProfile);

				} else {
					model.addAttribute("error", "Account does not exist.");
					returnVal = "login";
				}
				return returnVal;
			} catch (Exception e) {
				model.addAttribute("error",
						"Sorry there was an error. Please try again!!!");
				returnVal = "login";
				return returnVal;
			}
		}

		if (result.hasErrors()) {
			int i = 0;
			for (ObjectError objError : result.getAllErrors()) {
				model.addAttribute("error" + i, objError.getDefaultMessage());
				i++;
			}
		}

		return returnVal;
	}

	/**
	 * 
	 * @param request
	 * @param person
	 * @param response
	 * @param userAccounts
	 * @param table
	 */
	private void setCookieAndSession(HttpServletRequest request, Person person,
			HttpServletResponse response, UserAccounts userAccounts,
			String table) {

		if (request.getParameter(CommonConstants.REMEMBER) != null) {
			Cookie userNameCookie = new Cookie(CommonConstants.COOKIE_USERNAME,
					userAccounts.getUserName());
			Cookie userIdCookie = new Cookie(CommonConstants.COOKIE_USERID,
					userAccounts.getUserId().toString());
			Cookie userRoleCookie = new Cookie(CommonConstants.COOKIE_USERROLE,
					userAccounts.getRole().getRoleName());
			Cookie userTypeCookie = new Cookie(CommonConstants.COOKIE_USERTYPE,
					userAccounts.getUserType());

			userNameCookie.setMaxAge(604800);
			userIdCookie.setMaxAge(604800);
			userRoleCookie.setMaxAge(604800);
			userTypeCookie.setMaxAge(604800);
			response.addCookie(userNameCookie);
			response.addCookie(userIdCookie);
			response.addCookie(userRoleCookie);
			response.addCookie(userTypeCookie);
		}

		HttpSession httpSession = request.getSession();
		httpSession.setAttribute(CommonConstants.SESSION_USERID, userAccounts
				.getUserId().toString());
		httpSession.setAttribute(CommonConstants.SESSION_USERNAME,
				userAccounts.getUserName());
		httpSession.setAttribute(CommonConstants.SESSION_USERFULLNAME,
				person.getFirstName() + " " + person.getLastName());
		httpSession.setAttribute(CommonConstants.SESSION_USERROLE, userAccounts
				.getRole().getRoleName());
		httpSession.setAttribute(CommonConstants.SESSION_USERTYPE,
				userAccounts.getUserType());
		if (person instanceof Student) {
			httpSession.setAttribute(CommonConstants.SESSION_USERPROFILE,
					(Student) person);
		} else if (person instanceof Employee) {
			httpSession.setAttribute(CommonConstants.SESSION_USERPROFILE,
					(Employee) person);
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		HttpSession httpSession = request.getSession();
		Cookie cookieArray[] = request.getCookies();
		for (Cookie cookie : cookieArray) {
			if (cookie.getName().equals(CommonConstants.COOKIE_USERID)
					|| cookie.getName().equals(CommonConstants.COOKIE_USERNAME)
					|| cookie.getName().equals(CommonConstants.COOKIE_USERROLE)
					|| cookie.getName().equals(CommonConstants.COOKIE_USERTYPE)) {
				cookie.setMaxAge(0);
				response.addCookie(cookie);
			}
		}
		if (httpSession != null)
			httpSession.invalidate();
		UserAccounts user = new UserAccounts();
		model.addAttribute("loginForm", user);
		return "redirect:";
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String errorRedirect(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return "error";
	}

}
