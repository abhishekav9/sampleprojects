package edu.neu.info6250.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.neu.info6250.dao.JobDao;
import edu.neu.info6250.model.Job;

@Controller
public class JobController {

	@Autowired
	private JobDao jobDao;

	// @RequestMapping(value = "/upload", method = RequestMethod.POST)
	// public String uploadForm(@RequestParam("file") MultipartFile[] file,
	// Model map) {
	//
	// List<String> fileNames = new ArrayList<String>();
	//
	// String returnval = "error";
	// if (null != file && file.length > 0) {
	// for (MultipartFile multipartFile : file) {
	//
	// String fileName = multipartFile.getOriginalFilename();
	// if (!"".equalsIgnoreCase(fileName)) {
	// try {
	// multipartFile.transferTo(new File(
	// "/Users/abhishekashwathnarayanvenkat/"
	// + fileName));
	// } catch (Exception e) {
	// System.out.println(e.getMessage());
	// returnval = "error";
	// }
	// fileNames.add(fileName);
	// }
	// }
	// }
	//
	// map.addAttribute("files", fileNames);
	// return returnval;
	// }

	// searchJobs

	/**
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/searchJobs", method = RequestMethod.POST)
	public String getMyCoursesPost(HttpServletRequest request, Model model) {

		String returnval = "job_search";
		return returnval;
	}
	/**
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/searchJobs", method = RequestMethod.GET)
	public String getMyCourses(HttpServletRequest request, Model model) {

		String returnval = "job_search";
		return returnval;
	}

	@RequestMapping(value = "/quickJobSearch", method = RequestMethod.POST)
	public String quickSearch(@RequestParam("carRequired") String carRequired,
			@RequestParam("jobType") String jobType,
			@RequestParam("postedFrom") String postedFrom,
			@RequestParam("postedTo") String postedTo,
			@RequestParam("duration") String duration,
			@RequestParam("jobTitle") String jobTitle,
			@RequestParam("gpa") String gpa, Model model) {

		try {

			if (postedTo.isEmpty() || postedFrom.isEmpty()) {
				if (!postedTo.isEmpty() && postedFrom.isEmpty()) {
					model.addAttribute("error", "Enter proper values");
					return "forward:/searchJobs";
				} else if(postedTo.isEmpty() && !postedFrom.isEmpty()) {
					model.addAttribute("error", "Enter proper values");
					return "forward:/searchJobs";
				}
			}
			if (gpa != "")
				Float.parseFloat(gpa);
			if (duration != "")
				Integer.parseInt(duration);
			List<Job> jobList = jobDao.quickSearchJob(carRequired, duration,
					gpa, jobTitle, jobType, postedFrom, postedTo);
			model.addAttribute("eventList", jobList);
			model.addAttribute("resultPage", "resultPageMany");
			return "resultPageMany";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "Enter proper values");
			return "forward:/searchJobs";
		}
	}

	/**
	 * 
	 * @param request
	 * @param jobTitle
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/jobTitleSearch", method = RequestMethod.POST)
	public String getJobTitleSearch(HttpServletRequest request,
			@RequestParam("jobTitle") String jobTitle, Model model) {

		try {
			List<Job> jobList = jobDao.quickFindByTitle(jobTitle);
			model.addAttribute("eventList", jobList);
			return "resultPageMany";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "Enter proper values");
			return "forward:/searchJobs";
		}
	}

	/**
	 * 
	 * @param memberNumber
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/jobIdSearch", method = RequestMethod.POST)
	public String memberNumberSearch(
			@RequestParam("jobId") Integer memberNumber, Model model) {

		try {
			List<Job> jobList = jobDao.quickFindById(memberNumber);
			model.addAttribute("eventList", jobList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "Enter proper values");
			return "forward:/searchJobs";
		}
		return "resultPageMany";
	}

}
