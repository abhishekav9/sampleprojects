package edu.neu.info6250.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import edu.neu.info6250.model.Book;
import edu.neu.info6250.model.Student;
import edu.neu.info6250.model.StudentHasBook;

public class BookDao extends Dao {

	/**
	 * 
	 * @param bookAuthor
	 * @param bookAvailability
	 * @param bookCategory
	 * @param bookDesc
	 * @param bookName
	 * @param bookPrice
	 * @param isbn
	 * @param bookEdition
	 * @throws Exception
	 */
	public void insertBooks(String bookAuthor, int bookAvailability,
			String bookCategory, String bookDesc, String bookName,
			float bookPrice, String bookEdition) throws Exception {
		try {
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			Book library = new Book();
			library.setBookAuthor(bookAuthor);
			library.setBookAvailability(bookAvailability);
			library.setBookCategory(bookCategory);
			library.setBookDesc(bookDesc);
			library.setBookEdition(bookEdition);
			library.setBookName(bookName);
			library.setBookPrice(bookPrice);

			tx.commit();
			session.close();
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add book to library " + bookName, e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Book> searchBooks(String bookAuthor, String bookCategory,
			String bookEdition, String bookName) throws Exception {

		Criteria criteriaQuery = null;
		try {
			criteriaQuery = getSession().createCriteria(Book.class);
			addLikeRestrictionIfNotNull(criteriaQuery, "bookAuthor", bookAuthor);
			addRestrictionIfNotNull(criteriaQuery, "bookCategory", bookCategory);
			addRestrictionIfNotNull(criteriaQuery, "bookEdition", bookEdition);
			addLikeRestrictionIfNotNull(criteriaQuery, "bookName", bookName);

			List<Book> bookList = criteriaQuery.list();
			return bookList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not search for the book", e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<StudentHasBook> queryBooksCheckedOut(int studentId)
			throws Exception {

		Criteria criteriaQuery = null;
		try {
			Query q = getSession().createQuery(
					"from Student where studentId = :userId");
			q.setInteger("userId", studentId);
			Student user = (Student) q.uniqueResult();
			criteriaQuery = getSession().createCriteria(StudentHasBook.class);
			addRestrictionIfNotNull(criteriaQuery, "student", user);

			List<StudentHasBook> bookList = criteriaQuery.list();
			return bookList;
		} catch (HibernateException e) {
			// rollback();
			System.out.println(e.getMessage());
			throw new Exception(e);
		}
	}

	/**
	 * 
	 * @param asList
	 * @throws Exception
	 */
	public void deleteBooksById(List<Integer> asList) throws Exception {

		try {

			Session session = getSession();
			Transaction tx = session.beginTransaction();
			Query q = session
					.createQuery("delete from Book where isbn IN (:isbn)");
			q.setParameterList("isbn", asList);
			q.executeUpdate();
			tx.commit();
			session.close();
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not delete books " + asList, e);
		}

	}

	/**
	 * 
	 * @param criteria
	 * @param propertyName
	 * @param value
	 */
	private void addLikeRestrictionIfNotNull(Criteria criteria,
			String propertyName, Object value) {
		if (value != null && !value.toString().isEmpty()) {
			criteria.add(Restrictions.ilike(propertyName, "%" + value + "%"));
		}
	}

	/**
	 * 
	 * @param criteria
	 * @param propertyName
	 * @param value
	 */
	private void addRestrictionIfNotNull(Criteria criteria,
			String propertyName, Object value) {
		if (value != null && !value.toString().isEmpty()) {
			criteria.add(Restrictions.eq(propertyName, value));
		}
	}

}
