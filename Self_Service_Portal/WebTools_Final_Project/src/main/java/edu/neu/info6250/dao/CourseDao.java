package edu.neu.info6250.dao;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import edu.neu.info6250.model.Course;
import edu.neu.info6250.model.CourseMaterials;
import edu.neu.info6250.model.CourseOffering;
import edu.neu.info6250.model.Employee;
import edu.neu.info6250.model.StudentCourseRegistered;

public class CourseDao extends Dao {

	// public void insertCourse() {
	// try {
	// Session session = getSession();
	// Transaction tx = session.beginTransaction();
	// Course course = new Course();
	// course.setCostPerCredit(costPerCredit);
	// course.setCourseCode(courseCode);
	// course.setCourseDesc(courseDesc);
	// course.setCourseDuration(courseDuration);
	// course.setCourseName(courseName);
	// course.setCredits(credits);
	// course.setProgram(program);
	// // course.setCoursePrerequisite(coursePrerequisite);
	// session.save(course);
	// tx.commit();
	// session.close();
	// } catch (HibernateException e) {
	// // rollback();
	// throw new Exception("Could not add contact to user " + contactName,
	// e);
	// }
	// }

	// public void registerForCourse(CourseOffering courseOffering, Student
	// student) {
	// try {
	// Session session = getSession();
	// Transaction tx = session.beginTransaction();
	// StudentCourseRegistered register = new StudentCourseRegistered();
	// register.setCourseOffering(courseOffering);
	// courseOffering.setAvailability(courseOffering.getAvailability() + 1);
	// register.setGrade(grade);
	// register.setSemester(semester);
	// register.setStudent(student);
	// register.setYear(year);
	//
	// session.save(register);
	// tx.commit();
	// session.close();
	// } catch (HibernateException e) {
	// // rollback();
	// throw new Exception("Could not add contact to user " + contactName,
	// e);
	// }
	// }

	// public void insertCourseOffering(Course course) {
	// try {
	// Session session = getSession();
	// Transaction tx = session.beginTransaction();
	// CourseOffering courseOffering = new CourseOffering();
	// courseOffering.setAvailability(availability);
	// courseOffering.setCourse(course);
	// courseOffering.setAvailability(availability);
	// courseOffering.setCourseRegnNum(courseRegnNum);
	// courseOffering.setEmployee(employee);
	// courseOffering.setEndDate(endDate);
	// courseOffering.setLocation(location);
	// courseOffering.setSecNo(secNo);
	// courseOffering.setStartDate(startDate);
	// session.save(courseOffering);
	// tx.commit();
	// session.close();
	// } catch (HibernateException e) {
	// // rollback();
	// throw new Exception("Could not add contact to user " + contactName,
	// e);
	// }
	// }

	/**
	 *
	 * @param registered
	 * @throws Exception
	 */
	public void dropCourse(StudentCourseRegistered registered) throws Exception {
		try {
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			session.delete(registered);
			tx.commit();
			session.close();
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user "
					+ registered.getStudentRegId(), e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public String getCourseMaterialFromId(int id) throws Exception {
		try {
			Query q = getSession()
					.createQuery(
							"from CourseMaterials where idcourseMaterials = :studentRegId");
			q.setInteger("studentRegId", id);
			CourseMaterials eachCourseOffering = (CourseMaterials) q
					.uniqueResult();
			return eachCourseOffering.getUploadedPath();
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user ", e);
		}
	}
	
	
	/**
	 * 
	 * @param courseMaterials
	 * @return
	 * @throws Exception
	 */
	public void insertCourseMaterial(CourseMaterials courseMaterials) throws Exception {
		try {
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			session.save(courseMaterials);
			tx.commit();
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user ", e);
		}
	}
	

	/**
	 * 
	 * @param emp
	 * @return
	 * @throws Exception
	 */
	public List<CourseOffering> getCourseOfferingFromEmployee(Employee emp)
			throws Exception {
		try {
			Query q = getSession().createQuery(
					"from CourseOffering where employee = :employee");
			q.setParameter("employee", emp);
			List<CourseOffering> courseOfferingList = q.list();
			return courseOfferingList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user ", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CourseOffering getCourseOfferingFromId(int id) throws Exception {
		try {
			Query q = getSession().createQuery(
					"from CourseOffering where courseRegnNum = :courseRegnNum");
			q.setInteger("courseRegnNum", id);
			CourseOffering eachCourseOffering = (CourseOffering) q
					.uniqueResult();
			return eachCourseOffering;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user ", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Course getCoursesFromCourseOfferingId(int id) throws Exception {
		try {

			Criteria c = getSession().createCriteria(Course.class, "co");
			c.createAlias("co.courseOfferings", "cOffer");
			c.add(Restrictions.eq("cOffer.courseRegnNum", id));

			Course course = (Course) c.uniqueResult();
			return course;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user ", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<CourseOffering> getRegisteredCoursesForProfessor(int id) throws Exception {
		try {

			Criteria c = getSession()
					.createCriteria(CourseOffering.class, "co");
			c.createAlias("co.employee", "emp");
			c.add(Restrictions.eq("emp.personId", id));
			List<CourseOffering> courseOfferings = c.list();
			return courseOfferings;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user ", e);
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<CourseOffering> getRegisteredCourses(int id) throws Exception {
		try {

			Criteria c = getSession()
					.createCriteria(CourseOffering.class, "co");
			c.createAlias("co.studentCourseRegistereds", "scr");
			c.createAlias("scr.student", "stu");
			c.add(Restrictions.eq("stu.personId", id));
			List<CourseOffering> courseOfferings = c.list();
			return courseOfferings;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user ", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CourseOffering getCourseOfferingFromCourseRegId(int id)
			throws Exception {
		try {
			Query q = getSession()
					.createQuery(
							"from StudentCourseRegistered where studentRegId = :studentRegId");
			q.setInteger("studentRegId", id);
			StudentCourseRegistered courseOffering = (StudentCourseRegistered) q
					.uniqueResult();
			CourseOffering cf = courseOffering.getCourseOffering();
			return cf;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user ", e);
		}
	}

	/**
	 * 
	 * @param courseRegnNum
	 * @return
	 * @throws Exception
	 */
	public List<CourseMaterials> getCourseMaterials(int courseRegnNum)
			throws Exception {
		try {
			Query q = getSession().createQuery(
					"from CourseOffering where courseRegnNum = :courseRegnNum");
			q.setInteger("courseRegnNum", courseRegnNum);
			CourseOffering courseOfferingList = (CourseOffering) q
					.uniqueResult();
			Set<CourseMaterials> courseMaterialSet = courseOfferingList
					.getCourseMaterialses();
			List<CourseMaterials> courseMaterialList = new LinkedList<>();
			courseMaterialList.addAll(courseMaterialSet);
			return courseMaterialList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user ", e);
		}
	}
}
