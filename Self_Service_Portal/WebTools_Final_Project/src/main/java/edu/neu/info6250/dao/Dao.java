package edu.neu.info6250.dao;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class Dao {

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private static final Logger log = Logger.getAnonymousLogger();

	private static final ThreadLocal sessionThread = new ThreadLocal();
	// private static final SessionFactory sessionFactory = new Configuration()
	// .configure().buildSessionFactory();
	private static final SessionFactory sessionFactory = buildSessionFactory();

	// //////////////

	private static SessionFactory buildSessionFactory() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Configuration configuration = new Configuration();
			configuration.configure();
			StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties());
			return configuration.buildSessionFactory(ssrb.build());

		} catch (Throwable th) {
			System.err.println("Error in Session Factory Init..");
			try {
				throw new Exception();
			} catch (Exception e) {
				System.err
						.println("HibernateUtil.buildSessionFactory()...Error");
			}
		}
		return sessionFactory;
	}

	// ///////////////////////

	protected Dao() {
	}

	public static Session getSession() {
		Session session = (Session) Dao.sessionThread.get();

		if (session == null) {
			session = sessionFactory.openSession();
			Dao.sessionThread.set(session);
		}
		return session;
	}

	protected void begin() {
		getSession().beginTransaction();
	}

	protected void commit() {
		getSession().getTransaction().commit();
	}

	protected void rollback() {
		try {
			getSession().getTransaction().rollback();
		} catch (HibernateException e) {
			log.log(Level.WARNING, "Cannot rollback", e);
		}
		try {
			getSession().close();
		} catch (HibernateException e) {
			log.log(Level.WARNING, "Cannot close", e);
		}
		Dao.sessionThread.set(null);
	}

	public static void close() {
		getSession().close();
		Dao.sessionThread.set(null);
	}
}
