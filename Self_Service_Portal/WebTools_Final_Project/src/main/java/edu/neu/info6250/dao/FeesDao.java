package edu.neu.info6250.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import edu.neu.info6250.model.Invoice;
import edu.neu.info6250.model.InvoiceItem;
import edu.neu.info6250.model.Student;

public class FeesDao extends Dao {

	public void insertFeeInvoice(Set<InvoiceItem> invoiceItems,
			Student student, float paidAmt, Date dueDate) throws Exception {
		try {
			Session session = getSession();
			Transaction tx = session.beginTransaction();

			Invoice invoice = new Invoice();
			invoice.setGeneratedDate(new Date());
			invoice.setPaidAmt(paidAmt);
			invoice.setDueDate(dueDate);
			invoice.setStudent(student);
			invoice.setInvoiceItems(invoiceItems);
			session.save(invoice);

			tx.commit();
			session.close();
		} catch (HibernateException e) {
			throw new Exception("Could not insertFeeInvoice ", e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Invoice> getInvoicesForStudent(Student student)
			throws Exception {

		Criteria criteriaQuery = null;
		try {
			criteriaQuery = getSession().createCriteria(Invoice.class);
			addRestrictionIfNotNull(criteriaQuery, "student", student);

			List<Invoice> invoiceList = criteriaQuery.list();
			return invoiceList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user ", e);
		}
	}

	/**
	 * 
	 * @param invoice
	 * @throws Exception
	 */
	public void updateInvoiceForStudent(Invoice invoice) throws Exception {

		try {
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			session.update(invoice);
			tx.commit();
			session.close();

		} catch (Exception e) {
			// rollback();
			throw new Exception("Could not add contact to user "
					+ invoice.getInvoiceId().toString(), e);
		}
	}

	/**
	 * 
	 * @param criteria
	 * @param propertyName
	 * @param value
	 */
	private void addRestrictionIfNotNull(Criteria criteria,
			String propertyName, Object value) {
		if (value != null && !value.toString().isEmpty()) {
			criteria.add(Restrictions.eq(propertyName, value));
		}
	}
}
