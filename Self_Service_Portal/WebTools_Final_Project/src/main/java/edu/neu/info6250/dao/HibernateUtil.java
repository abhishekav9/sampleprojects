package edu.neu.info6250.dao;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static SessionFactory SESSION_FACTORY = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Configuration configuration = new Configuration();
			configuration.configure();
			StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties());
			SESSION_FACTORY = configuration.buildSessionFactory(ssrb.build());
			return SESSION_FACTORY;

		} catch (Throwable th) {
			System.err.println("Error in Session Factory Init..");
			try {
				throw new Exception();
			} catch (Exception e) {
				System.err
						.println("HibernateUtil.buildSessionFactory()...Error");
			}
		}
		return SESSION_FACTORY;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */

	public static SessionFactory getSessionFactory() throws Exception {
		if (SESSION_FACTORY == null) {
			SESSION_FACTORY = buildSessionFactory();
		}
		return SESSION_FACTORY;
	}
}
