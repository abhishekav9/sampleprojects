package edu.neu.info6250.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import edu.neu.info6250.model.Announcement;
import edu.neu.info6250.model.CourseOffering;
import edu.neu.info6250.model.Events;

public class HomeDao extends Dao {

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Announcement> fetchAnnouncement(
			List<CourseOffering> courseOfferings) throws Exception {
		Criteria criteriaQuery = null;
		try {
			// begin();
			criteriaQuery = getSession().createCriteria(Announcement.class);
			// criteriaQuery.add(Restrictions.eq("gender", gender));
			// addRestrictionIfNotNull(criteriaQuery, "country", country);
			List<Announcement> userList = criteriaQuery.list();
			return userList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not search", e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Events> fetchEvents() throws Exception {
		Criteria criteriaQuery = null;
		try {
			// begin();
			criteriaQuery = getSession().createCriteria(Events.class);
			List<Events> userList = criteriaQuery.list();
			return userList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not search", e);
		}
	}

	/**
	 * 
	 * @param criteria
	 * @param propertyName
	 * @param value
	 */
	private void addRestrictionIfNotNull(Criteria criteria,
			String propertyName, Object value) {
		if (value != null && !value.toString().isEmpty()) {
			criteria.add(Restrictions.eq(propertyName, value));
		}
	}
}
