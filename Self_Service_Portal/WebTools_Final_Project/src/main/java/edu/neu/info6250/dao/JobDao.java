package edu.neu.info6250.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import edu.neu.info6250.model.Company;
import edu.neu.info6250.model.Job;
import edu.neu.info6250.model.Student;
import edu.neu.info6250.model.StudentJobApplication;

public class JobDao extends Dao {

	/**
	 * 
	 * @param jobTitle
	 * @return
	 * @throws Exception
	 */
	public List<Job> quickFindByTitle(String jobTitle) throws Exception {

		List<Job> jobList = null;
		try {
			Session session = getSession();
			Query query = session.getNamedQuery("findJobByTitle");
			query.setString("jobTitle", "%" + jobTitle + "%");

			jobList = query.list();
			return jobList;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return jobList;
	}

	/**
	 * 
	 * @param jobTitle
	 * @return
	 * @throws Exception
	 */
	public List<Job> quickFindById(int jobId) throws Exception {

		List<Job> jobList = null;
		try {
			Session session = getSession();
			Query query = session.getNamedQuery("findJobById");
			query.setInteger("jobId", jobId);

			jobList = query.list();
			return jobList;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return jobList;
	}

	/**
	 * 
	 * @param carRequired
	 * @param company
	 * @param duration
	 * @param jobDesc
	 * @param jobLocation
	 * @param jobTitle
	 * @param jobType
	 * @param maxWage
	 * @param minGpa
	 * @param minWage
	 * @throws Exception
	 */
	public void insertJob(String carRequired, Company company, int duration,
			String jobDesc, String jobLocation, String jobTitle,
			String jobType, float maxWage, float minGpa, float minWage)
			throws Exception {
		try {
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			Job job = new Job();
			job.setCarRequired(carRequired);
			job.setCompany(company);
			job.setCreatedDate(new Date());
			job.setDuration(duration);
			job.setJobDesc(jobDesc);
			job.setJobLocation(jobLocation);
			job.setJobTitle(jobTitle);
			job.setJobType(jobType);
			job.setMaxWage(maxWage);
			job.setMinWage(minWage);
			job.setMinGpa(minGpa);
			tx.commit();
			session.close();
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add job " + jobTitle, e);
		}
	}

	/**
	 * @throws Exception
	 * 
	 */
	public void insertJobApplication(Job job, int jobProfileId, Student student)
			throws Exception {
		try {
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			StudentJobApplication jobApplication = new StudentJobApplication();
			jobApplication.setApplicationStatus("SUBMITTED");
			jobApplication.setAppliedDate(new Date());
			jobApplication.setJob(job);
			jobApplication.setJobProfileId(jobProfileId);
			jobApplication.setStudent(student);
			session.save(jobApplication);
			tx.commit();
			session.close();
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add application "
					+ job.getJobTitle(), e);
		}
	}

	/**
	 * 
	 * @param jobApplication
	 * @throws Exception
	 */
	public void editJobApplication(StudentJobApplication jobApplication,
			String applicationStatus, int jobProfileId) throws Exception {
		try {
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			jobApplication.setApplicationStatus(applicationStatus);
			jobApplication.setJobProfileId(jobProfileId);
			session.save(jobApplication);
			tx.commit();
			session.close();
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user "
					+ jobApplication.getApplicationId(), e);
		}
	}

	/**
	 * 
	 * @param student
	 * @return
	 */
	public List<StudentJobApplication> studentAppliedJobSearch(Student student) {
		Criteria criteriaQuery = null;
		List<StudentJobApplication> jobList = null;
		try {
			criteriaQuery = getSession().createCriteria(
					StudentJobApplication.class);
			addRestrictionIfNotNull(criteriaQuery, "student", student);
			jobList = criteriaQuery.list();
			return jobList;

		} catch (Exception e) {
			// rollback();
		}
		return jobList;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Job> quickSearchJob(String carRequired, String duration,
			String gpa, String jobTitle, String jobType, String lo, String hi)
			throws Exception {
		Criteria criteriaQuery = null;
		try {
			criteriaQuery = getSession().createCriteria(Job.class);
			addRestrictionIfNotNull(criteriaQuery, "carRequired", carRequired);
			addRestrictionIfNotNull(criteriaQuery, "duration", duration);
			addRestrictionIfNotNull(criteriaQuery, "jobTitle", jobTitle);// like
			addRestrictionIfNotNull(criteriaQuery, "jobType", jobType);
			addRestrictionGe(criteriaQuery, "minGpa", gpa);
			addRestrictionBetweenIfNotNull(criteriaQuery, "createdDate", lo, hi);

			List<Job> jobList = criteriaQuery.list();
			return jobList;

		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user " + jobTitle, e);
		}
	}

	/**
	 * 
	 * @param criteria
	 * @param propertyName
	 * @param value
	 */
	private void addRestrictionIfNotNull(Criteria criteria,
			String propertyName, Object value) {
		if (value != null && !value.toString().isEmpty()) {
			criteria.add(Restrictions.eq(propertyName, value));
		}
	}

	/**
	 * 
	 * @param criteria
	 * @param propertyName
	 * @param value
	 * @throws ParseException
	 */
	private void addRestrictionBetweenIfNotNull(Criteria criteria,
			String propertyName, String lo, String hi) throws ParseException {
		if ((!lo.isEmpty()) && (!hi.isEmpty())) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			Date myDate = format.parse(lo);
			criteria.add(Restrictions.ge(propertyName, myDate));
			Calendar cal = Calendar.getInstance();
			cal.setTime(format.parse(hi));
			cal.add(Calendar.DATE, 1);
			String myDate1 = cal.get(Calendar.YEAR)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.DATE);
			Date myDateHi = format.parse(myDate1);
			criteria.add(Restrictions.le(propertyName, myDateHi));
		}
	}

	/**
	 * 
	 * @param criteria
	 * @param propertyName
	 * @param value
	 * @throws ParseException
	 */
	private void addRestrictionGe(Criteria criteria, String loPropertyName,
			String lo) {
		if (lo != null && !lo.isEmpty()) {
			criteria.add(Restrictions.ge(loPropertyName, Float.parseFloat(lo)));
		}
	}
}
