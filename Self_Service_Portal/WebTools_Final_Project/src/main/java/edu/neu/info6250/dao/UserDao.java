package edu.neu.info6250.dao;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import edu.neu.info6250.common.constants.CommonConstants;
import edu.neu.info6250.model.Announcement;
import edu.neu.info6250.model.CourseOffering;
import edu.neu.info6250.model.Employee;
import edu.neu.info6250.model.Person;
import edu.neu.info6250.model.Program;
import edu.neu.info6250.model.Student;
import edu.neu.info6250.model.UserAccounts;

public class UserDao extends Dao {

	/**
	 * 
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public List<Announcement> queryAnnouncement(Person person, String role,
			List<CourseOffering> courseListPassed) throws Exception {
		try {
			// begin();
			Query q = null;
			if (role.equals("Professor")) {
				Employee emp = (Employee) person;
				Set<CourseOffering> courseOfferingSet = emp
						.getCourseOfferings();
				List<CourseOffering> courseList = new LinkedList<>();
				if (courseOfferingSet != null && courseOfferingSet.size() > 0) {
					courseList.addAll(courseOfferingSet);
				}
				q = getSession()
						.createQuery(
								"from Announcement where announcementType = :toStudentType1 OR courseOffering IN (:courseOffering)");
				q.setParameterList("courseOffering", courseListPassed);
				q.setString("toStudentType1", CommonConstants.GENERAL);
			} else if (role.equals("Student")) {

				// Student emp = (Student) person;
				// Set<StudentCourseRegistered> studentCourseRegistered =
				// emp.getStudentCourseRegistereds();
				// List<CourseOffering> courseList = new LinkedList<>();
				// if (studentCourseRegistered != null &&
				// studentCourseRegistered.size() > 0) {
				// for (StudentCourseRegistered studentRegistered :
				// studentCourseRegistered) {
				// courseList.add(studentRegistered.getCourseOffering());
				// }
				// }
				q = getSession()
						.createQuery(
								"from Announcement where announcementType = :toStudentType1 OR student = :student OR courseOffering IN (:courseOffering)");
				q.setString("toStudentType1", CommonConstants.GENERAL);
				q.setParameter("student", (Student) person);
				q.setParameterList("courseOffering", courseListPassed);
			}
			List<Announcement> announcementList = q.list();
			// commit();
			return announcementList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get announcement", e);
		}
	}

	/**
	 * 
	 * @param contactName
	 * @param userName
	 * @param message
	 * @throws Exception
	 */
	public void insertStudent(String firstName, String lastName,
			String address1, String address2, String city, String country,
			String emailId, String gender, String international,
			String dateOfBirth, String ssn, String study, String zipcode,
			String mobile, Program program, String localAddress,
			String localCity, String localState, String localZipCode,
			String state, String status) throws Exception {
		try {
			Session session = getSession();
			Transaction tx = session.beginTransaction();

			Person person = new Person();
			person.setFirstName(firstName);
			person.setLastName(lastName);
			person.setAddress1(address1);
			person.setCity(city);
			person.setAddress2(address2);
			person.setCountry(country);
			person.setDateOfBirth(dateOfBirth);
			person.setEmailId(emailId);
			person.setGender(gender);
			person.setMobile(mobile);
			person.setState(state);
			person.setZipcode(zipcode);

			Student student = new Student();
			student.setInternational(international);
			student.setLocalAddress(localAddress);
			student.setLocalCity(localCity);
			student.setLocalState(localState);
			student.setLocalZipCode(localZipCode);
			student.setProgram(program);
			student.setStudy(study);
			student.setStudentNum(student.getStudentNum());
			student.setStatus(status);
			student.setSsn(ssn);
			student.setPerson(person);

			session.save(person);
			session.save(student);
			tx.commit();
			session.close();
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not add contact to user " + firstName, e);
		}
	}

	/**
	 * 
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	public List<Student> queryStudentsByStudentNum(String studentNum,
			String status) throws Exception {
		try {
			// begin();
			Query q = getSession()
					.createQuery(
							"from Student where studentNum = :studentNum AND status = :status");
			q.setString("studentNum", studentNum);
			q.setString("status", status);
			List<Student> studentList = q.list();
			// commit();
			return studentList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get student for " + studentNum, e);
		}
	}

	/**
	 * 
	 * @param contactId
	 * @return
	 * @throws Exception
	 */
	public boolean deactivateStudentById(List<String> studentNumList)
			throws Exception {
		try {
			// begin();
			Query q = getSession()
					.createQuery(
							"update Student set status =:status where studentNum IN (:studentNum)");
			q.setString("status", CommonConstants.INACTIVE);
			q.setParameterList("studentNum", studentNumList);
			int i = q.executeUpdate();
			if (i > 0)
				return true;
			else
				return false;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not deactivate students: ", e);
		}
	}

	/**
	 * 
	 * @param name
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public List<Student> queryUserByLastName(String lastName) throws Exception {
		try {
			// begin();
			Query q = getSession().createQuery(
					"from Person where lastName = :lastName");
			q.setString("lastName", lastName);
			List<Person> personList = q.list();

			List<Integer> personIdList = new LinkedList<Integer>();
			for (Person person : personList) {
				personIdList.add(person.getPersonId());
			}
			q = getSession().createQuery(
					"from Student where studentId IN (:studentId)");
			q.setParameterList("studentId", personIdList);
			// commit();
			List<Student> studentList = q.list();
			return studentList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get studentList by LastName "
					+ lastName, e);
		}
	}

	/**
	 * 
	 * @param name
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public UserAccounts queryUserAccountByNameAndPassword(String name,
			String password) throws Exception {
		try {
			// begin();
			Query q = getSession()
					.createQuery(
							"from UserAccounts where userName = :username and userPassword = :password");
			q.setString("username", name);
			q.setString("password", password);
			UserAccounts userAccounts = (UserAccounts) q.uniqueResult();

			// commit();
			return userAccounts;
		} catch (Exception e) {
			// rollback();
			throw new Exception("Could not get user " + name, e);
		}
	}

	/**
	 * 
	 * @param name
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public UserAccounts queryUserAccountById(int userId) throws Exception {
		try {
			// begin();
			Query q = getSession().createQuery(
					"from UserAccounts where userId = :userId");
			q.setInteger("userId", userId);
			UserAccounts userAccounts = (UserAccounts) q.uniqueResult();

			// commit();
			return userAccounts;
		} catch (Exception e) {
			// rollback();
			throw new Exception("Could not get user " + userId, e);
		}
	}

	/**
	 * 
	 * @param userAccountsUserId
	 * @param table
	 * @return
	 * @throws Exception
	 */
	public Person queryPersonByPersonId(int personId) throws Exception {
		try {
			// begin();
			Query q = getSession().createQuery(
					"from Person where personId = :personId");
			q.setInteger("personId", personId);
			Person p = (Person) q.uniqueResult();
			return p;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get person " + personId, e);
		}
	}

	/**
	 * 
	 * @param userAccountsUserId
	 * @return
	 * @throws Exception
	 */
	public Person queryUserProfileByUserId(int userId, String table)
			throws Exception {
		try {
			switch (table) {
			case CommonConstants.STUDENT:
				Query q = getSession().createQuery(
						"from Student where studentId = :userId");
				q.setInteger("userId", userId);
				Student user = (Student) q.uniqueResult();
				// commit();
				return user;
			case CommonConstants.EMPLOYEE:
				Query query = getSession().createQuery(
						"from Employee where person_personId = :userId");
				query.setInteger("userId", userId);
				Employee emp = (Employee) query.uniqueResult();
				// commit();
				return emp;
			default:
				return null;
			}

		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user " + userId, e);
		}
	}

	// /**
	// *
	// * @param memberNumber
	// * @return
	// * @throws Exception
	// */
	// public Student queryStudentByUserId(int userAccountsUserId)
	// throws Exception {
	// try {
	// // begin();
	// Query q = getSession()
	// .createQuery(
	// "from Student where userAccountsUserId = :userAccountsUserId");
	// q.setInteger("userAccountsUserId", userAccountsUserId);
	// Student user = (Student) q.uniqueResult();
	// // commit();
	// return user;
	// } catch (HibernateException e) {
	// // rollback();
	// throw new Exception("Could not get user " + userAccountsUserId, e);
	// }
	// }
	//
	// /**
	// *
	// * @param userAccountsUserId
	// * @return
	// * @throws Exception
	// */
	// public Instructor queryInstructorByUserId(int userAccountsUserId)
	// throws Exception {
	// try {
	// // begin();
	// Query q = getSession()
	// .createQuery(
	// "from Instructor where userAccountsUserId = :userAccountsUserId");
	// q.setInteger("userAccountsUserId", userAccountsUserId);
	// Instructor user = (Instructor) q.uniqueResult();
	// // commit();
	// return user;
	// } catch (HibernateException e) {
	// // rollback();
	// throw new Exception("Could not get user " + userAccountsUserId, e);
	// }
	// }

	/**
	 * 
	 * @param gender
	 * @param seekingGender
	 * @param country
	 * @param city
	 * @param state
	 * @param fromAge
	 * @param toAge
	 * @return
	 * @throws Exception
	 */
	public List<Student> queryUserByQuickSearch(String gender,
			String seekingGender, String country) throws Exception {
		Criteria criteriaQuery = null;
		try {
			// begin();
			criteriaQuery = getSession().createCriteria(Student.class);
			criteriaQuery.add(Restrictions.eq("gender", gender));
			addRestrictionIfNotNull(criteriaQuery, "country", country);
			List<Student> userList = criteriaQuery.list();
			return userList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not search", e);
		}
	}

	/**
	 * 
	 * @param criteria
	 * @param propertyName
	 * @param value
	 */
	private void addRestrictionIfNotNull(Criteria criteria,
			String propertyName, Object value) {
		if (value != null && !value.toString().isEmpty()) {
			criteria.add(Restrictions.eq(propertyName, value));
		}
	}

	/**
	 * 
	 * @param user
	 * @param newPass
	 * @throws Exception
	 */
	public void setNewPassword(UserAccounts user, String newPass)
			throws Exception {
		// TODO Auto-generated method stub
		Session session = getSession();
		user.setUserPassword(newPass);
		Transaction tx = session.beginTransaction();
		session.update(user);
		tx.commit();
		session.close();
	}
}
