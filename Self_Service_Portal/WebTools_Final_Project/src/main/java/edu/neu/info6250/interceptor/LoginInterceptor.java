package edu.neu.info6250.interceptor;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import edu.neu.info6250.common.constants.CommonConstants;

public class LoginInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		if (session != null) {
			String user = (String) session.getAttribute(CommonConstants.SESSION_USERID);
			if (user != null && !user.isEmpty())
				return true;
			else {
				request.setAttribute("errorFlag", "Session timed out...");
				RequestDispatcher rd = request.getRequestDispatcher("logout");
				rd.forward(request, response);
				return false;
			}
		} else {
			request.setAttribute("errorFlag", "Session timed out...");
			RequestDispatcher rd = request.getRequestDispatcher("logout");
			rd.forward(request, response);
			return false;
		}
	}
}