package edu.neu.info6250.model;

// Generated Apr 19, 2015 2:18:29 PM by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * Employee generated by hbm2java
 */
@Entity
@Table(name = "employee", catalog = "WebTools_Final_Project")
@PrimaryKeyJoinColumn(name = "person_personId", referencedColumnName = "personId")
public class Employee extends Person implements java.io.Serializable {

	private int person_personId;
	private Location location;
	private Person person;
	private String type;
	private Set<CourseOffering> courseOfferings = new HashSet<CourseOffering>(0);

	public Employee() {
	}

	public Employee(Location location, Person person, String type) {
		this.location = location;
		this.person = person;
		this.type = type;
	}

	public Employee(Location location, Person person, String type,
			Set<CourseOffering> courseOfferings) {
		this.location = location;
		this.person = person;
		this.type = type;
		this.courseOfferings = courseOfferings;
	}

//	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "person"))
//	@Id
//	@GeneratedValue(generator = "generator")
//	@Column(name = "person_personId", unique = true, nullable = false)
//	public int getPersonPersonId() {
//		return this.personPersonId;
//	}
//
//	public void setPersonPersonId(int personPersonId) {
//		this.personPersonId = personPersonId;
//	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "location_locationId", nullable = false)
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Column(name = "type", nullable = false, length = 45)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	public Set<CourseOffering> getCourseOfferings() {
		return this.courseOfferings;
	}

	public void setCourseOfferings(Set<CourseOffering> courseOfferings) {
		this.courseOfferings = courseOfferings;
	}

}
