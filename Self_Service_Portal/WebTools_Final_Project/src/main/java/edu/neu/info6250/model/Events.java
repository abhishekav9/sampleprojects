package edu.neu.info6250.model;

// Generated Apr 19, 2015 2:18:29 PM by Hibernate Tools 4.3.1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Events generated by hbm2java
 */
@Entity
@Table(name = "events", catalog = "WebTools_Final_Project")
public class Events implements java.io.Serializable {

	private Integer idevents;
	private Location location;
	private String eventTitle;
	private String eventDesc;
	private String organizer;
	private Date startDate;
	private Date endDate;
	private String eventType;
	private String eventPath;

	public Events() {
	}

	public Events(Location location, String eventTitle, String eventDesc,
			Date startDate, Date endDate) {
		this.location = location;
		this.eventTitle = eventTitle;
		this.eventDesc = eventDesc;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Events(Location location, String eventTitle, String eventDesc,
			String organizer, Date startDate, Date endDate, String eventType,
			String eventPath) {
		this.location = location;
		this.eventTitle = eventTitle;
		this.eventDesc = eventDesc;
		this.organizer = organizer;
		this.startDate = startDate;
		this.endDate = endDate;
		this.eventType = eventType;
		this.eventPath = eventPath;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "idevents", unique = true, nullable = false)
	public Integer getIdevents() {
		return this.idevents;
	}

	public void setIdevents(Integer idevents) {
		this.idevents = idevents;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "location_locationId", nullable = false)
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Column(name = "eventTitle", nullable = false, length = 100)
	public String getEventTitle() {
		return this.eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	@Column(name = "eventDesc", nullable = false, length = 500)
	public String getEventDesc() {
		return this.eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	@Column(name = "organizer", length = 100)
	public String getOrganizer() {
		return this.organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "startDate", nullable = false, length = 19)
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "endDate", nullable = false, length = 19)
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "eventType", length = 45)
	public String getEventType() {
		return this.eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	@Column(name = "eventPath", length = 100)
	public String getEventPath() {
		return this.eventPath;
	}

	public void setEventPath(String eventPath) {
		this.eventPath = eventPath;
	}

}
