package edu.neu.info6250.model;

// Generated Apr 19, 2015 2:18:29 PM by Hibernate Tools 4.3.1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@NamedQueries({ @NamedQuery(name = "findJobByTitle", query = "from Job where jobTitle LIKE :jobTitle"), 
@NamedQuery(name="findJobById",
query="from Job where jobId = :jobId") })
/**
 * Job generated by hbm2java
 */
@Entity
@Table(name = "job", catalog = "WebTools_Final_Project")
public class Job implements java.io.Serializable {

	private Integer jobId;
	private Company company;
	private String jobTitle;
	private String jobDesc;
	private Date createdDate;
	private String carRequired;
	private String jobLocation;
	private Float minGpa;
	private Integer duration;
	private Float minWage;
	private Float maxWage;
	private String jobType;
	private Set<StudentJobApplication> studentJobApplications = new HashSet<StudentJobApplication>(
			0);

	public Job() {
	}

	public Job(Company company) {
		this.company = company;
	}

	public Job(Company company, String jobTitle, String jobDesc,
			Date createdDate, String carRequired, String jobLocation,
			Float minGpa, Integer duration, Float minWage, Float maxWage,
			String jobType, Set<StudentJobApplication> studentJobApplications) {
		this.company = company;
		this.jobTitle = jobTitle;
		this.jobDesc = jobDesc;
		this.createdDate = createdDate;
		this.carRequired = carRequired;
		this.jobLocation = jobLocation;
		this.minGpa = minGpa;
		this.duration = duration;
		this.minWage = minWage;
		this.maxWage = maxWage;
		this.jobType = jobType;
		this.studentJobApplications = studentJobApplications;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "jobId", unique = true, nullable = false)
	public Integer getJobId() {
		return this.jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "company_companyId", nullable = false)
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name = "jobTitle", length = 45)
	public String getJobTitle() {
		return this.jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	@Column(name = "jobDesc", length = 45)
	public String getJobDesc() {
		return this.jobDesc;
	}

	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdDate", length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "carRequired", length = 3)
	public String getCarRequired() {
		return this.carRequired;
	}

	public void setCarRequired(String carRequired) {
		this.carRequired = carRequired;
	}

	@Column(name = "jobLocation", length = 45)
	public String getJobLocation() {
		return this.jobLocation;
	}

	public void setJobLocation(String jobLocation) {
		this.jobLocation = jobLocation;
	}

	@Column(name = "minGpa", precision = 12, scale = 0)
	public Float getMinGpa() {
		return this.minGpa;
	}

	public void setMinGpa(Float minGpa) {
		this.minGpa = minGpa;
	}

	@Column(name = "duration")
	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@Column(name = "minWage", precision = 12, scale = 0)
	public Float getMinWage() {
		return this.minWage;
	}

	public void setMinWage(Float minWage) {
		this.minWage = minWage;
	}

	@Column(name = "maxWage", precision = 12, scale = 0)
	public Float getMaxWage() {
		return this.maxWage;
	}

	public void setMaxWage(Float maxWage) {
		this.maxWage = maxWage;
	}

	@Column(name = "jobType", length = 45)
	public String getJobType() {
		return this.jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "job")
	public Set<StudentJobApplication> getStudentJobApplications() {
		return this.studentJobApplications;
	}

	public void setStudentJobApplications(
			Set<StudentJobApplication> studentJobApplications) {
		this.studentJobApplications = studentJobApplications;
	}

}
