package edu.neu.info6250.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class PasswordChangeValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {

		return PasswordChange.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		PasswordChange user = (PasswordChange) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "oldPass",
				"validate.name", "Should not be blank");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPass",
				"validate.password", "Should not be blank");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassAgain",
				"validate.name", "Should not be blank");
	}

}