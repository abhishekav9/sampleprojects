package edu.neu.info6250.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import edu.neu.info6250.model.UserAccounts;

public class UserValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {

		return UserAccounts.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		UserAccounts user = (UserAccounts) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName",
				"validate.userName", "Username is incorrect");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPassword",
				"validate.userPassword", "Userpassword is incorrect");
	}

}
