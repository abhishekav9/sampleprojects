<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Library</title>
<script type="text/javascript">
	function checkDelete() {
		var checkboxes = document.getElementsByName('delete');
		var selected = [];
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i].checked) {
				selected.push(checkboxes[i].value);
			}
		}
		if (selected.length > 0) {
			return true;
		} else {
			alert("Select atleast one!!");
			return false;
		}
	}
</script>
</head>
<body>
	<h3>Welcome ${userAccounts.userName}</h3>

	<h4>Your Library:</h4>
	<form action="deleteMessages" method="POST"
		onsubmit="return checkDelete();">
		<table>
			<tbody>
				<tr>
					<th>Book ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Category</th>
					<th>Availability</th>
					<th>Edition</th>
					<th>Author</th>
				</tr>

				<c:forEach items="${bookList}" var="book" varStatus="loopCounter">
					<tr>
						<td><fmt:formatNumber groupingUsed="false"
								pattern="0000000000" value="${book.bookId}" /></td>
						<td><c:out value="${book.bookName}"></c:out></td>
						<td><c:out value="${book.bookDesc}"></c:out></td>
						<td><c:out value="${book.bookCategory}"></c:out></td>
						<td><c:out value="${book.bookAvailability}"></c:out></td>
						<td><c:out value="${book.bookEdition}"></c:out></td>
						<td><c:out value="${book.bookAuthor}"></c:out></td>
					</tr>
				</c:forEach>
				<tr>
					<td>You have ${announcements.size()} messages</td>
				</tr>
			</tbody>
		</table>
	</form>
</body>
</html>