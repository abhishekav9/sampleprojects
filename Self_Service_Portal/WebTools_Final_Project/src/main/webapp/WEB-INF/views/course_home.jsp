<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html lang="en">
<head>
<!-- Bootstrap Core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>My Service Portal</title>

<!-- Custom CSS -->
<style>
body {
	padding-top: 70px;
	/* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
}

.span12 {
	text-align: right
}
</style>

</head>

<body>

	<!-- Navigation -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- Page Content -->
	<div class="container">

		<div class="row">
			<h5>Welcome ${fullName}</h5>
			<br>
			<!-- <div class="col-lg-12 text-center">
				<h1>A Bootstrap Starter Template</h1>
				<p class="lead">Complete with pre-defined file paths that you
					won't have to change!</p>
				<ul class="list-unstyled">
					<li>Bootstrap v3.3.1</li>
					<li>jQuery v1.11.1</li>
				</ul>
			</div> -->
			<h4>My Courses</h4>
			<table class="table table-striped">
				<tbody>
					<tr>
						<th>Course Reg Num</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Section</th>
						<th>Location</th>
						<th>CourseMaterials</th>
					</tr>
					<c:forEach items="${eventList}" var="message"
						varStatus="loopCounter">
						<tr>
							<td><c:out value="${message.courseRegnNum}"></c:out></td>
							<td><c:out value="${message.startDate}"></c:out></td>
							<td><c:out value="${message.endDate}"></c:out></td>
							<td><c:out value="${message.secNo}"></c:out></td>
							<td><c:out value="${message.location.buildingName}"></c:out>
								<c:out value="${message.location.roomNo}"></c:out></td>
							<td><a
								href="viewCourseMaterial?sendTo=${message.courseRegnNum}">CourseMaterial</a></td>

						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->

</body>

</html>
