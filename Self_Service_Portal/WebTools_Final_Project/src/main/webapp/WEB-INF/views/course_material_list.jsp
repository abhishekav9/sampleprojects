<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html lang="en">
<head>
<!-- Bootstrap Core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>My Service Portal</title>

<!-- Custom CSS -->
<style>
body {
	padding-top: 70px;
	/* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
}

.span12 {
	text-align: right
}
</style>

</head>

<body>

	<!-- Navigation -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- Page Content -->
	<div class="container">

		<div class="row">
			<h5>Welcome ${fullName}</h5>
			<br>
			<!-- <div class="col-lg-12 text-center">
				<h1>A Bootstrap Starter Template</h1>
				<p class="lead">Complete with pre-defined file paths that you
					won't have to change!</p>
				<ul class="list-unstyled">
					<li>Bootstrap v3.3.1</li>
					<li>jQuery v1.11.1</li>
				</ul>
			</div> -->
			<h4>Course Materials - ${course.courseName}</h4>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th>ID</th>
						<th>Link</th>
					</tr>
					<c:choose>
						<c:when test="${eventList.size()>0}">
							<c:forEach items="${eventList}" var="message"
								varStatus="loopCounter">
								<tr>
									<td><c:out value="${message.idcourseMaterials}"></c:out></td>
									<td><a
										href="downloadCourseMaterial/${message.idcourseMaterials}/${message.courseOffering.courseRegnNum}">${message.uploadedPath}</a></td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<td>No Materials uploaded</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->

</body>

</html>
