<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html lang="en">
<head>
<!-- Bootstrap Core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.css">
<script type="text/javascript">
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp
			.getDate(), 0, 0, 0, 0);

	var checkin = $('#dpd1').datepicker({
		onRender : function(date) {
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev) {
		if (ev.date.valueOf() > checkout.date.valueOf()) {
			var newDate = new Date(ev.date)
			newDate.setDate(newDate.getDate() + 1);
			checkout.setValue(newDate);
		}
		checkin.hide();
		$('#dpd2')[0].focus();
	}).data('datepicker');

	var checkout = $('#dpd2').datepicker({
		onRender : function(date) {
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev) {
		checkout.hide();
	}).data('datepicker');
</script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>My Service Portal</title>

<!-- Custom CSS -->
<style>
body {
	padding-top: 70px;
	/* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
}

.span12 {
	text-align: right
}
</style>

</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="home">My Courses</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#">Library<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<!-- <li><a href="books">My Books</a></li> -->
							<li><a href="books">My Books</a></li>
							<li><a href="searchBooks">Search / Checkout</a></li>
						</ul></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#">Courses<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="courses">My Courses</a></li>
						</ul></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#">Jobs<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="jobs">My Jobs</a></li>
							<li><a href="searchJobs">Search / Apply</a></li>
							<li><a href="createProfile">Create Profile</a></li>
						</ul></li>
					<li><a href="fees">My Fees</a></li>
					<li><a href="settings">Account</a></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#">Account<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="settings">Settings</a></li>
							<li><a href="logout">Logout</a></li>
						</ul></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>

	<!-- Page Content -->
	<div class="container">

		<div class="row">
			<h5>Welcome ${fullName}</h5>
			<br>
			<!-- <div class="col-lg-12 text-center">
				<h1>A Bootstrap Starter Template</h1>
				<p class="lead">Complete with pre-defined file paths that you
					won't have to change!</p>
				<ul class="list-unstyled">
					<li>Bootstrap v3.3.1</li>
					<li>jQuery v1.11.1</li>
				</ul>
			</div> -->
			<h4>JobSearch</h4>
			<table class="table table-striped">
				<tbody>
					<tr>
						<th></th>
						<th>Event Description</th>
						<th>Organizer</th>
						<th>Start</th>
						<th>End</th>
						<th>Event Type</th>
						<th>Location</th>
					</tr>
					<c:forEach items="${eventList}" var="message"
						varStatus="loopCounter">
						<tr>
							<td><c:out value="${message.eventTitle}"></c:out></td>
							<td><c:out value="${message.eventDesc}"></c:out></td>
							<td><c:out value="${message.organizer}"></c:out></td>
							<td><c:out value="${message.startDate}"></c:out></td>
							<td><c:out value="${message.endDate}"></c:out></td>
							<td><c:out value="${message.eventType}"></c:out></td>
							<td><c:out value="${message.location.buildingName}"></c:out>
								<c:out value="${message.location.roomNo}"></c:out></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<h4>Announcements</h4>
			<c:forEach items="${announcementList}" var="message"
				varStatus="loopCounter">

				<div class="panel panel-info">
					<div class="panel-heading">
						<h6 class="panel-title">${message.announcementType}
							${message.courseOffering.course.courseName}</h6>
						<div class="span12">Posted Date: ${message.startDate}</div>
					</div>
					<h5 class="panel-title">${message.announcementTitle}</h5>
					<div class="panel-body">${message.announcementBody}</div>
				</div>

				<%-- <div class="row">
							<div class="col-md-12">
								<strong>${message.announcementType}</strong> <b>${message.announcementTitle}</b>
								<br> ${message.announcementBody}
							</div>
							<div class="col-md-12"></div>
						</div> --%>
			</c:forEach>
		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->

</body>

</html>
