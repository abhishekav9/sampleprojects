<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html lang="en">
<head>
<!-- Bootstrap Core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet">
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"
	type="text/javascript"></script>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>My Service Portal</title>

<!-- Custom CSS -->
<style>
body {
	padding-top: 70px;
	/* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
}

.span12 {
	text-align: right
}
</style>

<link
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/start/jquery-ui.css"
	rel="Stylesheet" type="text/css" />
<script type="text/javascript">
	$(document).ready(function() {
		$("#txtFrom").datepicker({
			numberOfMonths : 1,
			dateFormat : 'yy/mm/dd',
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() + 1);
				$("#txtTo").datepicker("option", "minDate", dt);
			}
		});
		$("#txtTo").datepicker({
			numberOfMonths : 1,
			dateFormat : 'yy/mm/dd',
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() - 1);
				$("#txtFrom").datepicker("option", "maxDate", dt);
			}
		});
	});
</script>
</head>

<body>

	<!-- Navigation -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- Page Content -->
	<div class="container">

		<div class="row">
			<h5>Welcome ${fullName}</h5>
			<br>
			<h4>Job Search:</h4>
			<c:if test="${error.length()>0}">
				<div id="index-alert" class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					<strong>Oh snap!</strong> ${error}
				</div>
			</c:if>
			<form action="jobTitleSearch" method="post">
				<table class="table table-striped">
					<tbody>
						<tr>
							<td><b>Look up by Job Title:</b></td>
						</tr>
						<tr>
							<td><input type="text" required="required" name="jobTitle">|<input
								type="submit" value="See Job"></td>
						</tr>
					</tbody>
				</table>
			</form>
			<form action="jobIdSearch" method="POST">
				<table class="table table-striped">
					<tbody>
						<tr>
							<td><b>Look up by Job ID:</b></td>
						</tr>
						<tr>
							<td><input type="number" required="required" name="jobId">|<input
								type="submit" value="See Job"></td>
						</tr>
					</tbody>
				</table>
			</form>
			<form action="quickJobSearch" method="post">
				<h4>Quick Search:</h4>
				<table class="table table-striped">
					<tbody>
						<tr>
							<td><b>Job Type:</b></td>
							<td><select name="jobType"><option
										value="Internship">Internship</option>
									<option value="Co-op">Co-op</option>
									<option value="Full-Time">Full-Time</option></select></td>
							<td><b>Car Required:</b></td>
							<td><select name="carRequired"><option value="">--Select--</option>
									<option value="Yes">Yes</option>
									<option value="No">No</option></select></td>
						<tr>
							<td><b>Posted from:</b></td>
							<td><input type="text" id="txtFrom" name="postedFrom" /></td>
							<td><b>Posted to:</b></td>
							<td><input type="text" id="txtTo" name="postedTo" /></td>
						</tr>
						<tr>
							<td><b>Duration (months):</b></td>
							<td><input type="number" min="0" max="12" step="1"
								name="duration"></td>
							<td><b>Job Title:</b></td>
							<td><input type="text" name="jobTitle"></td>
							<td>Min GPA:</td>
							<td><input type="number" min="0" max="4" step="0.1"
								name="gpa"></td>

						</tr>
						<tr>
							<td><input type="submit" value="Search"></td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</body>

</html>
