<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>
<c:if test="${cookie.containsKey('userIdCookie')}">
	<c:set var="userId" value="${cookie['userIdCookie'].value}"
		scope="session" />
	<c:if test="${cookie.containsKey('userNameCookie')}">
		<c:set var="userName" value="${cookie['userNameCookie'].value}"
			scope="session" />
	</c:if>

	<c:if test="${cookie.containsKey('userRoleCookie')}">
		<c:set var="userRole" value="${cookie['userRoleCookie'].value}"
			scope="session" />
	</c:if>

	<c:if test="${cookie.containsKey('userTypeCookie')}">
		<c:set var="userType" value="${cookie['userTypeCookie'].value}"
			scope="session" />
	</c:if>
	<c:redirect url="/"></c:redirect>
</c:if>
<%-- <c:choose>
	<c:when test="${cookie.containsKey('userIdCookie')}">
		<c:set var="userId" value="${cookie['userIdCookie'].value}"
			scope="session" />
		<c:redirect url="/"></c:redirect>
	</c:when>
</c:choose> --%>
<html>
<head>
<style type="text/css">
body {
	background: url(http://lorempixel.com/1920/1920/abstract/7/) no-repeat
		center center fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
</style>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<title>Self Service Portal</title>
</head>
<body>
	<%-- <h6 style="color: #ff0000">${error}</h6> --%>
	<c:if test="${error.length()>0}">
		<div id="index-alert" class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			<strong>Oh snap!</strong> ${error}
		</div>
	</c:if>
	<form:form method="POST" commandName="user" id="user"
		class="form-horizontal" role="form">
		<div class="container">
			<div style="background: transparent !important" class="jumbotron">
				<h2 style="color: white;">NEU Self Service Portal</h2>
				<p style="color: white;">Your consolidated source for
					information and resources important to students. Use your
					credentials to access this web site.</p>
			</div>
			<div id="loginbox" style="margin-top: 50px;"
				class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="panel-title">Sign In</div>
						<div
							style="float: right; font-size: 80%; position: relative; top: -10px">
							<a href="forgotPassword">Forgot password?</a>
						</div>
					</div>

					<div style="padding-top: 30px" class="panel-body">


						<c:if test="${error0.length()>0}">
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<strong>Error! </strong>${error0}
							</div>
						</c:if>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-user"></i></span>
							<form:input path="userName" class="form-control" id="userName"
								placeholder="email" required="required" />
							<br>
						</div>
						<c:if test="${error1.length()>0}">
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<strong>Error! </strong>${error1}
							</div>
						</c:if>

						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-lock"></i></span>
							<form:password path="userPassword" id="userPassword"
								class="form-control" placeholder="password" required="required" />
							<br>
							<form:errors path="userPassword" cssStyle="color:#ff0000"></form:errors>
						</div>



						<div class="input-group">
							<div class="checkbox">
								<label> <input id="login-remember" type="checkbox"
									value="remember" name="remember"> Remember me
								</label>
							</div>
						</div>


						<div style="margin-top: 10px" class="form-group">
							<!-- Button -->

							<div class="col-sm-12 controls">
								<input type="submit" class="btn btn-primary" name="submit"
									value="Submit" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form:form>
</body>
</html>