<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="home">Home</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Library<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<!-- <li><a href="books">My Books</a></li> -->
						<li><a href="#">My Books</a></li>
						<!-- <li><a href="searchBooks">Search / Checkout</a></li> -->
					</ul></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Courses<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="courses">My Courses</a></li>
					</ul></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Jobs<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href=#>My Jobs</a></li>
						<li><a href="searchJobs">Search / Apply</a></li>
						<li><a href="#">Create Profile</a></li>
					</ul></li>
				<li><a href="#">My Fees</a></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Account<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="changePassword">Settings</a></li>
						<li><a href="logout">Logout</a></li>
					</ul></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>