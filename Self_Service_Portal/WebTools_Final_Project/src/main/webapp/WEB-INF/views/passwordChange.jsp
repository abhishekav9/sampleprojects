<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html lang="en">
<head>
<!-- Bootstrap Core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>My Service Portal</title>

<!-- Custom CSS -->
<style>
body {
	padding-top: 70px;
	/* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
}

.span12 {
	text-align: right
}
</style>

<link href="<c:url value="/resources/css/jquery-ui.css" />"
	rel="stylesheet" type="text/css" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="<c:url value="/resources/js/jquery-ui.min.js" />"
	type="text/javascript"></script>
</head>

<body>

	<!-- Navigation -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- Page Content -->
	<div class="container">

		<div class="row">
			<h5>Welcome ${fullName}</h5>
			<br>
			<h3>Change password</h3>
			<form:form method="POST" action="changePassword"
				commandName="passwordChange">
				<table>
					<tr>
						<td>Enter old Password:</td>
						<td><form:password path="oldPass" /></td>
						<td><form:errors path="oldPass" cssStyle="color:#ff0000" />
					</tr>
					<tr>
						<td>New Password:</td>
						<td><form:password path="newPass" /></td>
						<td><form:errors path="newPass" cssStyle="color:#ff0000" /></td>
					</tr>
					<tr>
						<td>New Password again:</td>
						<td><form:password path="newPassAgain" /></td>
						<td><form:errors path="newPassAgain" cssStyle="color:#ff0000" /></td>
					</tr>
					<tr>
						<td><input type="submit" value="Change Password" /></td>
					</tr>

				</table>
			</form:form>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</body>

</html>