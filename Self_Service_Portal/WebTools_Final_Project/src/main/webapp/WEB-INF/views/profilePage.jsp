<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>My Messages</title>
<script type="text/javascript">
	function checkDelete() {
		var checkboxes = document.getElementsByName('delete');
		var selected = [];
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i].checked) {
				selected.push(checkboxes[i].value);
			}
		}
		if (selected.length > 0) {
			return true;
		} else {
			alert("Select atleast one!!");
			return false;
		}
	}
</script>
</head>
<body>
	<h3>Welcome ${userAccounts.userName}</h3>

	<h4>Your Basic Information:</h4>
	<form action="deleteMessages" method="POST"
		onsubmit="return checkDelete();">
		<table>
			<tbody>

				<tr>
					<td><input type="text" disabled="disabled" name="lastName"
						value="${personProfile.lastName}" /></td>
					<td><input type="text" disabled="disabled" name="firstName"
						value="${personProfile.firstName}" /></td>
					<td><input type="text" disabled="disabled" name="dateOfBirth"
						value="${personProfile.dateOfBirth}" /></td>
				</tr>
				<tr>
					<td><input type="text" disabled="disabled" name="gender"
						value="${personProfile.gender}" /></td>
					<td><input type="text" name="emailId"
						value="${personProfile.emailId}" /></td>
					<td><input type="text" name="mobile"
						value="${personProfile.mobile}" /></td>
				</tr>
				<tr>
					<td><input type="text" disabled="disabled" name="address1"
						value="${personProfile.address1}" /></td>
					<td><input type="text" disabled="disabled" name="city"
						value="${personProfile.city}" /></td>
					<td><input type="text" disabled="disabled" name="state"
						value="${personProfile.state}" /></td>
				</tr>
				<tr>
					<td><input type="text" disabled="disabled" name="country"
						value="${personProfile.country}" /></td>
					<td><input type="text" disabled="disabled" name="zipcode"
						value="${personProfile.zipcode}" /></td>

				</tr>
			</tbody>
		</table>
		Academic Info:
		<table>
			<tbody>
				<tr>
					<td><input type="text" disabled="disabled" name="studentNum"
						value="${userProfile.studentNum}" /></td>
					<td><input type="text" name="localAddress"
						value="${userProfile.localAddress}" /></td>
					<td><input type="text" name="localCity"
						value="${userProfile.localCity}" /></td>
				</tr>
				<tr>
					<td><input type="text" name="localState"
						value="${userProfile.localState}" /></td>
					<td><input type="text" name="localZipCode"
						value="${userProfile.localZipCode}" /></td>
					<td><input type="text" disabled="disabled" name="programName"
						value="${userProfile.program.programName}" /></td>
				</tr>
				<tr>
					<td><input type="text" disabled="disabled" name="study"
						value="${userProfile.study}" /></td>
					<td><input type="text" disabled="disabled" name="ssn"
						value="${userProfile.ssn}" /></td>
					<td><input type="text" disabled="disabled"
						name="international" value="${userProfile.international}" /></td>
				</tr>
				<%-- <c:choose>
					<c:when test="${messages.size() > 0}">
						<tr>
							<td><input type="submit" value="Delete Selected Messages"></td>
						</tr>
					</c:when>
				</c:choose> --%>
			</tbody>
		</table>
	</form>
</body>
</html>