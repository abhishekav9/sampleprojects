<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html lang="en">
<head>
<!-- Bootstrap Core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>My Service Portal</title>

<!-- Custom CSS -->
<style>
body {
	padding-top: 70px;
	/* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
}

.span12 {
	text-align: right
}
</style>

<link href="<c:url value="/resources/css/jquery-ui.css" />"
	rel="stylesheet" type="text/css" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="<c:url value="/resources/js/jquery-ui.min.js" />"
	type="text/javascript"></script>
</head>

<body>

	<!-- Navigation -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- Page Content -->
	<div class="container">

		<div class="row">
			<h5>Welcome ${fullName}</h5>
			<br>
			<h4>Search Results: ${searchParameter}</h4>

			<form action="sendMessage" method="post">
				<input type="hidden" name="resultPage" value="${resultPage}">
				<h3>Search Result:</h3>
				<table>
					<tr>
						<th>Job Title</th>
						<th>Job Desc</th>
						<th>Posted Date</th>
						<th>Car Required</th>
						<th>Job Location</th>
						<th>Min Gpa</th>
						<th>Duration</th>
						<th>minWage</th>
						<th>maxWage</th>
						<th>Job Type</th>
					</tr>

					<c:choose>
						<c:when test="${user!=null}">
							<tr>
								<td><c:out value="${user.jobTitle}"></c:out></td>
								<td><c:out value="${user.jobDesc}"></c:out></td>
								<td><c:out value="${user.createdDate}"></c:out></td>
								<td><c:out value="${user.carRequired}"></c:out></td>
								<td><c:out value="${user.jobLocation}"></c:out></td>
								<td><c:out value="${user.minGpa}"></c:out></td>
								<td><c:out value="${user.duration}"></c:out></td>
								<td><c:out value="${user.minWage}"></c:out></td>
								<td><c:out value="${user.maxWage}"></c:out></td>
								<td><c:out value="${user.jobType}"></c:out></td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td>"No Results"</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</table>
			</form>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</body>

</html>