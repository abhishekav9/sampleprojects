function populateGraph(number){
	if(number == 1){
		var x = document.getElementsByClassName("result");
		
		x[0].style="background:#e3495c;width:50%";
		x[0].innerHTML="Knowledege (50%)"
		x[1].style="background:#e74d24;width:70%";
		x[1].innerHTML="Innovation (70%)";
		x[2].style="background:#f37121;width:90%";
		x[2].innerHTML="Research (90%)";
		x[3].style="background:#fec246;width:100%";
		x[3].innerHTML="Time Efficiency (100%)";
		x[4].style="background:#f89728;width:90%";
		x[4].innerHTML="Skill Diversity (90%)";
		var divRank = document.getElementById("rank1");

		divRank.style=="color:#b8b8b8;";
	
	}else if(number == 2){
		var x = document.getElementsByClassName("result");
		x[0].style="background:#e3495c;width:90%";
		x[0].innerHTML="Knowledege (90%)"
		x[1].style="background:#e74d24;width:50%";
		x[1].innerHTML="Innovation (50%)"
		x[2].style="background:#f37121;width:40%";
		x[2].innerHTML="Research (40%)";
		x[3].style="background:#fec246;width:75%";
		x[3].innerHTML="Time Efficiency (75%)";
		x[4].style="background:#f89728;width:63%";
		x[4].innerHTML="Skill Diversity (63%)";
		var divRank = document.getElementById("rank2");

		divRank.style=="color:#b8b8b8;";
		
		
	}else if(number == 3){
		var x = document.getElementsByClassName("result");
		x[0].style="background:#e3495c;width:60%";
		x[0].innerHTML="Knowledege (60%)"
		x[1].style="background:#e74d24;width:30%";
		x[1].innerHTML="Innovation (30%)"
		x[2].style="background:#f37121;width:70%";
		x[2].innerHTML="Research (70%)";
		x[3].style="background:#fec246;width:50%";
		x[3].innerHTML="Time Efficiency (50%)";
		x[4].style="background:#f89728;width:90%";
		x[4].innerHTML="Skill Diversity (90%)";
		var divRank = document.getElementById("rank3");

		divRank.style=="color:#b8b8b8;";
		
	}
}


function showMenu(number){
	
	if(number == 1){

	if (document.contains(document.getElementById("contDiv"))) {
            document.getElementById("contDiv").remove();
	}

	var mainDiv =document.getElementById("rndContent");
	var div = document.createElement("div");
	div.id="contDiv";
	var h1 = document.createElement("h1");
	h1.style="color:65585a; padding:2%";
	var h41 = document.createElement("h4");
	var h42 = document.createElement("h4");
	var h43 = document.createElement("h4");
	h1.innerHTML = "Summary";
	div.appendChild(h1);
	h41.innerHTML = "The aim of this competition is to encourage cross-sector supply chains that can deliver integrated energy solutions at different scales to meet the many energy systems challenges of achieving a low-carbon economy by 2050. Projects should focus on both the demand and supply side across the energy networks of electricity, heat and combustible gases.";
	h41.style="color:65585a;text-align: justify;padding:2%";
	h42.innerHTML = "This is a two-stage competition with a first stage of feasibility studies. Up to three projects will then be chosen for follow-on collaborative research and development (R&D) following a collaboration workshop on conclusion of the feasibility stage. At each stage, projects must be led by a business. Up to £500,000 of the total funding will be available for the smaller-scale feasibility studies. These can be single-company or collaborative. Small businesses could receive up to 70% of their eligible project costs, medium-sized businesses 60% and large businesses 50%. We expect total eligible costs for feasibility studies projects to be no more than £40,000. Feasibility studies are expected to last up to three months. We plan to commit up to £9 million for the collaborative R&D projects in the second stage.";
	h42.style="color:65585a;text-align: justify;padding:2%";
	h43.innerHTML = " Each consortium must involve at least one SME. For these industrial research projects, a business partner can attract 50% public funding for their project costs (60% for SMEs). The first stage of this competition (feasibility studies) opens on 5 January 2015. The deadline for registration is at noon on 11 February 2015. The second stage (collaborative R&D) is by invitation and is expected to open on 7 September 2015. A briefing event for potential applicants at the feasibility stage will be held in Birmingham on 15 January 2015.";
	h43.style="color:65585a;text-align: justify;padding:2%";
	
	div.appendChild(h41);
	div.appendChild(h42);
	div.appendChild(h43);
	mainDiv.appendChild(div);

	}	else if(number == 2 ){

	var elem = document.getElementById("contDiv");
    if (document.contains(document.getElementById("contDiv"))) {
            document.getElementById("contDiv").remove();
	}

	var mainDiv =document.getElementById("rndContent");
	var div = document.createElement("div");
	div.id="contDiv";
	var h1 = document.createElement("h2");
	h1.style="color:65585a; padding:2%";
	var h41 = document.createElement("h4");
	var h42 = document.createElement("h4");
	var h43 = document.createElement("h4");
	h1.innerHTML = "Stage 1 – Feasibility studies ";
	div.appendChild(h1);
	h41.innerHTML = "The feasibility study element of this competition will open for applications on 5 January 2015.";
	h41.style="color:65585a;text-align: justify;padding:2%";
	h42.innerHTML = "All applicants must first register via our website, and the deadline for registration is at noon on 11 February 2015. The deadline for applications is at noon on 18 February 2015.";
	h42.style="color:65585a;text-align: justify;padding:2%";
	h43.innerHTML = "A briefing will be held in Birmingham on 15 January 2015 to highlight the main features of the competition and to explain the application process. Applicants are strongly recommended to attend. ";
	h43.style="color:65585a;text-align: justify;padding:2%";

	var h12 = document.createElement("h2");
	h12.style="color:65585a; padding:2%";
	var h412 = document.createElement("h4");
	var h422 = document.createElement("h4");
	var h432 = document.createElement("h4");
	h12.innerHTML = "Stage 2 – Collaborative R&D (by invitation only) ";
	
	h412.innerHTML = "We will hold a competition briefing and project collaboration workshop on 2–3 September (venue to be confirmed). Following the workshop, the collaborative R&D competition will open for applications on 7 September 2015 and close at noon on 22 October 2015.";
	h412.style="color:65585a;text-align: justify;padding:2%";

	
	div.appendChild(h41);
	div.appendChild(h42);
	div.appendChild(h43);
	div.appendChild(h12);
	div.appendChild(h412);
	mainDiv.appendChild(div);

	} else if(number == 3 ){

	var elem = document.getElementById("contDiv");
    if (document.contains(document.getElementById("contDiv"))) {
            document.getElementById("contDiv").remove();
	}

	var mainDiv =document.getElementById("rndContent");
	var div = document.createElement("div");
	div.id="contDiv";
	var h1 = document.createElement("h1");
	h1.style="color:65585a; padding:2%";
	var h41 = document.createElement("h4");
	h1.innerHTML = "Interoperability";
	div.appendChild(h1);
	h41.innerHTML = "Combining and /or aggregating individual technologies, such as distributed generation, energy harvesting, storage integration, heating and cooling, utilising waste and low-grade heat, virtual power plants | Multiple vector approaches, namely across electricity, heat and combustible gases | Innovations for infrastructure such as monitoring, management, communications, and control ";
	h41.style="color:65585a;text-align: justify;padding:2%";
	div.appendChild(h41);

	var h13 = document.createElement("h1");
	h13.style="color:65585a; padding:2%";
	var h413 = document.createElement("h4");
	h13.innerHTML = "Scalability";
	div.appendChild(h13);
	h413.innerHTML = "Developing energy systems at scale | Interconnection at multiple scales | systems design | other relevant and novel approaches to systems scalability";
	h413.style="color:65585a;text-align: justify;padding:2%";
	div.appendChild(h413);

	var h12 = document.createElement("h1");
	h12.style="color:65585a; padding:2%";
	var h412 = document.createElement("h4");
	h12.innerHTML = "Adaptability";
	div.appendChild(h12);
	h412.innerHTML = "Consideration of multiple sites with different geographical constraints/ opportunities | Technology transfer from other utility sectors, such as water and waste | Demand-side management innovation • tools and services for decision making or customer engagement ";
	h412.style="color:65585a;text-align: justify;padding:2%";
	div.appendChild(h412);
	mainDiv.appendChild(div);

	}else if(number == 4){

	if (document.contains(document.getElementById("contDiv"))) {
            document.getElementById("contDiv").remove();
	}

	var mainDiv =document.getElementById("rndContent");
	var div = document.createElement("div");
	div.id="contDiv";
	var h1 = document.createElement("h1");
	h1.style="color:65585a; padding:2%";
	var h41 = document.createElement("h4");
	var h42 = document.createElement("h4");
	var h43 = document.createElement("h4");
	h1.innerHTML = "Background";
	div.appendChild(h1);
	h41.innerHTML = "Our energy systems – the way we supply and use energy in all its forms in the UK – are changing significantly as we strive with the rest of Europe to become a low-carbon economy by 2050. Over the coming decades we will see an increasingly complex mix and scale of technologies involved in generating, transmitting, distributing and storing energy. ";
	h41.style="color:65585a;text-align: justify;padding:2%";
	h42.innerHTML = "How we integrate new supply and demand side technologies and approaches to achieve resilient, affordable and sustainable energy systems is a big challenge. For instance, how will our energy systems adapt to meet the shifting demand from growing numbers of electric vehicles and from increased electric heating? However, UK businesses – and SMEs in particular – are finding that the level of investment and risk needed to validate their technologies at scale and as part of an integrated energy system is prohibitive.";
	h42.style="color:65585a;text-align: justify;padding:2%";
	h43.innerHTML = "Valuable innovations may be lost to the energy market if SMEs are not effectively engaged in the supply chain. We want to address the issue through this competition, creating business opportunities along and across sectors and their supply chains. The UK's energy infrastructure may need more than £300bn investment to address the challenges of delivering secure and affordable energy for all, while meeting environmental legislation. Navigant and Bloomberg estimate that the market for smart energy systems will be worth £3 billion to £5 billion by 2020. ";
	h43.style="color:65585a;text-align: justify;padding:2%";
	
	div.appendChild(h41);
	div.appendChild(h42);
	div.appendChild(h43);
	mainDiv.appendChild(div);

	}else if(number == 6){

	if (document.contains(document.getElementById("contDiv"))) {
            document.getElementById("contDiv").remove();
	}

	var mainDiv =document.getElementById("rndContent");
	var div = document.createElement("div");
	div.id="contDiv";
	var h1 = document.createElement("h1");
	h1.style="color:65585a; padding:2%";
	var h51 = document.createElement("h5");
	var h52 = document.createElement("h5");
	var h53 = document.createElement("h5");
	var h54 = document.createElement("h5");
	h1.innerHTML = "Key Dates";
	div.appendChild(h1);
	h51.innerHTML = "Feasibility studies competition opens: 5 January 2016 ";
	h51.style="color:65585a;text-align: justify;padding:0.5%";
	h52.innerHTML = "Competition briefing: 15 January 2016";
	h52.style="color:65585a;text-align: justify;padding:0.5%";
	h53.innerHTML = "Registration deadline: 11 March 2016 noon";
	h53.style="color:65585a;text-align: justify;padding:0.5%";
	h54.innerHTML = "Deadline for applications: 18 February 2015 noon";
	h54.style="color:65585a;text-align: justify;padding:0.5%";
	
	div.appendChild(h51);
	div.appendChild(h52);
	div.appendChild(h53);
	div.appendChild(h54);
	mainDiv.appendChild(div);

	}
}